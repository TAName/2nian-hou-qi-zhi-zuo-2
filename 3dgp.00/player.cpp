
#include "player.h"
#include "fbx_load.h"
#include "objmanager.h"
#include "gamepad.h"
#include "stage.h"
#include "easing.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

Player::Player(ID3D11Device* device, const char* fbx_filename, int type, const DirectX::XMFLOAT3&position)
//: m_obj(nullptr), m_isHave(false), moveFlg(false),  m_isThrow(false), electrodeeCubePos(0,0,0)
{
	std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
	FbxLoader fbx_loader;
	fbx_loader.Load(fbx_filename, *model_data);

	std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
	m_obj = std::make_shared<PlayerObj>(model_resource);

	m_obj->SetPosition(position);
	m_obj->SetScale(DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f));
	m_obj->SetAngle(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));

	m_electrodeeCubeSpeed = DirectX::XMFLOAT3(0, 0, 0);

	// 向き
	m_PlayerDirection();
	m_dir = Direction::UP;

	m_target = -1;

	// アニメーション
	m_currentAnimation = AnimationNum::Wait;
	m_nextAnimation = -1;
	m_loop = true;
	m_move = false;
	m_obj->Play(AnimationNum::Wait, true);
	mPlayerType = type;
	switch (mPlayerType)
	{
	case 0:
		pObjManager.Set_Player1(m_obj);
		break;
	case 1:
		pObjManager.Set_Player2(m_obj);
		break;
	}
	moveTimer = 0;
}

void Player::m_PlayerDirection()
{
	// プレイヤーの向き オイラー角
	DirectX::XMMATRIX R;
	DirectX::XMFLOAT4X4 rotation;
	R = DirectX::XMMatrixRotationRollPitchYaw(
		m_obj->GetAngle().x, m_obj->GetAngle().y, m_obj->GetAngle().z);
	DirectX::XMStoreFloat4x4(&rotation, R);

	m_forward = DirectX::XMFLOAT3(rotation._31, rotation._32, rotation._33);
	// 正規化
	float l = sqrtf(m_forward.x*m_forward.x + m_forward.y*m_forward.y + m_forward.z*m_forward.z);
	float f = 1 / l;
	m_forward.x *= f; m_forward.y *= f; m_forward.z *= f;
}

void Player::m_Move(float elapsedTime,
	DirectX::XMFLOAT3& translate, DirectX::XMFLOAT3& rotation)
{
	static float angle = 0.f * 0.01745f;
	static float nextAngle = 0.f * 0.01745f;

	// 移動
	if (input::ButtonPressedState(mPlayerType, input::PadLabel::UP))
	{
		rotation.y = 0.f * 0.01745f;
#if MODO
		if (moveTimer % 10 == 0)translate.z += 10;
#else
		translate.z += 1;
#endif
		moveTimer++;
		m_obj->SetSpeed(DirectX::XMFLOAT3(0, 0, 1));

		m_move = true;
		if (m_currentAnimation != AnimationNum::Catch &&
			m_currentAnimation != AnimationNum::Throw)
		{
			m_ChangeAinmation(AnimationNum::Move, true);
		}
	}
	else if (input::ButtonPressedState(mPlayerType, input::PadLabel::DOWN))
	{
		rotation.y = 180.f * 0.01745f;
#if MODO
		if (moveTimer % 10 == 0)translate.z -= 10;
#else
		translate.z -= 1;
#endif
		moveTimer++;
		m_obj->SetSpeed(DirectX::XMFLOAT3(0, 0, -1));

		m_move = true;
		if (m_currentAnimation != AnimationNum::Catch &&
			m_currentAnimation != AnimationNum::Throw)
		{
			m_ChangeAinmation(AnimationNum::Move, true);
		}
	}
	else if (input::ButtonPressedState(mPlayerType, input::PadLabel::RIGHT))
	{
		rotation.y = 270.f * 0.01745f;
#if MODO
		if (moveTimer % 10 == 0)translate.x -= 10;
#else
		translate.x -= 1;
#endif
		moveTimer++;
		m_obj->SetSpeed(DirectX::XMFLOAT3(-1, 0, 0));

		m_move = true;
		if (m_currentAnimation != AnimationNum::Catch &&
			m_currentAnimation != AnimationNum::Throw)
		{
			m_ChangeAinmation(AnimationNum::Move, true);
		}
	}
	else if (input::ButtonPressedState(mPlayerType, input::PadLabel::LEFT))
	{
		rotation.y = 90.f * 0.01745f;
#if MODO
		if (moveTimer % 10 == 0)translate.x += 10;
#else
		translate.x += 1;
#endif
		moveTimer++;
		m_obj->SetSpeed(DirectX::XMFLOAT3(1, 0, 0));
		m_move = true;
		if (m_currentAnimation != AnimationNum::Catch &&
			m_currentAnimation != AnimationNum::Throw)
		{
			m_ChangeAinmation(AnimationNum::Move, true);
		}
	}
	else
	{
		m_move = false;
		moveTimer = 0;
	}
	//if (angle != nextAngle)
	//{
	//    //angle = InQuad(elapsedTime, 20.f, nextAngle, angle);
	//    angle = nextAngle;
	//}
	//rotation.y = nextAngle;
}

void Player::m_Throw(DirectX::XMFLOAT3& translate, DirectX::XMFLOAT3& rotation)
{
	static const float SPEED = 2.f;

	// 電気を持つ　投げる
	static int rigidTimer = 0;
	if (input::ButtonRisingState(mPlayerType, input::PadLabel::A))
	{
		// 投げる
		if (m_isHave)
		{
			DirectX::XMFLOAT3 P, N;
			float L;
			if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(translate.x + m_forward.z*2.f, 0, translate.z + m_forward.x*2.f), DirectX::XMFLOAT3(translate.x + m_forward.z*2.f + m_forward.x*7.f, 0, translate.z + m_forward.x*2.f + m_forward.z*7.f), &P, &N, &L) == -1
				|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(translate.x - m_forward.z*2.f, 0, translate.z - m_forward.x*2.f), DirectX::XMFLOAT3(translate.x - m_forward.z*2.f + m_forward.x*7.f, 0, translate.z - m_forward.x*2.f + m_forward.z*7.f), &P, &N, &L) == -1)
			{

				m_isHave = false;
				m_electrodeeCubeSpeed = DirectX::XMFLOAT3(
					m_forward.x * SPEED,
					0,
					m_forward.z * SPEED);
				// 電気に速度を入れる
				pStageManager.GetCube().at(m_target).SetSpeed(m_electrodeeCubeSpeed);
				pStageManager.GetCube().at(m_target).SetPosition(DirectX::XMFLOAT3(
					translate.x + m_forward.x * 7,
					0,
					translate.z + m_forward.z * 7));

				m_target = -1;
				rigidTimer = 5;
				m_ChangeAinmation(AnimationNum::Throw);
				m_move = true;
				m_obj->SetCarryCube(-1);
			}
		}

		// もつ
		else if (!m_isHave)
		{
			m_target = -1;
			m_electroNum = 0;
			for (auto& c : pStageManager.GetCube())
			{
				if (c.GetObjType() == mPlayerType && !c.GetMoveFlag())
				{
					m_electrodeeCubePos = c.GetPosition();
					// プレイヤーから電気への向き
					DirectX::XMFLOAT3 electDir;
					electDir = DirectX::XMFLOAT3(
						m_electrodeeCubePos.x - translate.x,
						m_electrodeeCubePos.y - translate.y,
						m_electrodeeCubePos.z - translate.z);
					// 正規化
					float l = sqrtf(electDir.x*electDir.x + electDir.y*electDir.y + electDir.z*electDir.z);
					float f;
					if (l != 0)  f = 1 / l;
					else f = 0;
					electDir.x *= f; electDir.y *= f; electDir.z *= f;

					float dot = m_forward.x * electDir.x + m_forward.z * electDir.z;
					d = dot;
					// もつ条件
					if (dot > 0.7f &&
						(int)l < 15)
					{
						m_ChangeAinmation(AnimationNum::Catch);
						m_target = m_electroNum;
						m_obj->SetCarryCube(m_target);
						m_isHave = true;
						break;
					}
				}
				m_electroNum++;

			}
		}
	}
	// 電気
	auto& cube = pStageManager.GetCube();
	if (m_isHave)
	{
		cube.at(m_target).SetPosition(DirectX::XMFLOAT3(
			translate.x + m_forward.x * 7,
			0,
			translate.z + m_forward.z * 7));

		m_move = true;
	}

}

void Player::Update(float elapsed_time)
{
	// 生存
	if (!m_obj->GetExist()) return;

	// 変数
	DirectX::XMFLOAT3 translate = m_obj->GetPosition();
	//DirectX::XMFLOAT3 scale = m_obj->GetScale();
	DirectX::XMFLOAT3 rotation = m_obj->GetAngle();

	// 移動関数
	m_Move(elapsed_time, translate, rotation);
	// 投げる関数
	m_Throw(translate, rotation);
	// 向き
	m_PlayerDirection();

	if (!m_move)
	{
		m_ChangeAinmation(AnimationNum::Wait, true);
	}

#ifdef USE_IMGUI
	// IMGUI
	ImGui::Begin("player");
	ImGui::Text("forward x:%f, y:%f, z:%f", m_forward.x, m_forward.y, m_forward.z);
	ImGui::Text("playerPos x:%f, y:%f, z:%f", translate.x, translate.y, translate.z);
	ImGui::Text("dot: %f", d);
	// アニメーション
	ImGui::Text("currentAnimationNum: %d   ", m_currentAnimation);
	ImGui::SameLine();
	switch (m_currentAnimation)
	{
	case AnimationNum::Wait: ImGui::Text("Wait"); break;
	case AnimationNum::Move: ImGui::Text("Move"); break;
	case AnimationNum::Catch: ImGui::Text("Catch"); break;
	case AnimationNum::Throw: ImGui::Text("Throw"); break;
	}
	ImGui::End();
#endif

	// 情報を返す
	m_obj->SetPosition(translate);
	//m_obj->SetScale(scale);
	m_obj->SetAngle(rotation);
}

