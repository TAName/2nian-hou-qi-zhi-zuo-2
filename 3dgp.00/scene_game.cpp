#include "scene_game.h"
#include "fbx_load.h"
#include"camera.h"
#include"objmanager.h"
#include"stage.h"
#include"particle.h"
#include"screenout.h"
#include"soundmanager.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

extern int gamechoice;
Scene_Game::Scene_Game(ID3D11Device * device)
{
	loading_thread = std::make_unique<std::thread>([&](ID3D11Device *device)
	{
		std::lock_guard<std::mutex> lock(loading_mutex);
		pObjManager.Create(device);
		//std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
		//FbxLoader fbx_loader;
		//fbx_loader.Load(fbx_filename, *model_data);

		//std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));

		//m_obj = std::make_unique<Obj3D>(model_resource);
		//pObjManager.Set_Player1(m_obj);

		//m_obj->SetPosition(DirectX::XMFLOAT3(-5, 0, 50));
		//m_obj->SetColor(DirectX::XMFLOAT4(0, 0, 0, 1));
		mPlayer1 = std::make_unique<Player>(device, "Data/FBX/player2_02_c.fbx", 0, DirectX::XMFLOAT3(80.0f, 0.0f, 20.0f));
		pStageManager.Create(device);
		//pObjManager.Set_StageObj(m_stage);
		mPlayer2 = std::make_unique<OpponentPlayer>(device, gamechoice);
		pParticleManager->Create(device);
	}, device);
	mLoad = std::make_unique<Loading>(device, L"Data/image/operation_a.png", L"Data/image/NOW_LOADING.png", L"Data/image/button.png");
	pCamera.Create(DirectX::XMFLOAT3(0, 350, 100), DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f));
	start = std::make_unique<StageStart>(device, L"Data/image/123.png", L"Data/image/select_a.png", L"Data/image/button.png", L"Data/image/gamestart.png");
	background = std::make_unique<Sprite>(device, L"Data/image/gameBG.png");
}

int Scene_Game::Update(float elapsed_time)
{
	if (mLoad->NowLoadingUpdate(IsNowLoading()))
	{

		return 0;
	}
	EndLoading();

	int fadoOut = pScreenOut.Update(elapsed_time);
	if (start->Update(elapsed_time, fadoOut))
	{
		pObjManager.Update();
		return 0;
	}
	if (fadoOut == 2)
	{
		pSoundManager.Stop(SoundManager::SOUND::BGM_Game);

		return 3;
	}
	mPlayer1->Update(elapsed_time);
	mPlayer2->Update(elapsed_time);
	pStageManager.Update();
	pParticleManager->Update();

	pObjManager.Update();

	return 0;
}


void Scene_Game::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{

	if (mLoad->NowLoadingRender(IsNowLoading(), devicecontext))
	{
		return;
	}
	const float PI = 3.14f;
	//ワールド行列を作成
	if (!start->RenderSelect())
	{

		//ビュー行列
		DirectX::XMMATRIX V;
		{
			//カメラの設定
			DirectX::XMVECTOR eye, focus, up;
			eye = DirectX::XMVectorSet(pCamera.GetEye().x, pCamera.GetEye().y, pCamera.GetEye().z, 1.0f);//視点
			focus = DirectX::XMVectorSet(pCamera.GetFocus().x, pCamera.GetFocus().y, pCamera.GetFocus().z, 1.0f);//注視点
			up = DirectX::XMVectorSet(pCamera.GetUp().x, pCamera.GetUp().y, pCamera.GetUp().z, 1.0f);//上ベクトル

			V = DirectX::XMMatrixLookAtLH(eye, focus, up);
		}
		DirectX::XMMATRIX P;
		{
			//画面サイズ取得のためビューポートを取得
			D3D11_VIEWPORT viewport;
			UINT num_viewports = 1;
			devicecontext->RSGetViewports(&num_viewports, &viewport);

			//角度をラジアン(θ)に変換
			float fov_y = 30 * (PI / 180.f);//角度
			float aspect = viewport.Width / viewport.Height;//画面比率
			float near_z = 0.1f;//表示最近面までの距離
			float far_z = 1000.0f;//表紙最遠面までの距離

			P = DirectX::XMMatrixPerspectiveFovLH(fov_y, aspect, near_z, far_z);
		}
		//
		DirectX::XMFLOAT4X4 view, projection;
		DirectX::XMStoreFloat4x4(&view, V);
		DirectX::XMStoreFloat4x4(&projection, P);
		static DirectX::XMFLOAT4 c(1.f, 1.0f, 1.0f, 1.f);
		//立方体の描画

		background->render(devicecontext, 0, 0, 1920, 1080, 0, 0, 1920, 1080, 0, 1, 1, 1, 0.5);
		pObjManager.Render(devicecontext, view, projection);
		pParticleManager->Render(devicecontext, V, P, DirectX::XMFLOAT4(0, -1, -1, 0));
	}
	pScreenOut.Render(devicecontext);
	start->Render(devicecontext);



}

Scene_Game::~Scene_Game()
{
	pCamera.Destroy();
	pObjManager.Destroy();
	pStageManager.Destroy();
	pParticleManager->Destroy();
	if (mLoad->NowLoadingUpdate(IsNowLoading()))EndLoading();
}
