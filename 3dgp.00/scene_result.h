#pragma once
#include"scene.h"
#include<memory>
#include"skinned_mesh.h"
#include <thread>
#include <mutex>
#include"sprite.h"
#include "playerobj.h"
#include "electrode_cube.h"
#include "cubeObj.h"

class Scene_Result :public Scene
{
	std::unique_ptr<skinned_mesh> skinned;
	std::unique_ptr<Sprite> particle;

    std::shared_ptr<PlayerObj> m_player1;
    std::shared_ptr<PlayerObj> m_player2;
    DirectX::XMFLOAT3 m_p1Pos;
    DirectX::XMFLOAT3 m_p2Pos;
    DirectX::XMFLOAT3 m_p1Scale;
    DirectX::XMFLOAT3 m_p2Scale;
    DirectX::XMFLOAT3 m_p1Angle;
    DirectX::XMFLOAT3 m_p2Angle;

    std::shared_ptr<CubeObj> m_electrode;
    DirectX::XMFLOAT3 m_cubespeed;
    DirectX::XMFLOAT3 m_electPos;
    DirectX::XMFLOAT3 m_electscale;
    DirectX::XMFLOAT3 m_electAngle;

    std::shared_ptr<ModelRenderer>m_modelRenderer;
    std::unique_ptr<Sprite> m_sprBack;
    std::unique_ptr<Sprite> m_sprWin;
    std::unique_ptr<Sprite> m_sprLose;
    std::unique_ptr<Sprite> m_sprMask;
    std::unique_ptr<Sprite> m_sprTitle;
    std::unique_ptr<Sprite> m_sprGame;

    DirectX::XMFLOAT3 m_forward;
    DirectX::XMFLOAT3 m_blowawayVec;
    DirectX::XMFLOAT3 m_zoom;
    int m_winner;
    // シーン切り替え タイトル->1  ゲーム->2
    int m_nextScene;
    float m_totalTime;
    bool m_fadeFlag;
    int m_fadeTimer;
    float m_alpha;
    int m_currentAnimationPlayer1;
    int m_currentAnimationPlayer2;

    // 演出
    bool m_isHave;
	bool playsound;
	int anim;
	int animTime;
    enum AnimationNum
    {
        Wait = 1,
        Action = 0,
        Catch = 2,
        Throw = 3
    };

public:
	//Now Loading
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;


	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}

    // アニメーション変更
    void m_ChangeAinmation(const int nextAnimation, int currentAnimation, std::shared_ptr<PlayerObj> obj, bool loop = false)
    {
        if (currentAnimation != nextAnimation)
        {
            currentAnimation = nextAnimation;
            obj->Play(currentAnimation, loop);
        }
    }
  
	Scene_Result(ID3D11Device*device);
    void ChangeScene();
    void Move(std::shared_ptr<PlayerObj> winner, std::shared_ptr<PlayerObj> loser, float elapsed_time);
    void MoveRender(ID3D11DeviceContext* devicecontext);
    void PlayerDirection(std::shared_ptr<PlayerObj> obj);
	int Update(float elapsed_time);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);

    ~Scene_Result();
};