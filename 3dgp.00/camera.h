#pragma once
#include<DirectXMath.h>

class Camera 
{
private:
	static Camera*m_camera;
	DirectX::XMFLOAT3 m_eye;
	DirectX::XMFLOAT3 m_focus;
	DirectX::XMFLOAT3 m_up;
	DirectX::XMFLOAT3 m_target_pos;
	DirectX::XMFLOAT3 m_leng;
	DirectX::XMFLOAT3 m_target_angle;

	Camera(const DirectX::XMFLOAT3& eye, const DirectX::XMFLOAT3& focus, const DirectX::XMFLOAT3& up);
	int m_type;
public:
	enum Camera_TYPE
	{
		RELATIVE_POS,
		TRACKING,
		TPS_CAMERA,
		FPS_CAMERA,
		CAMERA_MAX
	};
	static void Create(const DirectX::XMFLOAT3& eye, const DirectX::XMFLOAT3& focus, const DirectX::XMFLOAT3& up)
	{
		if (m_camera != nullptr)
			return;

		m_camera = new Camera(eye, focus, up);
	}

	void SetCamera(const DirectX::XMFLOAT3&target_pos, const DirectX::XMFLOAT3&leng, const DirectX::XMFLOAT3&target_angle, const Camera_TYPE&type);
	void Updata();
	void Relative_pos_Camera();
	void SetEye(const DirectX::XMFLOAT3 eye) { m_eye = eye; }
	DirectX::XMFLOAT3&GetEye() { return m_eye; }
	DirectX::XMFLOAT3&GetFocus() { return m_focus; }
	DirectX::XMFLOAT3&GetUp() { return m_up; }
	static Camera&getinctanse()
	{
		return *m_camera;
	}
	void Destroy()
	{
		delete m_camera;
		m_camera = nullptr;
	}
};
#define pCamera (Camera::getinctanse())