#include "objmanager.h"
#include"collision.h"
#include"vectordelete.h"
#include"screenout.h"
#include"particle.h"
#include"soundmanager.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif
ObjManager*ObjManager::m_objmanager = nullptr;
int ObjManager::m_winner = -1;
ObjManager::ObjManager(ID3D11Device*device)
{
	modelRenderer = std::make_shared<ModelRenderer>(device);
	ObjManager::m_winner = -1;
}
void ObjManager::SetHitCubPosition(const DirectX::XMFLOAT3 & cubeMin, const DirectX::XMFLOAT3 & cubeMax, const DirectX::XMFLOAT3 & playerScale, DirectX::XMFLOAT3 * playerPosition, const DirectX::XMFLOAT3&size)
{
	if (playerPosition->z - 2.f <= cubeMax.z && playerPosition->z + 2.f >= cubeMin.z)
	{
		if (playerPosition->x > cubeMax.x&&playerPosition->x - size.x*playerScale.x <= cubeMax.x)
		{
			playerPosition->x = cubeMax.x + size.x * playerScale.x;
		}
		else if (playerPosition->x < cubeMin.x&&playerPosition->x + size.x*playerScale.x >= cubeMin.x)
		{
			playerPosition->x = cubeMin.x - size.x * playerScale.x;
		}

	}

	if (playerPosition->x - 2.f <= cubeMax.x&&playerPosition->x + 2.f >= cubeMin.x)
	{
		if (playerPosition->z > cubeMax.z&&playerPosition->z - size.z*playerScale.z <= cubeMax.z)
		{
			playerPosition->z = cubeMax.z + size.z * playerScale.z;
		}
		else if (playerPosition->z < cubeMin.z&&playerPosition->z + size.z*playerScale.z >= cubeMin.z)
		{
			playerPosition->z = cubeMin.z - size.z * playerScale.z;
		}

	}


}
int ObjManager::SetHitCubPosition(const DirectX::XMFLOAT3 & cubeMin, const DirectX::XMFLOAT3 & cubeMax, const DirectX::XMFLOAT3 & playerScale, DirectX::XMFLOAT3 * playerPosition, const DirectX::XMFLOAT3 & sizeMin, const DirectX::XMFLOAT3 & sizeMax, HIT*hit)
{
	int hitturn = -1;
	if (playerPosition->z - 2.f <= cubeMax.z && playerPosition->z + 2.f >= cubeMin.z)
	{
		if (playerPosition->x > cubeMax.x&&playerPosition->x - sizeMin.x*playerScale.x <= cubeMax.x)
		{
			playerPosition->x = cubeMax.x + sizeMin.x * playerScale.x;
			hit->left = true;
		}
		else if (playerPosition->x < cubeMin.x&&playerPosition->x + sizeMax.x*playerScale.x >= cubeMin.x)
		{
			playerPosition->x = cubeMin.x - sizeMax.x * playerScale.x;
			hit->right = true;
		}

	}

	if (playerPosition->x - 2.f <= cubeMax.x&&playerPosition->x + 2.f >= cubeMin.x)
	{
		if (playerPosition->z > cubeMax.z&&playerPosition->z - sizeMin.z*playerScale.z <= cubeMax.z)
		{
			playerPosition->z = cubeMax.z + sizeMin.z * playerScale.z;
			hit->down = false;
		}
		else if (playerPosition->z < cubeMin.z&&playerPosition->z + sizeMax.z*playerScale.z >= cubeMin.z)
		{
			playerPosition->z = cubeMin.z - sizeMax.z * playerScale.z;
			hit->up = false;
		}

	}
	return hitturn;
}
//ステージOBJとの当たり判定
void ObjManager::StageHit(const DirectX::XMFLOAT3&playerMin, const DirectX::XMFLOAT3&playerMax, const DirectX::XMFLOAT3&player2Min, const DirectX::XMFLOAT3&player2Max)
{
	DirectX::XMFLOAT3 cubeMin, cubeMax, cube2Min, cube2Max, p1Position, p2Position, cube2Position,cubePosition;
	if (player1Obj != nullptr)p1Position = player1Obj->GetPosition();
	if (player2Obj != nullptr)p2Position = player2Obj->GetPosition();
	int turn;
	HIT hit;
	hit.clear();
	for (auto&stage : stageObj)
	{
		stage->Update();
		cubePosition = stage->GetPosition();
		cubeMin = DirectX::XMFLOAT3(stage->GetPosition().x - 5.f*stage->GetScale().x, stage->GetPosition().y, stage->GetPosition().z - 5.f*stage->GetScale().z);
		cubeMax = DirectX::XMFLOAT3(stage->GetPosition().x + 5.f*stage->GetScale().x, stage->GetPosition().y + 10.f*stage->GetScale().y, stage->GetPosition().z + 5.f*stage->GetScale().z);
		if (player2Obj != nullptr&&player2Obj->GetExist())
		{
			if (IsHitCube(player2Min, player2Max, cubeMin, cubeMax))
			{
#if MODO
				player2Obj->SetHit(true);
				if (player2Obj->GetSpeed().x != 0)
				{
					p2Position.x = cubePosition.x-player2Obj->GetSpeed().x*9.f;
				}
				else if (player2Obj->GetSpeed().z != 0)
				{
					p2Position.z = cubePosition.z-player2Obj->GetSpeed().z*9.f;
				}
#else
				turn=SetHitCubPosition(cubeMin, cubeMax, player2Obj->GetScale(), &p2Position,
					DirectX::XMFLOAT3(p2Position.x - player2Min.x, p2Position.y - player2Min.y, p2Position.z - player2Min.z)
					, DirectX::XMFLOAT3(player2Max.x - p2Position.x, player2Max.y - p2Position.y, player2Max.z - p2Position.z),&hit);
#endif
					player2Obj->SetLeft(hit.left);
					player2Obj->SetHitRight(hit.right);
					player2Obj->SetHitDown(hit.down);
					player2Obj->SetHitUp(hit.up);
					hit.clear();
				player2Obj->SetPosition(p2Position);

			}

		}
		if (player1Obj != nullptr&&player1Obj->GetExist())
		{
			if (IsHitCube(playerMin, playerMax, cubeMin, cubeMax))
			{
				player1Obj->SetHit(true);
#if MODO

				if (player1Obj->GetSpeed().x != 0)
				{
					p1Position.x = cubePosition.x- player1Obj->GetSpeed().x*9.f;
				}
				else if (player1Obj->GetSpeed().z != 0)
				{
					p1Position.z = cubePosition.z- player1Obj->GetSpeed().z*9.f;
				}
#else
				SetHitCubPosition(cubeMin, cubeMax, player1Obj->GetScale(), &p1Position,
					DirectX::XMFLOAT3(p1Position.x - playerMin.x, p1Position.y - playerMin.y, p1Position.z - playerMin.z)
					, DirectX::XMFLOAT3(playerMax.x - p1Position.x, playerMax.y - p1Position.y, playerMax.z - p1Position.z),&hit);
#endif
				hit.clear();
				player1Obj->SetPosition(p1Position);

			}

		}
		int cubeNum = 0;

		for (auto&cube : cubeObj)
		{
			if (player1Obj->GetCarryCube() == cubeNum || player2Obj->GetCarryCube() == cubeNum)
			{
				cubeNum++;
				continue;
			}
			if (!cube->GetMove())
			{
				cubeNum++;
				continue;
			}
			cube->Update();
			cube2Position = cube->GetPosition();
			cube2Min = DirectX::XMFLOAT3(cube->GetPosition().x - 5.f*cube->GetScale().x*0.7f, cube->GetPosition().y, cube->GetPosition().z - 5.f*cube->GetScale().z*0.7f);
			cube2Max = DirectX::XMFLOAT3(cube->GetPosition().x + 5.f*cube->GetScale().x*0.7f, cube->GetPosition().y + 10.f*cube->GetScale().y, cube->GetPosition().z + 5.f*cube->GetScale().z*0.7f);
			if (IsHitCube(cube2Min, cube2Max, cubeMin, cubeMax))
			{
				if (!cube->GetChange())
				{
					if (cube->GetType() == 0)
					{
						cube->SetColor(DirectX::XMFLOAT4(1, 0, 0, 1));
						cube->SetType(1);

						for (int i = 0; i < 70; i++)
						{
							pParticleManager->Add_Board(std::make_shared<ParticlePut_Red>(), pParticleManager->smoke, cube->GetPosition());

						}
					}
					else
					{
						cube->SetColor(DirectX::XMFLOAT4(0, 0, 1, 1));
						cube->SetType(0);
						for (int i = 0; i < 70; i++)
						{
							pParticleManager->Add_Board(std::make_shared<ParticlePut_Blue>(), pParticleManager->smoke, cube->GetPosition());

						}
					}
					cube->SetChange(true);
				}
				SetHitCubPosition(cubeMin, cubeMax, cube->GetScale(), &cube2Position, DirectX::XMFLOAT3(5.f, 5.f, 5.f));
				cube->SetPosition(cube2Position);
				//break;
			}
			cubeNum++;
		}

	}
	for (auto&cube : cubeObj)
	{
		cube->SetChange(false);
	}
}
//CubeOBJとの当たり判定
void ObjManager::CubeHit(const DirectX::XMFLOAT3&playerMin, const DirectX::XMFLOAT3&playerMax, const DirectX::XMFLOAT3&player2Min, const DirectX::XMFLOAT3&player2Max)
{
	int cube1No = 0, cube2No = 0;
	DirectX::XMFLOAT3  cubeMin, cubeMax, cube2Min, cube2Max, p1Position, p2Position, cube2Position, cubePosition;
	if (player1Obj != nullptr)p1Position = player1Obj->GetPosition();
	if (player2Obj != nullptr)p2Position = player2Obj->GetPosition();
	int turn;
	HIT hit;
	hit.clear();
	for (auto&cube : cubeObj)
	{
		cube->Update();
		if (player2Obj->GetCarryCube() == cube1No || player1Obj->GetCarryCube() == cube1No)
		{
			cube1No++;
			continue;
		}
		cubePosition = cube->GetPosition();
		cubeMin = DirectX::XMFLOAT3(cube->GetPosition().x - 5.f*cube->GetScale().x*0.7f, cube->GetPosition().y, cube->GetPosition().z - 5.f*cube->GetScale().z*0.7f);
		cubeMax = DirectX::XMFLOAT3(cube->GetPosition().x + 5.f*cube->GetScale().x*0.7f, cube->GetPosition().y + 10.f*cube->GetScale().y, cube->GetPosition().z + 5.f*cube->GetScale().z*0.7f);
		//player2の当たり判定
		if (player2Obj != nullptr)
		{
			if (player2Obj->GetExist())
			{
				{
					if (IsHitCube(player2Min, player2Max, cubeMin, cubeMax))
					{
						player2Obj->SetHit(true);
						if (cube->GetType() == 1 && !cube->GetMove())
						{
#if MODO

							if (player2Obj->GetSpeed().x != 0)
							{
								p2Position.x = cubePosition.x- player2Obj->GetSpeed().x*(5.f*0.7f + 3.f)*cube->GetScale().x;
							}
							else if (player2Obj->GetSpeed().z != 0)
							{
								p2Position.z = cubePosition.z- player2Obj->GetSpeed().z*(5.f*0.7f + 3.f)*cube->GetScale().z;
							}
#else
							turn=SetHitCubPosition(cubeMin, cubeMax, player2Obj->GetScale(), &p2Position,
								DirectX::XMFLOAT3(p2Position.x - player2Min.x, p2Position.y - player2Min.y, p2Position.z - player2Min.z)
								, DirectX::XMFLOAT3(player2Max.x - p2Position.x, player2Max.y - p2Position.y, player2Max.z - p2Position.z),&hit);
#endif
								player2Obj->SetLeft(hit.left);
								player2Obj->SetHitRight(hit.right);
								player2Obj->SetHitDown(hit.down);
								player2Obj->SetHitUp(hit.up);
								hit.clear();
							player2Obj->SetPosition(p2Position);
						}
						else if (cube->GetType() == 0)
						{
							if (m_winner == -1)
							{
								m_winner = 0;
								pSoundManager.Play(SoundManager::SOUND::Des);
								pSoundManager.SetVolume(SoundManager::SOUND::Des, 2.0f);

							}
							player2Obj->SetExist(false);
							for (int i = 0; i < 77; i++)
							{
								pParticleManager->Add_Board(std::make_shared<Particlebreak>(), pParticleManager->shere2, player2Obj->GetPosition());

							}
						}
					}
				}
			}

		}
		//player1の当たり判定
		if (player1Obj != nullptr)
		{
			if (player1Obj->GetExist())
			{
				{

					if (IsHitCube(playerMin, playerMax, cubeMin, cubeMax))
					{
						player1Obj->SetHit(true);
						if (cube->GetType() == 0 && !cube->GetMove())
						{
#if MODO
							if (player1Obj->GetSpeed().x != 0)
							{
								p1Position.x = cubePosition.x- player1Obj->GetSpeed().x*(5.f*0.7f+3.f)*cube->GetScale().x;
							}
							else if (player1Obj->GetSpeed().z != 0)
							{
								p1Position.z = cubePosition.z- player1Obj->GetSpeed().z*(5.f*0.7f + 3.f)*cube->GetScale().z;
							}
#else
							SetHitCubPosition(cubeMin, cubeMax, player1Obj->GetScale(), &p1Position,
								DirectX::XMFLOAT3(p1Position.x - playerMin.x, p1Position.y - playerMin.y, p1Position.z - playerMin.z)
								, DirectX::XMFLOAT3(playerMax.x - p1Position.x, playerMax.y - p1Position.y, playerMax.z - p1Position.z),&hit);
#endif
							hit.clear();
							player1Obj->SetPosition(p1Position);
						}
						else if (cube->GetType() == 1)
						{
							player1Obj->SetExist(false);
							if (m_winner == -1)
							{
								m_winner = 1;
								pSoundManager.Play(SoundManager::SOUND::Des);
								pSoundManager.SetVolume(SoundManager::SOUND::Des, 2.0f);
							}
							for (int i = 0; i < 77; i++)
							{
								pParticleManager->Add_Board(std::make_shared<Particlebreak>(), pParticleManager->shere2, player1Obj->GetPosition());

							}
						}
					}
				}
			}

		}

		//Cube同士の当たり判定
		for (auto&cube2 : cubeObj)
		{
			//cubeとcube2が同じとき
			if (cube2No == cube1No)
			{
				cube2No++;
				continue;
			}
			if (!cube2->GetMove())
			{
				cube2No++;
				continue;
			}

			cube2Position = cube2->GetPosition();
			cube2Min = DirectX::XMFLOAT3(cube2Position.x - 5.f*cube2->GetScale().x, cube2Position.y, cube2Position.z - 5.f*cube2->GetScale().z);
			cube2Max = DirectX::XMFLOAT3(cube2Position.x + 5.f*cube2->GetScale().x, cube2Position.y + 10.f*cube2->GetScale().y, cube2Position.z + 5.f*cube2->GetScale().z);
			if (IsHitCube(cube2Min, cube2Max, cubeMin, cubeMax))
			{
				SetHitCubPosition(cubeMin, cubeMax, cube2->GetScale(), &cube2Position, DirectX::XMFLOAT3(5.f, 5.f, 5.f));
				cube2->SetPosition(cube2Position);

				//cubeが動いてないかつcubeとcube2の色が違うとき
				if (!cube->GetMove() && cube->GetType() != cube2->GetType())
				{
					cube->SetColor(cube2->GetColor());
					cube->SetChange(true);
					cubeChange = true;

					if (cube2->GetColor().x == 1)
					{
						for (int i = 0; i < 70; i++)
						{
							pParticleManager->Add_Board(std::make_shared<ParticlePut_Red>(), pParticleManager->smoke, cube->GetPosition());

						}
					}
					else
					{
						for (int i = 0; i < 70; i++)
						{
							pParticleManager->Add_Board(std::make_shared<ParticlePut_Blue>(), pParticleManager->smoke, cube->GetPosition());

						}
					}
				}

				cube2->Update();
				break;
			}
			cube2No++;
		}
		cube2No = 0;


		cube1No++;

	}

}
//エディターの当たり判定
void ObjManager::EditorHit(const DirectX::XMFLOAT3 & playerMin, const DirectX::XMFLOAT3 & playerMax, const DirectX::XMFLOAT3 & player2Min, const DirectX::XMFLOAT3 & player2Max)
{
	DirectX::XMFLOAT3  cubeMin, cubeMax, editorMin, editorMax, out, out2;
	float out3, niar;
	int num = -1, floorNo = 0;

	if (editorObj != nullptr)
	{
		if (editorObj->GetColor().y == 0.f)
		{
			editorMin = DirectX::XMFLOAT3(editorObj->GetPosition().x - 4.5f*editorObj->GetScale().x, editorObj->GetPosition().y, editorObj->GetPosition().z - 4.5f*editorObj->GetScale().z);
			editorMax = DirectX::XMFLOAT3(editorObj->GetPosition().x + 4.5f*editorObj->GetScale().x, editorObj->GetPosition().y + 10.f*editorObj->GetScale().y, editorObj->GetPosition().z + 4.5f*editorObj->GetScale().z);

			for (auto&stage : stageObj)
			{
				stage->Update();
				cubeMin = DirectX::XMFLOAT3(stage->GetPosition().x - 5.f*stage->GetScale().x, stage->GetPosition().y, stage->GetPosition().z - 5.f*stage->GetScale().z);
				cubeMax = DirectX::XMFLOAT3(stage->GetPosition().x + 5.f*stage->GetScale().x, stage->GetPosition().y + 10.f*stage->GetScale().y, stage->GetPosition().z + 5.f*stage->GetScale().z);
				if (IsHitCube(editorMin, editorMax, cubeMin, cubeMax))
				{
					stage->SetExist(false);
					editorObj->SetColor(DirectX::XMFLOAT4(1.f, 1.f, 1.f, 0.5f));
					break;
				}

			}
			for (auto&stage : cubeObj)
			{
				stage->Update();
				cubeMin = DirectX::XMFLOAT3(stage->GetPosition().x - 5.f*stage->GetScale().x, stage->GetPosition().y, stage->GetPosition().z - 5.f*stage->GetScale().z);
				cubeMax = DirectX::XMFLOAT3(stage->GetPosition().x + 5.f*stage->GetScale().x, stage->GetPosition().y + 10.f*stage->GetScale().y, stage->GetPosition().z + 5.f*stage->GetScale().z);
				if (IsHitCube(editorMin, editorMax, cubeMin, cubeMax))
				{
					if (editorObj->GetColor().y == 0.f)
					{
						stage->SetExist(false);
						editorObj->SetColor(DirectX::XMFLOAT4(1.f, 1.f, 1.f, 0.5f));
						break;
					}
				}

			}

			for (auto&floor : floorObj)
			{
				if (floor->RayPick(DirectX::XMFLOAT3(editorObj->GetPosition().x, editorObj->GetPosition().y + 10.f, editorObj->GetPosition().z), DirectX::XMFLOAT3(editorObj->GetPosition().x, editorObj->GetPosition().y - 10.f, editorObj->GetPosition().z), &out, &out2, &out3) != -1)
				{
					if (num != -1)
					{
						if (niar > out3)
						{
							num = floorNo;
							niar = out3;
						}
					}
					else
					{
						num = floorNo;
						niar = out3;
					}
				}
				floorNo++;
			}
		}
		if (num != -1)
		{
			if (editorObj->GetColor().y == 0.f)floorObj.at(num)->SetExist(false);
		}
		editorObj->Update();
		editorObj->SetColor(DirectX::XMFLOAT4(1.f, 1.f, 1.f, 0.5f));
	}

}
//更新
void ObjManager::Update()
{
	cubeChange = false;
	Vectordelete(stageObj);
	DirectX::XMFLOAT3  p1_position;
	DirectX::XMFLOAT3 player1Min, player1Max, player2Min, player2Max, cubeMin, cubeMax, cube2Min, cube2Max;
	//playerの当たり判定エリア設定
	if (player2Obj != nullptr)
	{
		player2Obj->CreateHitArea();
		player2Min = player2Obj->GetHitMin();
		player2Max = player2Obj->GetHitMax();
	}
	if (player1Obj != nullptr)
	{
		player1Obj->CreateHitArea();
		player1Min = player1Obj->GetHitMin();
		player1Max = player1Obj->GetHitMax();

	}
#ifdef USE_IMGUI
	ImGui::Begin("player1Obj");
	ImGui::Text("%d", player1Obj->GetCarryCube());
	ImGui::Text("hitMin x:%f y:%f z:%f", player1Obj->GetPosition().x - player1Min.x, player1Obj->GetPosition().y - player1Min.y, player1Obj->GetPosition().z - player1Min.z);
	ImGui::Text("hitMax x:%f y:%f z:%f", player1Max.x - player1Obj->GetPosition().x, player1Max.y - player1Obj->GetPosition().y, player1Max.z - player1Obj->GetPosition().z);
	ImGui::End();
#endif
	//ステージOBJとの当たり判定
	StageHit(player1Min, player1Max, player2Min, player2Max);
	if (player2Obj != nullptr)
	{
		if (player2Obj->GetCarryCube() != -1)
		{
			DirectX::XMFLOAT3 position = player2Obj->GetPosition();
			DirectX::XMFLOAT3 scale = player2Obj->GetScale();
			player2Min = DirectX::XMFLOAT3(position.x - 3.f*scale.x, position.y, position.z - 3.f*scale.z);
			player2Max = DirectX::XMFLOAT3(position.x + 3.f*scale.x, position.y + 6.f*scale.y, position.z + 3.f*scale.z);
		}
	}
	if (player1Obj != nullptr)
	{
		if (player1Obj->GetCarryCube() != -1)
		{
			DirectX::XMFLOAT3 position = player1Obj->GetPosition();
			DirectX::XMFLOAT3 scale = player1Obj->GetScale();
			player1Min = DirectX::XMFLOAT3(position.x - 3.f*scale.x, position.y, position.z - 3.f*scale.z);
			player1Max = DirectX::XMFLOAT3(position.x + 3.f*scale.x, position.y + 6.f*scale.y, position.z + 3.f*scale.z);
		}

	}

	//Cubeとの当たり判定	
	CubeHit(player1Min, player1Max, player2Min, player2Max);
	//エディターObjとの当たり判定
	EditorHit(player1Min, player1Max, player2Min, player2Max);
	//cubeの連鎖
	if (cubeChange)
	{
		bool end = false;
		while (1)
		{


			int cubeNo = 0, cube2No = 0;
			for (auto&cube : cubeObj)
			{
				if (player2Obj->GetCarryCube() == cubeNo || player1Obj->GetCarryCube() == cubeNo)
				{
					cubeNo++;
					continue;
				}
				cubeMin = DirectX::XMFLOAT3(cube->GetPosition().x - 5.f*cube->GetScale().x*1.1f, cube->GetPosition().y, cube->GetPosition().z - 5.f*cube->GetScale().z*1.1f);
				cubeMax = DirectX::XMFLOAT3(cube->GetPosition().x + 5.f*cube->GetScale().x*1.1f, cube->GetPosition().y + 5.f*4.f*cube->GetScale().y, cube->GetPosition().z + 5.f*cube->GetScale().z*1.1f);
				cube2No = 0;
				for (auto&cube2 : cubeObj)
				{
					if (player2Obj->GetCarryCube() == cube2No || player1Obj->GetCarryCube() == cube2No)
					{
						cube2No++;
						continue;
					}
					//if (cube->GetType() == cube2->GetType())
					//{
					//	cube2No++;
					//	continue;
					//}
					if (cube->GetChange() || cube2->GetChange())
					{
						cube2Min = DirectX::XMFLOAT3(cube2->GetPosition().x - 5.f*cube2->GetScale().x*1.1f, cube2->GetPosition().y, cube2->GetPosition().z - 5.f*cube2->GetScale().z*1.1f);
						cube2Max = DirectX::XMFLOAT3(cube2->GetPosition().x + 5.f*cube2->GetScale().x*1.1f, cube2->GetPosition().y + 10.f*cube2->GetScale().y, cube2->GetPosition().z + 5.f*cube2->GetScale().z*1.1f);
						if (IsHitCube(cube2Min, cube2Max, cubeMin, cubeMax))
						{
							if (cube->GetChange() && !cube2->GetChange())
							{
								cube2->SetColor(cube->GetColor());
								cube2->SetChange(true);
								cube2->SetType(cube->GetType());
							}
							else if (!cube->GetChange() && cube2->GetChange())
							{
								cube->SetColor(cube2->GetColor());
								cube->SetChange(true);
								cube->SetType(cube2->GetType());
							}
						}
					}
					cube2No++;
				}
				cubeNo++;
			}
			if (!end)break;
		}

	}
	if (!player1Obj->GetExist() || !player2Obj->GetExist())
	{
		pScreenOut.ScreenOutStart();
	}
	//playerなどの更新
	if (player2Obj != nullptr)
	{
		player2Obj->Update();
		player2Obj->Animation(player2Obj->GetAnimTime());
	}
	if (player1Obj != nullptr)
	{
		player1Obj->Update();
		player1Obj->Animation(player1Obj->GetAnimTime());
	}
	for (auto&floor : floorObj)
	{
		floor->Update();
	}
}
//ステージObjとのレイピック
int ObjManager::StageObjRayPic(const DirectX::XMFLOAT3 & ryaStart, const DirectX::XMFLOAT3 & ryaEnd, DirectX::XMFLOAT3 * outPosition, DirectX::XMFLOAT3 * outNormal, float*outDirection)
{
	int ret = -1, nowRet = -1;
	for (auto&stage : stageObj)
	{
		//stage->Update();
		nowRet = stage->RayPick(ryaStart, ryaEnd, outPosition, outNormal, outDirection);
		if (nowRet != -1)
		{
			ret = nowRet;
		}
	}
	return ret;
}
//cubeObjとのレイピック
int ObjManager::CubeObjRayPic(const DirectX::XMFLOAT3 & ryaStart, const DirectX::XMFLOAT3 & ryaEnd, DirectX::XMFLOAT3 * outPosition, DirectX::XMFLOAT3 * outNormal, float * outDirection,int t)
{
	int ret = 0, nowRet = -1, cuneNum = -1;
	for (auto&cube : cubeObj)
	{
		cube->Update();
		if (cube->GetType() == t||player2Obj->GetCarryCube()== ret)
		{
			ret++;
			continue;
		}
		nowRet = cube->RayPick(ryaStart, ryaEnd, outPosition, outNormal, outDirection);
		if (nowRet != -1)cuneNum = ret;
		ret++;
	}
	return cuneNum;
}
//描画
void ObjManager::Render(ID3D11DeviceContext * devicecontext, const DirectX::XMFLOAT4X4&view, const DirectX::XMFLOAT4X4&projection)
{
	modelRenderer->Begin(devicecontext,
		view,
		projection,
		DirectX::XMFLOAT4(0, -1, 0.5f, 1.f)	// ライトの向き
	);
	for (auto&&s_obj : stageObj)
	{
		s_obj->Render(modelRenderer, devicecontext);
	}
	for (auto&c_obj : cubeObj)
	{
		c_obj->Render(modelRenderer, devicecontext);
	}
	for (auto&floor : floorObj)
	{
		floor->Render(modelRenderer, devicecontext);
	}
	if (editorObj != nullptr)editorObj->Render(modelRenderer, devicecontext);
	if (player1Obj != nullptr)player1Obj->Render(modelRenderer, devicecontext);
	if (player2Obj != nullptr)player2Obj->Render(modelRenderer, devicecontext);
	modelRenderer->End(devicecontext);

}
