#pragma once

#include"model.h"
#include"model_renderer.h"
class Obj3D
{
public:
	operator const bool() const { return mExist; };

	Obj3D(std::shared_ptr<ModelResource>model_resource);
	//更新
	void Update();
	//レイピック
	int RayPick(const DirectX::XMFLOAT3 & startPosition, const DirectX::XMFLOAT3 & endPosition, DirectX::XMFLOAT3 * outPosition, DirectX::XMFLOAT3 * outNormal,float*outDistance);
	//アニメーション開始
	void Play(int anim_num, bool loop = false) { mObj->PlayAnimation(anim_num, loop); }
	//アニメーション更新
	void Animation(float anim_time) { mObj->UpdateAnimation(anim_time); }
	//アニメーション終了フラグ取得
	const bool GetEndAnimation() const { return mObj->IsPlayAnimation(); }
	//ゲッター
	const DirectX::XMFLOAT3&GetPosition()const { return mPosition; }
	const DirectX::XMFLOAT3&GetScale()const { return mScale; }
	const DirectX::XMFLOAT3&GetAngle()const { return mAngle; }
	const DirectX::XMFLOAT4X4&GetWorld()const { return mWorld; }
	const DirectX::XMFLOAT4&GetColor()const { return mColor; }
	const bool&GetExist() const { return mExist; }
	const bool&GetLeft()const { return turn.left; }
	const bool&GetRight()const { return turn.right; }
	const bool&GetFront()const { return turn.front; }
	const bool&GetBack()const { return turn.back; }
	//セッター
	void SetPosition(const DirectX::XMFLOAT3&position) { mPosition = position; }
	void SetScale(const DirectX::XMFLOAT3&scale) { mScale = scale; }
	void SetAngle(const DirectX::XMFLOAT3&angle) { mAngle = angle; }
	void SetColor(const DirectX::XMFLOAT4&color) { mColor = color; }
	void SetExist(bool exist) { mExist = exist; }
	void SetLeft(bool left) { turn.left = left; }
	void SetRight(bool right) { turn.right = right; }
	void SetFront(bool front) { turn.front = front; }
	void SetBack(bool back) { turn.back = back; }
	//描画
	void Render(std::shared_ptr<ModelRenderer>model_renderer, ID3D11DeviceContext* devicecontext);
private:
	struct TURN
	{
		bool left;
		bool right;
		bool front;
		bool back;
	};
	TURN turn;
	DirectX::XMFLOAT3 mPosition;
	DirectX::XMFLOAT3 mScale;
	DirectX::XMFLOAT3 mAngle;
	DirectX::XMFLOAT4X4 mWorld;
	DirectX::XMFLOAT4 mColor;
	std::unique_ptr<Model>mObj;
	bool mExist;
};