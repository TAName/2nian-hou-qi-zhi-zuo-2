#pragma once
#include"scene.h"
#include<memory>
#include"skinned_mesh.h"
#include <thread>
#include <mutex>
#include"sprite.h"
#include"player.h"
#include "fbx_load.h"
#include"model_renderer.h"
#include "Board.h"

#include "gamepad.h"


class Scene_Title :public Scene
{
	float i = 0;
	std::shared_ptr<ModelRenderer>modelRenderer;
	std::unique_ptr<Sprite> particle;

	DirectX::XMFLOAT3 tstpos;
	float tstscale;
	DirectX::XMFLOAT2 title_size;
	
	bool selectflag;
	DirectX::XMFLOAT2  selectscale;
	bool feadflag;

	enum  class Mode //sprite 追加
	{
		start,
		select,
		solo,
		duo,
	}mode ;
	
	

	std::unique_ptr<Sprite> bg;
	std::unique_ptr<Sprite>	title;

	std::unique_ptr<Sprite> push;
	std::unique_ptr<Sprite> batu;
	std::unique_ptr<Sprite> solo;
	std::unique_ptr<Sprite> duo;
	


	std::unique_ptr<PlayerObj> player;
	std::unique_ptr<PlayerObj> player2;

	//アニメーション
	int m_currentAnimation;
	bool playsound;
	int animTime;
	int anim;
public:
	//Now Loading
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;


	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}


	Scene_Title(ID3D11Device*device);
	void Move();
	int Update(float elapsed_time);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
	~Scene_Title();
};
