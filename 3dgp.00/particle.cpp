#include "Particle.h"
#include"framework.h"
ParticleManager*ParticleManager::instance = nullptr;
void Particle_obj::Render(ID3D11DeviceContext * immediate_context, const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, const DirectX::XMFLOAT4 & light_dir)
{
	DirectX::XMMATRIX W;
	{

		DirectX::XMMATRIX s, r, t;
		s = DirectX::XMMatrixScaling(scale, scale, scale);//スケール行列
		r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
		t = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);//移動行列

		W = s*r*t;
	}

	DirectX::XMFLOAT4X4 v, p, wvp, w;
	DirectX::XMStoreFloat4x4(&v, view);
	DirectX::XMStoreFloat4x4(&p, projection);
	DirectX::XMStoreFloat4x4(&wvp, W*view*projection);
	DirectX::XMStoreFloat4x4(&w, W);

	obj->render(immediate_context, color, light_dir, wvp, w);

}
void Particle_board::Render(ID3D11DeviceContext * immediate_context, const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, const DirectX::XMFLOAT4 & light_dir)
{


	DirectX::XMFLOAT4X4 v, p;
	DirectX::XMStoreFloat4x4(&v, view);
	DirectX::XMStoreFloat4x4(&p, projection);


	board->render(immediate_context, pos, scale, v, p, color);

}
void ParticleManager::Init(ID3D11Device *device)
{
	particleObjList.clear();
	particleBoardList.clear();

	obj.push_back(std::make_shared<geometric_Cube>(device));
	obj.push_back(std::make_shared<geometricSphere>(device, 32, 16));
	board.push_back(std::make_shared <Board>(device, L"Data/image/STAR2.png", false));
	board.push_back(std::make_shared <Board>(device, L"Data/image/playereffect1.png", false));
	board.push_back(std::make_shared <Board>(device, L"Data/image/playereffect3.png", false));
	board.push_back(std::make_shared <Board>(device, L"Data/image/particle-smoke.png", false));

	Load();
}

void ParticleManager::Update()
{
#ifdef USE_IMGUI
	ImGui::Begin("Particle ");

	if (ImGui::Button("Set_obj"))
	{
		Add_Obj(std::make_shared<ParticletstObj>(), geo::cube, DirectX::XMFLOAT3(0, 0, 0));
	}
	if (ImGui::Button("Set_board"))
	{
		for (int i = 0; i < Data.amount; i++)
		{
			Add_Board(std::make_shared<ParticlePut_Blue>(), tex::smoke, DirectX::XMFLOAT3(0, 0, 0));
		}

	}

	if (ImGui::Button("save"))
	{
		FILE *fp;
		fopen_s(&fp, "Data/file/Particle2.bin", "wb");

		fwrite(&Data, sizeof(ParticleData), 1, fp);

		fclose(fp);

	}
	if (ImGui::Button("Load"))
	{
		Load();
	}

	ImGui::Text("color: (%f)", Data.color.w);
	ImGui::SliderFloat("color", &Data.color.w, 0, 1);
	ImGui::SliderFloat("speed.x", &Data.speed.x, 0, 40);
	ImGui::SliderFloat("v_x", &Data.v.x, -0.5f, 5.8f);
	ImGui::SliderFloat("v_y", &Data.v.y, -0.8f, 5.8f);
	ImGui::SliderFloat("v_z", &Data.v.z, -0.8f, 5.8f);
	ImGui::SliderFloat("a_x", &Data.accel.x, -0.1f, 0.1f);
	ImGui::SliderFloat("a_y", &Data.accel.y, -0.1f, 0.1f);
	ImGui::SliderFloat("a_z", &Data.accel.z, -0.1f, 0.1f);
	ImGui::SliderInt("amount", &Data.amount, 0, 200);
	ImGui::SliderInt("time_MAX", &Data.dispMaxTimer, 0, 200);

	ImGui::End();
#endif
	for (auto & it : particleObjList)
	{
		it->Update();
	}
	for (auto & it : particleBoardList)
	{
		it->Update();
	}



	auto eraser_obj = [](const std::shared_ptr<Particle_obj>& p)
	{
		if (!p->exist) return true;
		return false;
	};
	particleObjList.erase(std::remove_if(particleObjList.begin(), particleObjList.end(), eraser_obj), particleObjList.end());

	auto eraser_board = [](const std::shared_ptr<Particle_board>& p)
	{
		if (!p->exist) return true;
		return false;
	};
	particleBoardList.erase(std::remove_if(particleBoardList.begin(), particleBoardList.end(), eraser_board), particleBoardList.end());
}
void ParticleManager::Load()
{
	FILE *fp;
	if (fopen_s(&fp, "Data/file/Particle2.bin", "rb") == 0)//ファイルがあれば
	{
		int size = 0;
		fread(&Data, sizeof(ParticleData), 1, fp);
		fclose(fp);
	}
}
void ParticleManager::Render(ID3D11DeviceContext * immediate_context, const DirectX::XMMATRIX & view, const DirectX::XMMATRIX & projection, const DirectX::XMFLOAT4 & light_dir)
{

	blender::Set(blender::ADD, immediate_context);

	for (auto& it : particleObjList)
	{

		it->Render(immediate_context, view, projection, light_dir);
	}

	for (auto& it : particleBoardList)
	{

		it->Render(immediate_context, view, projection, light_dir);
	}
}

bool ParticleManager::Add_Obj(std::shared_ptr<Particle_obj> p, geo i, const DirectX::XMFLOAT3 & pos)
{



	p->Init(pos, obj[static_cast<int>(i)]);

	particleObjList.emplace_back(p);
	return true;
}
bool ParticleManager::Add_Board(std::shared_ptr<Particle_board> p, tex i, const DirectX::XMFLOAT3 & pos)
{

	p->Init(pos, board[static_cast<int>(i)]);

	particleBoardList.emplace_back(p);
	return true;
}
void ParticletstObj::Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<geometric_primitive> obj)
{
	this->obj = obj;
	exist = true;
	scale = 40.0f;
	color = pParticleManager->Data.color;
	this->pos = pos;
	speed = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f);
	angle = DirectX::XMFLOAT3(0.f, 0.0f, 1.f);
	dispMaxTimer = 40;
	dispTimer = 0;
}
void ParticletstObj::Update()
{
	pos.x += speed.x;
	pos.y += speed.y;
	pos.z += speed.z;

	dispTimer++;
	if (dispTimer >= dispMaxTimer)	exist = false;
}

void ParticlePut_Blue::Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<Board> board)
{
	exist = true;

	this->board = board;


	w = 1.0f;
	color = DirectX::XMFLOAT4(0.5f, 1.0f, 1.0f, w);
	scale = 9.545f;

	//blender::Set(blender::NONE);

	angle = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
	int a = rand() % 2;
	if (a == 0)
	{
		this->pos.x = pos.x + 1;
	}
	else
	{
		this->pos.x = pos.x - 1;
	}
	int b = rand() % 2;
	if (b == 0)
	{
		this->pos.z = -pos.z + 1;
	}
	else
	{
		this->pos.z = -pos.z - 1;
	}
	
	this->pos.y = pos.y;
	

	speed.x = ((rand() % 2001) - 1000) * 0.001f *0.645f;
	speed.z = ((rand() % 2001) - 1000) * 0.001f *0.400f;
	speed.y = ((rand() % 2001) - 1000) * 0.001f *0.500f;



	accel.x = ((rand() % 2001) - 1000) * 0.001f*0.014f;
	accel.y = ((rand() % 2001) - 1000) * 0.001f*0.014f;
	accel.z = ((rand() % 2001) - 1000) * 0.001f*0.011f;



	dispTimer = 0;
	dispMaxTimer = 27;


}
void ParticlePut_Blue::Update()
{
	int a = rand() % 2;
	if (a == 0)
	{
		speed.x += accel.x;
		speed.y -= accel.y;
		speed.z += accel.z;
	}
	else
	{
		speed.x -= accel.x;
		speed.y += accel.y;
		speed.z -= accel.z;
	}
	pos.x += speed.x;
	pos.y += speed.y;
	pos.z += speed.z;

	scale = InSine(static_cast<float>(dispTimer), static_cast<float>(dispMaxTimer), 0.f, 9.545f);
	w = OutQuad(static_cast<float>(dispTimer), static_cast<float>(dispMaxTimer), 0.f, 1.0f);
	color.w = w;



	dispTimer++;

	if (dispTimer >= dispMaxTimer)	exist = false;
}

void ParticlePut_Red::Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<Board> board)
{
	exist = true;

	this->board = board;


	w = 1.0f;
	color = DirectX::XMFLOAT4(1.0f, 0.5f, 1.0f, w);
	scale = 9.545f;


	angle = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
	int a = rand() % 2;
	if (a == 0)
	{
		this->pos.x = pos.x + 1;
	}
	else
	{
		this->pos.x = pos.x - 1;
	}
	int b = rand() % 2;
	if (b == 0)
	{
		this->pos.z = -pos.z + 1;
	}
	else
	{
		this->pos.z = -pos.z - 1;
	}
	
	this->pos.y = pos.y;
	
	speed.x = ((rand() % 2001) - 1000) * 0.001f *0.645f;
	speed.z = ((rand() % 2001) - 1000) * 0.001f *0.400f;
	speed.y = ((rand() % 2001) - 1000) * 0.001f *0.500f;



	accel.x = ((rand() % 2001) - 1000) * 0.001f*0.014f;
	accel.y = ((rand() % 2001) - 1000) * 0.001f*0.014f;
	accel.z = ((rand() % 2001) - 1000) * 0.001f*0.011f;



	dispTimer = 0;
	dispMaxTimer = 27;


}
void ParticlePut_Red::Update()
{
	int a = rand() % 2;
	if (a == 0)
	{
		speed.x += accel.x;
		speed.y -= accel.y;
		speed.z += accel.z;
	}
	else
	{
		speed.x -= accel.x;
		speed.y += accel.y;
		speed.z -= accel.z;
	}
	pos.x += speed.x;
	pos.y += speed.y;
	pos.z += speed.z;

	scale = InSine(static_cast<float>(dispTimer), static_cast<float>(dispMaxTimer), 0.f, 9.545f);
	w = OutQuad(static_cast<float>(dispTimer), static_cast<float>(dispMaxTimer), 0.f, 1.0f);
	color.w = w;



	dispTimer++;

	if (dispTimer >= dispMaxTimer)	exist = false;
}

void Particlebreak::Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<Board> board)
{
	exist = true;

	this->board = board;

	scale = 4;
	w = 1.0f;
	color = DirectX::XMFLOAT4(1.0f, 1.f, 1.f, w);

	angle = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
	int a = rand() % 2;
	float g = 1.0f;
	if (a == 0)
	{
		this->pos.x = pos.x + g;
	}
	else
	{
		this->pos.x = pos.x - g;
	}
	int b = rand() % 2;
	if (b == 0)
	{
		this->pos.z = -pos.z + g;
	}
	else
	{
		this->pos.z = -pos.z - g;
	}
	//this->pos.x = pos.x;
	this->pos.y = pos.y;
	//this->pos.z = -pos.z;

	speed.x = ((rand() % 2001) - 1000) * 0.001f *pParticleManager->Data.v.x;
	speed.z = ((rand() % 2001) - 1000) * 0.001f *pParticleManager->Data.v.y;
	speed.y = ((rand() % 2001) - 1000) * 0.001f *pParticleManager->Data.v.z;

	scale_max = scale;
	accel.x = ((rand() % 2001) - 1000) * 0.001f*pParticleManager->Data.accel.x;
	accel.y = ((rand() % 2001) - 1000) * 0.001f*pParticleManager->Data.accel.y;
	accel.z = ((rand() % 2001) - 1000) * 0.001f*pParticleManager->Data.accel.z;


	dispTimer =0;
	dispMaxTimer = pParticleManager->Data.dispMaxTimer;
}
void Particlebreak::Update()
{
	/*speed.x -= accel.x;
	speed.y -= accel.y;
	speed.z -= accel.z;
	pos.x += speed.x;
	pos.y += speed.y;
	pos.z += speed.z;*/
	int a = rand() % 2;
	if (a == 0)
	{
		speed.x += accel.x;
		speed.y -= accel.y;
		speed.z += accel.z;
	}
	else
	{
		speed.x -= accel.x;
		speed.y += accel.y;
		speed.z -= accel.z;
	}
	pos.x += speed.x;
	pos.y += speed.y;
	pos.z += speed.z;
	//	float s = scale;
	scale = 2.f;
	//scale = InOutQuint(static_cast<float>(dispTimer), static_cast<float>(dispMaxTimer), 0.f, 4.0f);
	//scale = InBounce(static_cast<float>(dispTimer), 60.0f, 4.0f, 0.0f);
	float s;
	s = InQuad(static_cast<float>(dispTimer), static_cast<float>(dispMaxTimer), 0.f, 1.0f);
	w = InQuad(static_cast<float>(dispTimer), static_cast<float>(dispMaxTimer), 0.f, 1.0f);

	color.y = s;

	color.w = w;



	dispTimer++;

	if (dispTimer >= dispMaxTimer)	exist = false;
}
//void Particlebreak::Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<Board> board)
//{
//	exist = true;
//
//	this->board = board;
//
//	scale = 4;
//	w = 1.0f;
//	color = DirectX::XMFLOAT4(1.0f, 1.f, 1.f, w);
//
//	static const float	MUZZLE_SPEED = 0.4f;
//
//	blender::Set(blender::SUBTRACT);
//
//	angle = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
//	int a = rand() % 2;
//	float g = 1.0f;
//	if (a == 0)
//	{
//		this->pos.x = pos.x + g;
//	}
//	else
//	{
//		this->pos.x = pos.x - g;
//	}
//	int b = rand() % 2;
//	if (b == 0)
//	{
//		this->pos.z = -pos.z + g;
//	}
//	else
//	{
//		this->pos.z = -pos.z - g;
//	}
//	this->pos.x = pos.x;
//	this->pos.y = pos.y;
//	this->pos.z = -pos.z;
//
//	speed.x = ((rand() % 2001) - 1000) * 0.001f *pParticleManager->Data.v.x;
//	speed.z = ((rand() % 2001) - 1000) * 0.001f *pParticleManager->Data.v.y;
//	speed.y = ((rand() % 2001) - 1000) * 0.001f *pParticleManager->Data.v.z;
//
//	scale_max = scale;
//	accel.x = ((rand() % 2001) - 1000) * 0.001f;
//	accel.y = ((rand() % 2001) - 1000) * 0.001f;
//	accel.z = ((rand() % 2001) - 1000) * 0.001f;
//
//
//	dispTimer = pParticleManager->Data.dispTimer;
//	dispMaxTimer = pParticleManager->Data.dispMaxTimer;
//}
//void Particlebreak::Update()
//{
//	/*speed.x -= accel.x;
//	speed.y -= accel.y;
//	speed.z -= accel.z;
//	pos.x += speed.x;
//	pos.y += speed.y;
//	pos.z += speed.z;*/
//	int a = rand() % 2;
//	if (a == 0)
//	{
//		speed.x += accel.x;
//		speed.y += accel.y;
//		speed.z += accel.z;
//	}
//	else
//	{
//		speed.x -= accel.x;
//		speed.y += accel.y;
//		speed.z -= accel.z;
//	}
//	pos.x += speed.x;
//	pos.y += speed.y;
//	pos.z += speed.z;
//	
//	scale = InOutQuint(static_cast<float>(dispTimer), static_cast<float>(dispMaxTimer), 0.f, 4.0f);
//	scale = InBounce(static_cast<float>(dispTimer), 60.0f, 4.0f, 0.0f);
//	float s;
//	s = InQuad(static_cast<float>(dispTimer), static_cast<float>(dispMaxTimer), 0.f, 1.0f);
//
//	color.y = s;
//	color.x = s;
//	color.z = s;
//	color.w = w;
//
//
//
//	dispTimer++;
//
//	if (dispTimer >= dispMaxTimer)	exist = false;
//}