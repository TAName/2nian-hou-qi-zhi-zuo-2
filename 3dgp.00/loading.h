#pragma once
#include"sprite.h"
#include<memory>
class Loading
{
public:
	Loading(ID3D11Device*device, const wchar_t*textname,const wchar_t*loadingname,const wchar_t*buttonname);
	bool NowLoadingUpdate(bool isNowLoadingFlag);
	bool NowLoadingRender(bool isNowLoadingFlag, ID3D11DeviceContext* devicecontext);
private:
	bool loadEnd;
	std::unique_ptr<Sprite>text;
	std::unique_ptr<Sprite>load;
	std::unique_ptr<Sprite>button;
	int timer;
	int anim;
	int textNo;
};