#include "stage.h"
#include "fbx_load.h"
#include "objmanager.h"
#include"vectordelete.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif
StageManager*StageManager::mStage = nullptr;
StageManager::StageManager(ID3D11Device *device)
{
	const char *filename[] = { "Data/FBX/floor_03_c.fbx","Data/FBX/blockOut_03_c.fbx" ,"Data/FBX/blockIn_03_c.fbx" ,"Data/FBX/blockThrow1.fbx" };
	for (int i = 0; i < 4; i++)
	{
		std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
		FbxLoader fbx_loader;
		fbx_loader.Load(filename[i], *model_data);

		modelResource.emplace_back(std::make_shared<ModelResource>(device, std::move(model_data)));
	}
	filenumber = 1;
	tstPos = DirectX::XMFLOAT3(0, 0, 0);
#ifdef USE_IMGUI
	tstObj = std::make_shared<Obj3D>(modelResource[1]);

	pObjManager.SetEditor(tstObj);
	tstObj->SetAngle(DirectX::XMFLOAT3(0, 0, 0));
	tstObj->SetColor(DirectX::XMFLOAT4(1, 1, 1, 0.5f));
	tstObj->SetScale(DirectX::XMFLOAT3(1.0, 1.5, 1.0));
	tstObj->SetPosition(tstPos);

	plusX = 0;
	plusZ = 0;
	select = 0;
#endif
}
void StageManager::Update()
{

	for (auto&c : cube)
	{
		c.Update();
	}

	Editer();
}
void StageManager::EditorLoad()
{
#ifdef USE_IMGUI
	
	if (ImGui::Button("Load"))
	{
		FILE *fp;

		pObjManager.StageObjClear();
		pObjManager.CubeObjClear();
		pObjManager.floorObjClear();
	
		stage.clear();
		cube.clear();
		save.clear();
		saveCube.clear();
		std::string a = { "Data/file/Stage" };
		a += std::to_string(select);
		a += ".bin";
		
		if (fopen_s(&fp, a.data(), "rb") == 0)//ファイルがあれば
		{
			int size = 0;
			fread(&size, sizeof(int), 1, fp);

			if (size > 0)
			{

				save.resize(size);
				fread(&save.at(0), sizeof(SaveData), save.size(), fp);

				for (auto&s : save)
				{
					stage.emplace_back();
					stage.back().obj = std::make_shared<Obj3D>(modelResource[s.saveFile]);
					stage.back().filenumber = s.saveFile;
					if (s.saveFile != 0)
					{
						pObjManager.Set_StageObj(stage.back().obj);
						stage.back().obj->SetPosition(s.savePos);
					}
					else
					{
						pObjManager.Set_FloorObj(stage.back().obj);
						stage.back().obj->SetPosition(DirectX::XMFLOAT3(s.savePos.x, s.savePos.y, s.savePos.z));
					}
					stage.back().obj->SetAngle(DirectX::XMFLOAT3(0, 0, 0));
					stage.back().obj->SetColor(DirectX::XMFLOAT4(1, 1, 1, 1));
					stage.back().obj->SetScale(DirectX::XMFLOAT3(1, 1, 1));
					//stage.back().obj->SetPosition(s.savePos);
					stage.back().mExist = true;
				}
			}
			int sizeCube = 0;
			fread(&sizeCube, sizeof(int), 1, fp);

			if (sizeCube > 0)
			{
				saveCube.resize(sizeCube);
				fread(&saveCube.at(0), sizeof(SaveCube), saveCube.size(), fp);

				for (auto&ss : saveCube)
				{
					cube.emplace_back(modelResource[3], ss.savePos, ss.color);

				}

			}
			fclose(fp);
		}
		
		//pObjManager.SetEditor(tstObj);

	}
#endif
}
void StageManager::Save()
{
#ifdef USE_IMGUI
	if (ImGui::Button("save"))
	{
		FILE *fp;
		save.clear();
		saveCube.clear();

		std::string a = { "Data/file/Stage" };
		a += std::to_string(select);
		a += ".bin";

		fopen_s(&fp, a.data(), "wb");

		save.resize(stage.size());
		int size = static_cast<int>(save.size());
		fwrite(&size, sizeof(int), 1, fp);
		if (size > 0)
		{
			for (int i = 0; i < stage.size(); i++)
			{
				save.at(i).savePos = stage.at(i).obj->GetPosition();
				save.at(i).saveFile = stage.at(i).filenumber;
			}
			fwrite(&save.at(0), sizeof(SaveData), save.size(), fp);
		}


		saveCube.resize(cube.size());
		int sizeCube = static_cast<int>(saveCube.size());
		fwrite(&sizeCube, sizeof(int), 1, fp);
		if (sizeCube > 0)
		{
			for (int i = 0; i < cube.size(); i++)
			{
				saveCube.at(i).savePos = cube.at(i).GetPosition();
				saveCube.at(i).color = cube.at(i).GetColor();

			}
			fwrite(&saveCube.at(0), sizeof(SaveCube), saveCube.size(), fp);
		}
		fclose(fp);
	}
#endif
}
void StageManager::Editer()
{
	for (auto&a : stage)
	{
		if (!a.obj->GetExist())
		{
			a.mExist = false;
		}
	}
	Vectordelete(stage);
	for (auto&a : cube)
	{
		if (!a.GetObjExist())
		{
			a.SetExist(false);
		}
	}
	Vectordelete(cube);

#ifdef USE_IMGUI
	
	ImGui::Begin("Stage ");
	if (ImGui::CollapsingHeader("select"))
	{

		ImGui::RadioButton("Load00", &select, 0);
		ImGui::RadioButton("Load01", &select, 1);
		ImGui::RadioButton("Load02", &select, 2);
		ImGui::RadioButton("Load03", &select, 3);
		EditorLoad();
		Save();
	}
	if (ImGui::CollapsingHeader("Delete"))
	{
		if (ImGui::Button("delete"))//一個消し
		{
			tstObj->SetColor(DirectX::XMFLOAT4(1, 0, 0, 0.5f));

		}
		if (ImGui::Button("Allclear"))//全消去 
		{
			pObjManager.StageObjClear();

			pObjManager.Set_StageObj(tstObj);

			pObjManager.CubeObjClear();

			pObjManager.floorObjClear();
		}

	}
	if (ImGui::CollapsingHeader("set"))
	{
		if (ImGui::Button("Set_electrode_red"))
		{

			cube.emplace_back(modelResource[3], tstPos, DirectX::XMFLOAT4(1, 0, 0, 1));

		}
		if (ImGui::Button("Set_electrode_blue"))
		{

			cube.emplace_back(modelResource[3], tstPos, DirectX::XMFLOAT4(0, 0, 1, 1));
		}

		ImGui::SliderInt("plusX", &plusX, 0, 55);
		ImGui::SliderInt("plusZ", &plusZ, 0, 30);

		if (ImGui::Button("cleate_x"))//plusX分一定の間隔で作る
		{
			for (int i = 0; i < plusX; i++)
			{

				stage.emplace_back();
				stage.back().obj = std::make_shared<Obj3D>(modelResource[filenumber]);
				if (filenumber != 0)
				{
					pObjManager.Set_StageObj(stage.back().obj);
					stage.back().obj->SetPosition(DirectX::XMFLOAT3(tstPos.x - stage.back().obj->GetScale().x * 10 * i, tstPos.y, tstPos.z));
				}
				else
				{
					pObjManager.Set_FloorObj(stage.back().obj);
					stage.back().obj->SetPosition(DirectX::XMFLOAT3(tstPos.x - stage.back().obj->GetScale().x * 10 * i, tstPos.y-3, tstPos.z));
					
				}
				stage.back().obj->SetAngle(DirectX::XMFLOAT3(0, 0, 0));
				stage.back().obj->SetColor(DirectX::XMFLOAT4(1, 1, 1, 1));
				stage.back().obj->SetScale(DirectX::XMFLOAT3(1, 1, 1));
				//stage.back().obj->SetPosition(DirectX::XMFLOAT3(tstPos.x - stage.back().obj->GetScale().x * 10 * i, tstPos.y, tstPos.z));
				stage.back().filenumber = filenumber;
				stage.back().mExist = true;
			}
		}
		if (ImGui::Button("cleate_Z"))//plusZ分一定の間隔で作る
		{
			for (int i = 0; i < plusZ; i++)
			{

				stage.emplace_back();
				stage.back().obj = std::make_shared<Obj3D>(modelResource[filenumber]);
				if (filenumber != 0)
				{
					pObjManager.Set_StageObj(stage.back().obj);
					stage.back().obj->SetPosition(DirectX::XMFLOAT3(tstPos.x, tstPos.y, tstPos.z - stage.back().obj->GetScale().z * 10 * i));
				}
				else
				{
					pObjManager.Set_FloorObj(stage.back().obj);
					stage.back().obj->SetPosition(DirectX::XMFLOAT3(tstPos.x, tstPos.y-3, tstPos.z - stage.back().obj->GetScale().z * 10 * i));
				}
				stage.back().obj->SetAngle(DirectX::XMFLOAT3(0, 0, 0));
				stage.back().obj->SetColor(DirectX::XMFLOAT4(1, 1, 1, 1));
				stage.back().obj->SetScale(DirectX::XMFLOAT3(1, 1, 1));
				//stage.back().obj->SetPosition(DirectX::XMFLOAT3(tstPos.x, tstPos.y, tstPos.z - stage.back().obj->GetScale().z * 10 * i));
				stage.back().filenumber = filenumber;
				stage.back().mExist = true;
			}
		}

		if (ImGui::Button("Set_stage"))
		{
				stage.emplace_back();
				stage.back().obj = std::make_shared<Obj3D>(modelResource[filenumber]);
				if (filenumber != 0)
				{
					pObjManager.Set_StageObj(stage.back().obj);
					stage.back().obj->SetPosition(tstPos);
				}
				else
				{
					pObjManager.Set_FloorObj(stage.back().obj);
					stage.back().obj->SetPosition(DirectX::XMFLOAT3(tstPos.x,tstPos.y-3,tstPos.z));
				}
				stage.back().obj->SetAngle(DirectX::XMFLOAT3(0, 0, 0));
				stage.back().obj->SetColor(DirectX::XMFLOAT4(1, 1, 1, 1));
				stage.back().obj->SetScale(DirectX::XMFLOAT3(1, 1, 1));
				
				stage.back().filenumber = filenumber;
				stage.back().mExist = true;
		
		}

		if (ImGui::Button("-xxx"))
		{
			int stagesize = static_cast<int>(stage.size());
			if (stagesize > 0)
			{
				tstPos.x -= stage.back().obj->GetScale().x * 10;
			}
		}
		if (ImGui::Button("+xxx"))
		{
			int stagesize = static_cast<int>(stage.size());
			if (stagesize > 0)
			{
				tstPos.x += stage.back().obj->GetScale().x * 10;
			}
		}
		if (ImGui::Button("-zzz"))
		{
			int stagesize = static_cast<int>(stage.size());
			if (stagesize > 0)
			{
				tstPos.z -= stage.back().obj->GetScale().z * 10;
			}

		}
		if (ImGui::Button("+zzz"))
		{
			int stagesize = static_cast<int>(stage.size());
			if (stagesize > 0)
			{
				tstPos.z += stage.back().obj->GetScale().z * 10;
			}
		}

		if (ImGui::Button("floor"))
		{
			filenumber = 0;
		}
		if (ImGui::Button("out_cube"))
		{
			filenumber = 1;
		}
		if (ImGui::Button("in_cube"))
		{
			filenumber = 2;
		}
		
		tstObj->SetPosition(tstPos);

		ImGui::DragFloat("pos.x", &tstPos.x, 5.f);
		ImGui::DragFloat("pos.z", &tstPos.z, 5.f);
		ImGui::Text("%d", stage.size());
	}
	
	ImGui::End();
#endif


}

void StageManager::Load(const int&stageNo)
{
	FILE *fp;

	pObjManager.StageObjClear();
	pObjManager.CubeObjClear();
	pObjManager.floorObjClear();
	stage.clear();
	cube.clear();
	save.clear();
	saveCube.clear();
	std::string a = { "Data/file/Stage" };
	a += std::to_string(stageNo);
	a += ".bin";

	if (fopen_s(&fp, a.data(), "rb") == 0)//ファイルがあれば
	{
		int size = 0;
		fread(&size, sizeof(int), 1, fp);

		if (size > 0)
		{

			save.resize(size);
			fread(&save.at(0), sizeof(SaveData), save.size(), fp);

			for (auto&s : save)
			{
				stage.emplace_back();
				stage.back().obj = std::make_shared<Obj3D>(modelResource[s.saveFile]);
				stage.back().filenumber = s.saveFile;
				if (s.saveFile != 0)
				{
					pObjManager.Set_StageObj(stage.back().obj);
					stage.back().obj->SetPosition(s.savePos);
				}
				else
				{
					pObjManager.Set_FloorObj(stage.back().obj);
					stage.back().obj->SetPosition(DirectX::XMFLOAT3(s.savePos.x, s.savePos.y , s.savePos.z));
				}
				
				stage.back().obj->SetAngle(DirectX::XMFLOAT3(0, 0, 0));
				stage.back().obj->SetColor(DirectX::XMFLOAT4(1, 1, 1, 1));
				stage.back().obj->SetScale(DirectX::XMFLOAT3(1, 1, 1));
				//stage.back().obj->SetPosition(s.savePos);
				stage.back().mExist = true;
			}
		}
		int sizeCube = 0;
		fread(&sizeCube, sizeof(int), 1, fp);

		if (sizeCube > 0)
		{
			saveCube.resize(sizeCube);
			fread(&saveCube.at(0), sizeof(SaveCube), saveCube.size(), fp);

			for (auto&ss : saveCube)
			{
				cube.emplace_back(modelResource[3], ss.savePos, ss.color);

			}

		}
		fclose(fp);
	}
	select = stageNo;
}
