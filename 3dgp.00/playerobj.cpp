#include "playerobj.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif


PlayerObj::PlayerObj(std::shared_ptr<ModelResource> model_resource):Obj3D(model_resource)
{
	hitMin = DirectX::XMFLOAT3(0, 0, 0);
	hitMax = DirectX::XMFLOAT3(0, 0, 0);
	carryCube = -1;
	animTime = 1.f / 60.f;
	mHitLeft = false;
	mHitRight = false;
	mHitUp = false;
	mHitDown = false;
}


void PlayerObj::CreateHitArea()
{
	static float leng = 10.f;
	DirectX::XMFLOAT3 position = GetPosition();
	DirectX::XMFLOAT3 scale = GetScale();
	hitMin = DirectX::XMFLOAT3(position.x - 3.f*scale.x, position.y, position.z - 3.f*scale.z);
	hitMax = DirectX::XMFLOAT3(position.x + 3.f*scale.x, position.y + 6.f*scale.y, position.z + 3.f*scale.z);
	//if (carryCube == -1)return;
	//DirectX::XMFLOAT3 angle = GetAngle();

	//static const float a = 3.14f / 2.f;
	//int an = static_cast<int>(angle.y / a);
	//switch (an)
	//{
	//case 0:
	//	hitMax.z = position.z + leng*scale.z;
	//	break;					
	//case 1:						
	//	hitMax.x = position.x + leng*scale.x;
	//	break;					
	//case 2:						
	//	hitMin.z = position.z - leng*scale.z;
	//	break;					
	//case 3:
	//	hitMin.x = position.x - leng*scale.x;
	//	
	//	break;
	//}
}
