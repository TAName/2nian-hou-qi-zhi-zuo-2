#pragma once
#include"playerobj.h"
#include"player.h"
class OpponentPlayer
{
public:
	OpponentPlayer(ID3D11Device*device,int pType);
	void Update(float elapsed_time);
private:
	std::shared_ptr<PlayerObj>mObj;
	std::unique_ptr<Player>mPlayer;
	int state;
	DirectX::XMFLOAT3 mSpeed;
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 leng;
	DirectX::XMFLOAT3 sido;
	DirectX::XMFLOAT3 throwleng;
	int type;
	int targetNo;
	float angle;
	bool hit;
	bool rayHit;
	bool wallHit;
	bool animend;
	int playType;
	int moveTimer;
	bool turn;
	enum OPPONENT_MOVE
	{
		WAIT,
		TARGET,
		RUN,
		GET,
		THROW
	};
	bool cubeGetFlag;
	bool targetSetFlag;
	DirectX::XMFLOAT3 beforePosition;
	DirectX::XMFLOAT3 targetPosition;
	void CubeAvoidance();
	void CubeLockOn();
	void Wait();
	void Target();
	void SetSpeed(float leng,float*speed);
	void SetAngle(float increase, float speed);
	void MeasurementSpeed();
	void Run();
	void Get();
	void Throw();
};