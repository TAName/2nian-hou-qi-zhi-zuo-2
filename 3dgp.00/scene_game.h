#pragma once
#include"scene.h"
#include<memory>
#include"skinned_mesh.h"
#include <thread>
#include <mutex>
#include"sprite.h"
#include"obj3d.h"
#include"opponent_player.h"
#include"player.h"
#include"loading.h"
#include"stagestart.h"

class Scene_Game :public Scene
{
	std::unique_ptr<Loading> mLoad;
	std::shared_ptr<Obj3D>m_obj;
	std::shared_ptr<Obj3D>m_stage;
	std::unique_ptr<OpponentPlayer>mPlayer2;
	std::unique_ptr<Player>mPlayer1;
	std::unique_ptr<StageStart>start;
	std::unique_ptr<Sprite>background;
public:
	//Now Loading
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;


	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}
	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}

	Scene_Game(ID3D11Device*device);
	int Update(float elapsed_time);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
	~Scene_Game();
};
