#include "model.hlsli"

Texture2D diffuse_map : register(t0);
SamplerState diffuse_map_sampler_state : register(s0);
//ランバートシェーディング
float3 Diffuse(float3 N, float3 L, float3 C, float3 K)
{
	float D = dot(N, -L);
	D = max(0, D);			// 負の値を０にする
	return K * C * D;
}

float4 main(PSInput input) : SV_TARGET
{
	// 環境光
	float3 Ambient = AmbientColor.rgb;//
	float4 color = diffuse_map.Sample(diffuse_map_sampler_state, input.Tex);
	float3 C = LightColor.rgb;		//入射光(色と強さ)
	float3 L = normalize(LightDir.xyz);
	// 拡散反射
	float3 Kd = float3(1, 1, 1);
	float3 D = Diffuse(0.6, L, C, Kd);


	return color * input.Color*float4(Ambient + D, 1.0f);

}

