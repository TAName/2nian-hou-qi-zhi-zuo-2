#include "stagestart.h"
#include"gamepad.h"
#include"screenout.h"
#include"stage.h"
#include"soundmanager.h"
#include"camera.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif


StageStart::StageStart(ID3D11Device * device, const wchar_t * countfile, const wchar_t * selectfile, const wchar_t*buttontfile, const wchar_t*startfile)
{
	state = STARTSTATE::wait;
	fTimer = 4;
	selectNo = 0;
	countText = std::make_unique<Sprite>(device, countfile);
	selectText = std::make_unique<Sprite>(device, selectfile);
	buttonText = std::make_unique<Sprite>(device, buttontfile);
	startText = std::make_unique<Sprite>(device, startfile);
	position = DirectX::XMFLOAT2(200, 200);
	selecttimer = 0;
	buttontimer = 0;
	eye = DirectX::XMFLOAT3(0, 500, 100);
}

bool StageStart::Update(float elapsed_time, int fadoNo)
{
	bool set = true;
	switch (state)
	{
	case STARTSTATE::wait:
		state = STARTSTATE::select;
		break;
	case STARTSTATE::select:
		Select();
		break;
	case STARTSTATE::in:
		if (fadoNo == 1)
		{
			state = STARTSTATE::count;
			pSoundManager.Play(SoundManager::SOUND::Count);
			pSoundManager.SetVolume(SoundManager::SOUND::Count,3.0f);
		}
		break;
	case STARTSTATE::count:
		Count(elapsed_time);
		break;
	case STARTSTATE::play:
		fTimer -= elapsed_time;
		if (fTimer < 0)fTimer = 0;
		set = false;
		CameraEditor();
		break;
	}
	//mSound->Update();

	return set;
}

void StageStart::Render(ID3D11DeviceContext * devicecontext)
{
	switch (state)
	{
	case STARTSTATE::select:
		for (int i = 0; i < 4; i++)
		{
			if (i == selectNo)
			{
				if (selecttimer++ % 30 > 17)continue;
			}
			selectText->render(devicecontext, static_cast<float>(200+(800*(i%2))), static_cast<float>(100 + (425 * (i / 2))), 660, 340, static_cast<float>(1920*i), 0, 1920, 1080, 0, 1, 1, 1, 1);
		}
		if(buttontimer++% 30 > 17)buttonText->render(devicecontext, 1600, 900, 100, 100, 0, 0, 64, 64, 0, 1, 1, 1, 1);
		break;
	case STARTSTATE::count:
		countText->render(devicecontext, 880, 400, 200, 200, static_cast<float>(96 * (timer - 1)), 0, 96, 96, 0, 1, 1, 1, fTimer - static_cast<float>(timer));
		break;
	case STARTSTATE::play:
		startText->render(devicecontext, 680, 400, 600, 200, 0, 0, 690, 219, 0, 1, 1, 1, fTimer);
		break;
	}
}

void StageStart::Select()
{
	if (input::ButtonRisingState(0, input::PadLabel::UP))
	{
		if (selectNo >= 2)
		{
			selectNo -= 2;
		}
	}
	else if (input::ButtonRisingState(0, input::PadLabel::DOWN))
	{
		if (selectNo <= 1)
		{
			selectNo += 2;
		}
	}
	else if (input::ButtonRisingState(0, input::PadLabel::LEFT))
	{
		if (selectNo % 2 == 1)
		{
			selectNo--;
		}
	}
	else if (input::ButtonRisingState(0, input::PadLabel::RIGHT))
	{
		if (selectNo % 2 == 0)
		{
			selectNo++;
		}
	}

	if (input::ButtonRisingState(0, input::PadLabel::A))
	{
		
		state = STARTSTATE::in;
		pStageManager.Load(selectNo);
		pScreenOut.ScreenInStart();
		pSoundManager.Stop(SoundManager::SOUND::BGM_Select);
		pSoundManager.Play(SoundManager::SOUND::decision);
		pSoundManager.SetVolume(SoundManager::SOUND::decision, 1.0f);
		pSoundManager.Play(SoundManager::SOUND::BGM_Game,true);
		pSoundManager.SetVolume(SoundManager::SOUND::BGM_Game, 1.0f);
		LoadCamera();
	}
}

void StageStart::Count(float elapsed_time)
{
	fTimer -= elapsed_time;
	timer = static_cast<int>(fTimer);
	if (timer <= 0)
	{
		state = STARTSTATE::play;
		fTimer = 1.f;
	}
#ifdef USE_IMGUI
	ImGui::Begin("timer");
	ImGui::Text("timer%d", timer);
	ImGui::Text("timer%f", fTimer - timer);
	ImGui::End();
#endif
}

void StageStart::LoadCamera()
{
	FILE*fp;
	std::string camerafile = { "Data/file/camera" };
	camerafile += std::to_string(selectNo);
	camerafile += ".bin";
	if (fopen_s(&fp, camerafile.data(), "rb") == 0)
	{
		fread(&eye, sizeof(float), 3, fp);
		fclose(fp);
	}
	pCamera.SetEye(eye);
}

void StageStart::SaveCamera()
{
	FILE*fp;
	std::string camerafile = { "Data/file/camera" };
	camerafile += std::to_string(selectNo);
	camerafile += ".bin";
	fopen_s(&fp, camerafile.data(), "wb");
	fwrite(&eye, sizeof(float), 3, fp);
	fclose(fp);
}

void StageStart::CameraEditor()
{
#ifdef USE_IMGUI
	ImGui::Begin("camera");
	if (ImGui::SliderFloat("eye.x", &eye.x, 0, 1000) || ImGui::SliderFloat("eye.y", &eye.y, 0, 1000) || ImGui::SliderFloat("eye.z", &eye.z, 0, 1000))
	{
		pCamera.SetEye(eye);
	}
	if(ImGui::Button("save"))
	{
		SaveCamera();
	}
	ImGui::End();
#endif
}

