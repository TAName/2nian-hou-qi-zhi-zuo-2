#pragma once
#include"scene.h"
#include <thread>
#include <mutex>
#include"sprite.h"

class SceneManager
{
private:
	Scene*m_scene = nullptr;
	static SceneManager*m_scenemanager;
	SceneManager(ID3D11Device*device);

	enum SceneName
	{
		Title=1,
		Game,
		Result
	};
	void ChangScene(int next_scene, ID3D11Device * device);

public:

	static void Create(ID3D11Device*device)
	{
		if (m_scenemanager != nullptr)
			return;

		m_scenemanager = new SceneManager(device);
		
	}
	static SceneManager& GetInstance()
	{
		return *m_scenemanager;
	}
	static void Destroy()
	{
		if (m_scenemanager != nullptr)
		{
			delete m_scenemanager;
			m_scenemanager = nullptr;
		}
	}
	void Updata(float elapsed_time, ID3D11Device * device);
	void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
	~SceneManager() { 
		delete m_scene; 
	}
};
#define pSceneManager (SceneManager::GetInstance())