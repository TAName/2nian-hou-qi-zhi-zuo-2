#include <stdio.h> 

#include "static_mesh.h"
#include"WICTextureLoader.h"
#include "misc.h"

#include<fstream>
#include"ResourceManager.h"

static_mesh::static_mesh(ID3D11Device * device, wchar_t*file,bool vt_texcoord_flag)
{
	HRESULT hr = S_OK;
	std::vector<vertex> vertices;
	std::vector<u_int> indices;
	u_int current_index = 0;

	std::vector<DirectX::XMFLOAT3> positions;
	std::vector<DirectX::XMFLOAT3> normals;
	std::vector<DirectX::XMFLOAT2> texcoord;
	std::wifstream fin(file);
	u_int material_num = 0;
	_ASSERT_EXPR(fin, L"OBJ file not fuund");
	wchar_t file_name[256];

	wchar_t command[256];
	while (fin)
	{
		fin >> command;
		if (0 == wcscmp(command, L"mtllib"))
		{
			fin >> file_name;
			
			fin.ignore(1024, L'\n');
		}

		else if (0 == wcscmp(command, L"v"))
		{
			float x, y, z;
			fin >> x >> y >> z;
			positions.push_back(DirectX::XMFLOAT3(x, y, z));
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"vn"))
		{
			FLOAT i, j, k;
			fin >> i >> j >> k;
			normals.push_back(DirectX::XMFLOAT3(i, j, k));
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"vt"))
		{
			FLOAT t, e;
			fin >> t >> e;
			if(vt_texcoord_flag)texcoord.push_back(DirectX::XMFLOAT2(t, 1.0f-e));
			else texcoord.push_back(DirectX::XMFLOAT2(t, e));
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"usemtl"))
		{
			wchar_t s[256];
			fin >> s;
			subset sub;
			sub.usemtl = s;
			sub.mat_index = material_num;
			sub.index_count = 0;
			sub.index_start = current_index;
			subsets.push_back(sub);
			material_num++;
		}
		else if (0 == wcscmp(command, L"f"))
		{
			static u_int index = 0;
			subset*s = &subsets.back();
			for (u_int i = 0; i < 3; i++)
			{
				vertex vertex;
				u_int v, vt, vn;
				fin >> v;
				vertex.position = positions[v - 1];
				if (L'/' == fin.peek())
				{
					fin.ignore();
					if (L'/' != fin.peek())
					{
						fin >> vt;
						vertex.texcoord = texcoord[vt - 1];
					}
					if (L'/' == fin.peek())
					{
						fin.ignore();
						fin >> vn;
						vertex.normal = normals[vn - 1];
						
					}
				}
				vertices.push_back(vertex);
				indices.push_back(current_index++);
				s->index_count++;
			}
			fin.ignore(1024, L'\n');
		}
		else
		{
			fin.ignore(1024, L'\n');
		}
	}
	vertex_num = static_cast<UINT>(vertices.size());
	fin.close();
	//fin.clear();
	const wchar_t *f_name = file_name;
	wchar_t path_buffer[256];
	errno_t err;
	wchar_t dir[256];
	err=_wsplitpath_s(file, nullptr, 0, dir, sizeof(dir)/ sizeof(dir[0]), nullptr, 0, nullptr, 0);
	if (err != 0)
	{
		assert(0);
	}
	

	err = _wmakepath_s(path_buffer, sizeof(path_buffer)/ sizeof(path_buffer[0]), nullptr, dir, f_name, nullptr);
	if (err != 0)
	{
		assert(0);
	}
	
	std::wifstream fan(path_buffer);
	_ASSERT_EXPR(fan, L"OBJ file not fuund");
	wchar_t texture_name[256];
	wchar_t comand[256];

	while (fan)
	{
		fan >> comand;
	    if (0 == wcscmp(comand, L"newmtl"))
		{
			wchar_t s[256];
			fan >> s;
			material m;
			m.newmtl = s;
			materials.push_back(m);
		}
		else if (0 == wcscmp(comand, L"map_Kd"))
		{
			fan >> texture_name;
			materials.back().map_Kd = texture_name;
			fan.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(comand, L"Kd"))
		{
			FLOAT r, g, b;
			fan >> r >> g >> b;
			materials.rbegin()->Kd = DirectX::XMFLOAT3(r, g, b);
		}
		else if (0 == wcscmp(comand, L"Ka"))
		{
			FLOAT r, g, b;
			fan >> r >> g >> b;
			materials.rbegin()->Ka = DirectX::XMFLOAT3(r, g, b);
		}
		else if (0 == wcscmp(comand, L"Ks"))
		{
			FLOAT r, g, b;
			fan >> r >> g >> b;
			materials.rbegin()->Ks = DirectX::XMFLOAT3(r, g, b);
		}
		else if (0 == wcscmp(comand, L"illum"))
		{
			fan >> materials.back().illum;
		}
		else
		{
			fan.ignore(1024, L'\n');
		}
    }
	fan.close();
	fan.clear();
	// 頂点バッファの生成
	{
		// 頂点バッファを作成するための設定オプション
		D3D11_BUFFER_DESC buffer_desc = {};
		buffer_desc.ByteWidth = static_cast<UINT>(sizeof(vertex)*vertices.size());	// バッファ（データを格納する入れ物）のサイズ
		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// 頂点バッファとしてバッファを作成する。
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		// 頂点バッファに頂点データを入れるための設定
		D3D11_SUBRESOURCE_DATA subresource_data = {};
		subresource_data.pSysMem = vertices.data();	// ここに格納したい頂点データのアドレスを渡すことでCreateBuffer()時にデータを入れることができる。
		subresource_data.SysMemPitch = 0; //Not use for vertex buffers.
		subresource_data.SysMemSlicePitch = 0; //Not use for vertex buffers.
											   // 頂点バッファオブジェクトの生成
		hr = device->CreateBuffer(&buffer_desc, &subresource_data, &vertex_buffer);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}
	// インテックスバッファの生成
	{
		// インテックスバッファを作成するための設定オプション
		D3D11_BUFFER_DESC buffer_desc = {};
		buffer_desc.ByteWidth = static_cast<UINT>(sizeof(u_int)*indices.size());	// バッファ（データを格納する入れ物）のサイズ
		buffer_desc.Usage = D3D11_USAGE_DEFAULT;//またはD3D11_USAGE_IMMUTABLE
		buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;	// インテックスバッファとしてバッファを作成する。
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		// インテックスバッファにインテックスデータを入れるための設定
		D3D11_SUBRESOURCE_DATA subresource_data = {};
		subresource_data.pSysMem = indices.data();	// ここに格納したいインテックスデータのアドレスを渡すことでCreateBuffer()時にデータを入れることができる。
		subresource_data.SysMemPitch = 0; //Not use for vertex buffers.
		subresource_data.SysMemSlicePitch = 0; //Not use for vertex buffers.
											   // インテックスバッファオブジェクトの生成
		hr = device->CreateBuffer(&buffer_desc, &subresource_data, &index_buffer);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}
	// 定数バッファの生成
	{
		// 定数バッファを作成するための設定オプション
		D3D11_BUFFER_DESC buffer_desc = {};
		buffer_desc.ByteWidth = sizeof(cbuffer);	// バッファ（データを格納する入れ物）のサイズ
		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;	// 定数バッファとしてバッファを作成する。
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		hr = device->CreateBuffer(&buffer_desc, nullptr, &constant_buffer);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}

	// 頂点シェーダー＆頂点入力レイアウトの生成
	{
		//// 頂点シェーダーファイルを開く(sprite_vs.hlsl をコンパイルしてできたファイル)
		//FILE* fp = nullptr;
		//fopen_s(&fp, "static_mesh_vs.cso", "rb");
		//_ASSERT_EXPR_A(fp, "CSO File not found");

		//// 頂点シェーダーファイルのサイズを求める
		//fseek(fp, 0, SEEK_END);
		//long cso_sz = ftell(fp);
		//fseek(fp, 0, SEEK_SET);

		//// メモリ上に頂点シェーダーデータを格納する領域を用意する
		//unsigned char* cso_data = new unsigned char[cso_sz];
		//fread(cso_data, cso_sz, 1, fp);	// 用意した領域にデータを読み込む
		//fclose(fp);	// ファイルを閉じる

		//			// 頂点シェーダーデータを基に頂点シェーダーオブジェクトを生成する
		//HRESULT hr = device->CreateVertexShader(
		//	cso_data,		// 頂点シェーダーデータのポインタ
		//	cso_sz,			// 頂点シェーダーデータサイズ
		//	nullptr,
		//	&vertex_shader	// 頂点シェーダーオブジェクトのポインタの格納先。
		//);
		//_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

		//// GPUに頂点データの内容を教えてあげるための設定
		//D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
		//{
		//	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		//	{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		//	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		//};

		//// 入力レイアウトの生成
		//hr = device->CreateInputLayout(
		//	input_element_desc,				// 頂点データの内容
		//	ARRAYSIZE(input_element_desc),	// 頂点データの要素数
		//	cso_data,						// 頂点シェーダーデータ（input_element_descの内容と sprite_vs.hlslの内容に不一致がないかチェックするため）
		//	cso_sz,							// 頂点シェーダーデータサイズ
		//	&input_layout					// 入力レイアウトオブジェクトのポインタの格納先。
		//);
		//_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

		//// 頂点シェーダーデータの後始末
		//delete[] cso_data;
		// GPUに頂点データの内容を教えてあげるための設定
		D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};
		UINT n = ARRAYSIZE(input_element_desc);
		pResourceManager.LoadVertexShader(device, L"Data/shader/static_mesh_vs.cso", &vertex_shader, &input_layout, &n, input_element_desc);
	}

	// ピクセルシェーダーの生成
	{
		//// ピクセルシェーダーファイルを開く(sprite_ps.hlsl をコンパイルしてできたファイル)
		//FILE* fp = nullptr;
		//fopen_s(&fp, "static_mesh_ps.cso", "rb");
		//_ASSERT_EXPR_A(fp, "CSO File not found");

		//// ピクセルシェーダーファイルのサイズを求める
		//fseek(fp, 0, SEEK_END);
		//long cso_sz = ftell(fp);
		//fseek(fp, 0, SEEK_SET);

		//// メモリ上にピクセルシェーダーデータを格納する領域を用意する
		//unsigned char* cso_data = new unsigned char[cso_sz];
		//fread(cso_data, cso_sz, 1, fp);	// 用意した領域にデータを読み込む
		//fclose(fp);	// ファイルを閉じる

		//			// ピクセルシェーダーの生成
		//HRESULT hr = device->CreatePixelShader(
		//	cso_data,		// ピクセルシェーダーデータのポインタ
		//	cso_sz,			// ピクセルシェーダーデータサイズ
		//	nullptr,
		//	&pixel_shader	// ピクセルシェーダーオブジェクトのポインタの格納先
		//);
		//_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

		//// ピクセルシェーダーデータの後始末
		//delete[] cso_data;

		hr=pResourceManager.LoadPixelShader(device, L"Data/shader/static_mesh_ps.cso", &pixel_shader);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}
	// ラスタライザステートの生成
	{
		// ラスタライザステートを作成するための設定オプション
		D3D11_RASTERIZER_DESC rasterizer_desc = {}; //https://msdn.microsoft.com/en-us/library/windows/desktop/ff476198(v=vs.85).aspx
		rasterizer_desc.FillMode = D3D11_FILL_SOLID; //D3D11_FILL_WIREFRAME, D3D11_FILL_SOLID
		rasterizer_desc.CullMode = D3D11_CULL_BACK; //D3D11_CULL_NONE：両面描画, D3D11_CULL_FRONT, D3D11_CULL_BACK   
		rasterizer_desc.FrontCounterClockwise = FALSE;//これがFALSEなら時計回り、TRUEなら反時計回り
		rasterizer_desc.DepthBias = 0; //https://msdn.microsoft.com/en-us/library/windows/desktop/cc308048(v=vs.85).aspx
		rasterizer_desc.DepthBiasClamp = 0;
		rasterizer_desc.SlopeScaledDepthBias = 0;
		rasterizer_desc.DepthClipEnable = FALSE;
		rasterizer_desc.ScissorEnable = FALSE;
		rasterizer_desc.MultisampleEnable = FALSE;
		rasterizer_desc.AntialiasedLineEnable = FALSE;

		// ラスタライザステートの生成
		hr = device->CreateRasterizerState(
			&rasterizer_desc,
			&rasterizer_state
		);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		
	}
	for (auto&m : materials)
	{
		wchar_t f_nam[256];
		const wchar_t*t_name = m.map_Kd.data();
		err = _wmakepath_s(f_nam, sizeof(f_nam) / sizeof(f_nam[0]), nullptr, dir, t_name, nullptr);
		if (err != 0)
		{
			assert(0);
		}

		const wchar_t*tex_name = f_nam;

		pResourceManager.LoadTexture(device, tex_name, m.shader_resource_view.GetAddressOf(), &m.texture2d_desc);

	}
	
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = -FLT_MAX;
	sampDesc.MaxLOD = FLT_MAX;
	sampDesc.MaxAnisotropy = 16;
	memcpy(sampDesc.BorderColor, &DirectX::XMFLOAT4(1.0f, 1.f, 1.f, 1.f), sizeof(DirectX::XMFLOAT4));
	hr = device->CreateSamplerState(&sampDesc, &samplerstate);
	if (FAILED(hr)) {
		assert(0);
	}
	
	vertices.clear();
	indices.clear();
	positions.clear();
	normals.clear();
	texcoord.clear();
}

void static_mesh::render(ID3D11DeviceContext * immediate_context, const DirectX::XMFLOAT4 & color,
	const DirectX::XMFLOAT4 & light_direction, const DirectX::XMFLOAT4X4 & world_view_projection, const DirectX::XMFLOAT4X4 & world) const
{
	//定数バッファの内容を更新
	{
	}
	// 使用する頂点バッファやシェーダーなどをGPUに教えてやる。
	UINT stride = sizeof(vertex);
	UINT offset = 0;
	immediate_context->IASetVertexBuffers(0, 1, &vertex_buffer, &stride, &offset);
	immediate_context->IASetIndexBuffer(index_buffer, DXGI_FORMAT_R32_UINT, 0);//DXGI_FORMAT_R32_UINT=unsigned intの32ビット
	immediate_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);	// ポリゴンの描き方の指定
	immediate_context->IASetInputLayout(input_layout);

	immediate_context->RSSetState(rasterizer_state);

	immediate_context->VSSetShader(vertex_shader, nullptr, 0);
	immediate_context->PSSetShader(pixel_shader, nullptr, 0);
	immediate_context->PSSetSamplers(0, 1, &samplerstate);
	//
	for (auto&materia : materials)
	{
		cbuffer data;
		data.world_view_projection = world_view_projection;
		data.material_color.x = color.x*materia.Kd.x;
		data.material_color.y = color.y*materia.Kd.y;
		data.material_color.z = color.z*materia.Kd.z;
		data.material_color.w = color.w;
		data.light_direction = light_direction;
		data.world = world;
		immediate_context->UpdateSubresource(constant_buffer, 0, nullptr, &data, 0, 0);//定数バッファの内容を変えるときこれを使う
		immediate_context->VSSetConstantBuffers(0, 1, &constant_buffer);//頂点シェーダに定数バッファの情報を送る
		immediate_context->PSSetShaderResources(0, 1, materia.shader_resource_view.GetAddressOf());
		for (auto &subset : subsets)
		{
			if (materia.newmtl == subset.usemtl)
			{
				immediate_context->DrawIndexed(subset.index_count, subset.index_start, 0);
			}
		}
	}
	
	//for (auto &subset : subsets)
	//{
	//	//if (materia.newmtl == subset.usemtl)
	//	//{
	//	//	immediate_context->DrawIndexed(subset.index_count, subset.index_start, 0);
	//	//}
	//	 material* mat = &materials[subset.mat_index];
	//	immediate_context->PSSetShaderResources(0, 1, mat->shader_resource_view.GetAddressOf());

	//	immediate_context->DrawIndexed(subset.index_count, subset.index_start, 0);
	//}
	
	

	// ↑で設定したリソースを利用してポリゴンを描画する。
	//immediate_context->Draw(4, 0);
	//immediate_context->DrawIndexed(vertex_num, 0, 0);
	
}
