#include "electrode_cube.h"
#include"objmanager.h"

ElectrodeeCube::ElectrodeeCube(std::shared_ptr<ModelResource> modelResouce, const DirectX::XMFLOAT3 & position, const DirectX::XMFLOAT4 & color)
{
	mObj = std::make_shared<CubeObj>(modelResouce);
	pObjManager.Set_CubeObj(mObj);
	mObj->SetScale(DirectX::XMFLOAT3(0.8f, 0.8f, 0.8f));
	mObj->SetAngle(DirectX::XMFLOAT3(0, 0, 0));
	mObj->SetPosition(position);
	mObj->SetColor(color);
	if (color.x < 0.5f)type = 0;
	else type = 1;
	modeFlag = false;
	mSpeed = DirectX::XMFLOAT3(0, 0, 0);
	beforePosition = DirectX::XMFLOAT3(0, 0, 0);
	setflag = false;
	mExist = true;
}

void ElectrodeeCube::Update()
{
	mObj->ChangeClear();
	mObj->SetMove(modeFlag);
	mObj->SetChange(false);
	if (mObj->GetColor().x< 0.5f)type = 0;
	else type = 1;
	mObj->SetType(type);
	DirectX::XMFLOAT3 position = mObj->GetPosition();
	if (modeFlag)
	{
		
		if (setflag)setflag = false;
		else if (position.x != beforePosition.x || position.z != beforePosition.z)
		{
			modeFlag = false;
			mObj->SetMove(false);
			return;
		}
		position.x += mSpeed.x;
		position.z += mSpeed.z;
		mObj->SetPosition(position);
		
	}
	beforePosition = position;
}
