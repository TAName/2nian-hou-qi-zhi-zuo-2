#include "scene_result.h"
#include <math.h>
#include "fbx_load.h"
#include "gamepad.h"
#include "camera.h"
#include "objmanager.h"
#include "easing.h"
#include "screenout.h"
#include"soundmanager.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

Scene_Result::Scene_Result(ID3D11Device * device)
{
    loading_thread = std::make_unique<std::thread>([&](ID3D11Device *device)
    {
        std::lock_guard<std::mutex> lock(loading_mutex);
        // モデルロード
        const char* player1FileName = "Data/FBX/player2_02_c.fbx";
        const char* player2FileName = "Data/FBX/player1_03_c.fbx";
        const char* electrodeName = "Data/FBX/blockThrow2.fbx";

        m_modelRenderer = std::make_shared<ModelRenderer>(device);
        FbxLoader fbxLoader1, fbxLoader2, fbxLoader3;

        std::unique_ptr<ModelData> player1Model = std::make_unique<ModelData>();
        fbxLoader1.Load(player1FileName, *player1Model);
        std::unique_ptr<ModelData> player2Model = std::make_unique<ModelData>();
        fbxLoader2.Load(player2FileName, *player2Model);
        std::unique_ptr <ModelData> electrodeModel = std::make_unique<ModelData>();
        fbxLoader3.Load(electrodeName, *electrodeModel);

        std::shared_ptr<ModelResource> player1Resource = std::make_shared<ModelResource>(device, std::move(player1Model));
        std::shared_ptr<ModelResource> player2Resource = std::make_shared<ModelResource>(device, std::move(player2Model));
        std::shared_ptr<ModelResource> electrodeResource = std::make_shared<ModelResource>(device, std::move(electrodeModel));

        m_player1 = std::make_shared<PlayerObj>(player1Resource); // 右
        m_player2 = std::make_shared<PlayerObj>(player2Resource); // 左

        // 画像のロード
        m_sprBack = std::make_unique<Sprite>(device,  L"Data/image/bg.png");
        m_sprWin = std::make_unique<Sprite>(device,   L"Data/image/win.png");
        m_sprLose = std::make_unique<Sprite>(device,  L"Data/image/lose.png");
        m_sprTitle = std::make_unique<Sprite>(device, L"Data/image/return_to_title.png"); // 
        m_sprGame = std::make_unique<Sprite>(device,  L"Data/image/return_to_select.png");

        // 初期化
        m_player1->SetScale(DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f));
        m_player1->SetPosition(DirectX::XMFLOAT3(-15.0f, -2.0f, 0.0f));
        m_player1->SetAngle(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
        m_player1->Play(AnimationNum::Wait, true);

        m_player2->SetScale(DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f));
        m_player2->SetPosition(DirectX::XMFLOAT3(15.0f, -2.0f, 0.0f));
        m_player2->SetAngle(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
        m_player2->Play(AnimationNum::Wait, true);

        // キューブ
        m_electrode = std::make_shared<CubeObj>(electrodeResource);
        m_electrode->SetScale(DirectX::XMFLOAT3(0.5, 0.5, 0.5));
        //if (ObjManager::m_winner < 0) ObjManager::m_winner = 0;
        if (ObjManager::m_winner == 0)
        {
            PlayerDirection(m_player1);
            m_electrode->SetPosition(DirectX::XMFLOAT3(
                m_player1->GetPosition().x + m_forward.x * 6,
                0.0f,
                m_player1->GetPosition().z + m_forward.z * 6));
        }
        else if (ObjManager::m_winner == 1)
        {
            PlayerDirection(m_player2);
            m_electrode->SetPosition(DirectX::XMFLOAT3(
                m_player2->GetPosition().x + m_forward.x * 6,
                0.0f,
                m_player2->GetPosition().z + m_forward.z * 6));
        }

        m_p1Pos = m_player1->GetPosition();
        m_p2Pos = m_player2->GetPosition();

    }, device);
    particle = std::make_unique<Sprite>(device, L"Data/image/NOW_LOADING.png");
    pCamera.Create(DirectX::XMFLOAT3(0, 0, -100), DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f));
    //m_sprWin = std::make_unique<Sprite>(device, L"Data/image/");
    //m_sprLose = std::make_unique<Sprite>(device, L"DAta/image");

    // 初期値が-1  SceneGameでどちらかが死亡したら　0 か　１　が入る
    m_winner = ObjManager::m_winner;
    //m_winner = 0;
    m_nextScene = 0;
    if (m_winner == 0)
    {
        m_blowawayVec = DirectX::XMFLOAT3(1, 1, 0);
    }
    else if (m_winner == 1)
    {
        m_blowawayVec = DirectX::XMFLOAT3(-1, 1, 0);
    }
    m_zoom = DirectX::XMFLOAT3(0, -3.f, 70.f);
    m_totalTime = 0.0f;
    m_fadeFlag = false;
    m_fadeTimer = 0;
    m_alpha = 1.0f;
    m_isHave = true;
	playsound = false;
	anim = 0;
	animTime = 0;
    pScreenOut.Initialize();
}

void Scene_Result::PlayerDirection(std::shared_ptr<PlayerObj> obj)
{
    DirectX::XMMATRIX R;
    DirectX::XMFLOAT4X4 rotation;
    float l, f;
    // プレイヤーの向き オイラー角
    R = DirectX::XMMatrixRotationRollPitchYaw(
        obj->GetAngle().x, obj->GetAngle().y, obj->GetAngle().z);
    DirectX::XMStoreFloat4x4(&rotation, R);

    m_forward = DirectX::XMFLOAT3(rotation._31, rotation._32, rotation._33);
    // 正規化
    l = sqrtf(m_forward.x*m_forward.x + m_forward.y*m_forward.y + m_forward.z*m_forward.z);
    f = 1 / l;
    m_forward.x *= f; m_forward.y *= f; m_forward.z *= f;
}

void Scene_Result::Move(std::shared_ptr<PlayerObj> winner, std::shared_ptr<PlayerObj> loser, float elapsed_time)
{
    if(m_totalTime <= 20)    m_totalTime += elapsed_time;
    static int timer = 0;
    if (timer >= 360)
        timer = 0;

    PlayerDirection(winner);

    // きゅぶ
    if (m_isHave)
    {
        m_electPos =
            DirectX::XMFLOAT3(
                winner->GetPosition().x + m_forward.x * 6.f,
                0.0f,
                winner->GetPosition().z + m_forward.z * 6.f);

        if (m_totalTime >= 1.3f)
        {
            winner->SetAngle(DirectX::XMFLOAT3(
                0,
               static_cast<float>( 90 + (m_winner * 180) * 0.01745f),
                0));

            if (m_totalTime >= 1.8f)
            {
                m_cubespeed = DirectX::XMFLOAT3(
                    m_forward.x * 1.7f,
                    0.0f,
                    m_forward.z * 1.7f);
                m_isHave = false;
            }
        }

    }
    else
    {
        m_electPos.x += m_cubespeed.x;
        m_electPos.z += m_cubespeed.z;

        DirectX::XMFLOAT3 electDir;
        electDir = DirectX::XMFLOAT3(
            m_electPos.x - winner->GetPosition().x,
            m_electPos.y - winner->GetPosition().y,
            m_electPos.z - winner->GetPosition().z);
        float length = sqrtf(electDir.x*electDir.x + electDir.y*electDir.y + electDir.z*electDir.z);

        if (length >= 14)
        {
            loser->SetPosition(DirectX::XMFLOAT3(
                loser->GetPosition().x + m_blowawayVec.x * 2,
                loser->GetPosition().y + m_blowawayVec.y * 3,
                loser->GetPosition().z));

            loser->SetAngle(DirectX::XMFLOAT3(
                loser->GetAngle().x,
                loser->GetAngle().y,
                loser->GetAngle().z + 1));
        }

        if (m_totalTime >= 3.6f)
        {
            timer++;
            winner->SetAngle(DirectX::XMFLOAT3(
                0,
                0,
                0));

            winner->SetPosition(m_zoom);
            winner->SetAngle(DirectX::XMFLOAT3(
                0, timer * 0.01745f, 0));
        }
    }
}

void Scene_Result::MoveRender(ID3D11DeviceContext* devicecontext)
{
    float angle, r, g, b, a;
    angle = 0.f; r = 1.f; g = 1.f; b = 1.f; a = 1.f;

    //if (m_totalTime < 4.2)
    //{
    //}

    if (m_totalTime >= 3.6f)
    {
        // 座標(x, y) サイズ(x, y) テクスチャ(x, y) テクスチャサイズ(x, y) 角度angle 色r, g, b, a

        m_sprWin->render(devicecontext,
            800, 200, 320, 115,
            0, 0, 320, 115,
            angle, r, g, b, a);
        if (m_nextScene + 1 == 1)
        {
            m_sprTitle->render(devicecontext,
                180, 500, 576, 128,
                0, 0, 576, 128,
                angle, r, g, b, a);
            m_sprGame->render(devicecontext,
                1300, 500, 576/2, 128/2,
                0, 0, 576, 128,
                angle, r, g, b, a);
            //m_sprMask->render(devicecontext,
            //    200, 500, 400 + 50, 200 + 50,
            //    0, 0, 576, 128,
            //    angle, r, g, b, a);
        }
        if (m_nextScene + 1 == 2)
        {
            m_sprTitle->render(devicecontext,
                250, 550, 576/2, 128/2,
                0, 0, 576, 128,
                angle, r, g, b, a);
            m_sprGame->render(devicecontext,
                1300, 500, 576, 128,
                0, 0, 576, 128,
                angle, r, g, b, a);
            //m_sprMask->render(devicecontext,
            //    1300, 500, 400 + 50, 200 + 50,
            //    0, 0, 576, 128,
            //    angle, r, g, b, a);
        }
    }
}

int Scene_Result::Update(float elapsed_time)
{
	if (IsNowLoading())
	{
		return 0;
	}
	EndLoading();

    m_player1->Update();
    m_player2->Update();
    m_electrode->Update();
    m_player1->Animation(1.f / 30.f);
    m_player2->Animation(1.f / 30.f);

    if(m_winner == 0)  Move(m_player1, m_player2, elapsed_time);
    else   if (m_winner == 1)  Move(m_player2, m_player1, elapsed_time);

    // 情報を返す

    m_electrode->SetPosition(m_electPos);
	if (!playsound)
	{
		if (m_totalTime >= 3.6f)
		{
			playsound = true;
			pSoundManager.Play(SoundManager::SOUND::BGM_Result, true);
			pSoundManager.SetVolume(SoundManager::SOUND::BGM_Result, 1.0f);
		}
	}
    // タイトルかもう一度かの選択
    if ((input::ButtonRisingState(0, input::PadLabel::RIGHT) || 
        input::ButtonRisingState(0, input::PadLabel::LEFT)) &&
        !m_fadeFlag && m_totalTime >= 3.6f)
    {
        m_nextScene = (m_nextScene + 1) % 2;
    }
    if (input::ButtonRisingState(0, input::PadLabel::A) && m_totalTime >= 3.6f)
    {
        pScreenOut.ScreenOutStart();
        m_fadeFlag = true;
		pSoundManager.Play(SoundManager::SOUND::decision);
		pSoundManager.SetVolume(SoundManager::SOUND::decision, 1.0f);

    }

    // フェード処理
    if (m_fadeFlag)
    {
        int end = pScreenOut.Update(elapsed_time);
        if (end == 2)
        {
			pSoundManager.Stop(SoundManager::SOUND::BGM_Result);

            return m_nextScene + 1;
        }
    }

#ifdef USE_IMGUI
    // IMGUI
    ImGui::Begin("result");
    //ImGui::Text("winner: %d", ObjManager::m_winner);
    ImGui::Text("nextSenene: %d", m_nextScene + 1);
    ImGui::SameLine();
    switch (m_nextScene +1)
    {
    case 1: ImGui::Text("Title"); break;
    case 2: ImGui::Text("Game"); break;
    }
    ImGui::End();
#endif

	return 0;

}

void Scene_Result::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{
	if (IsNowLoading())
	{
		if (animTime++ % 10 == 0)anim++;
		if (anim > 3)anim = 0;
		particle->render(devicecontext, 1450, 950, static_cast<float>(320 + anim * 32), 32, 0, 0, static_cast<float>(640 + anim * 64), 64, 0, 1, 1, 1, 1);

		return;
	}

    const float PI = 3.14f;
    //ワールド行列を作成
    //ビュー行列
    DirectX::XMMATRIX V;
    {
        //カメラの設定
        DirectX::XMVECTOR eye, focus, up;
        eye = DirectX::XMVectorSet(pCamera.GetEye().x, pCamera.GetEye().y, pCamera.GetEye().z, 1.0f);//視点
        focus = DirectX::XMVectorSet(pCamera.GetFocus().x, pCamera.GetFocus().y, pCamera.GetFocus().z, 1.0f);//注視点
        up = DirectX::XMVectorSet(pCamera.GetUp().x, pCamera.GetUp().y, pCamera.GetUp().z, 1.0f);//上ベクトル

        V = DirectX::XMMatrixLookAtLH(eye, focus, up);
    }
    DirectX::XMMATRIX P;
    {
        //画面サイズ取得のためビューポートを取得
        D3D11_VIEWPORT viewport;
        UINT num_viewports = 1;
        devicecontext->RSGetViewports(&num_viewports, &viewport);

        //角度をラジアン(θ)に変換
        float fov_y = 30 * (PI / 180.f);//角度
        float aspect = viewport.Width / viewport.Height;//画面比率
        float near_z = 0.1f;//表示最近面までの距離
        float far_z = 1000.0f;//表紙最遠面までの距離

        P = DirectX::XMMatrixPerspectiveFovLH(fov_y, aspect, near_z, far_z);
    }
    //
    DirectX::XMFLOAT4X4 view, projection;
    DirectX::XMStoreFloat4x4(&view, V);
    DirectX::XMStoreFloat4x4(&projection, P);
    static DirectX::XMFLOAT4 c(1.f, 1.0f, 1.0f, 1.f);

    if (m_totalTime < 3.6f)
    {
        m_sprBack->render(devicecontext,
            0, 0, 1920, 1080,
            0, 0, 1920, 1080,
            0, 1, 1, 1, 1);
    }
    else
    {
        if (m_winner == 0)
        {
            m_sprBack->render(devicecontext,
                0, 0, 1920, 1080,
                0, 0, 1920 / 2, 1080,
                0, 1, 1, 1, 1);
        }
        else if (m_winner == 1)
        {
            m_sprBack->render(devicecontext,
                0, 0, 1920, 1080,
                1920 / 2, 0, 1920 / 2, 1080,
                0, 1, 1, 1, 1);
        }
    }

    m_modelRenderer->Begin(devicecontext, view, projection, DirectX::XMFLOAT4(0, -1, -1, 0));
    m_player1->Render(m_modelRenderer, devicecontext);
    m_player2->Render(m_modelRenderer, devicecontext);
    m_electrode->Render(m_modelRenderer, devicecontext);
    m_modelRenderer->End(devicecontext);

    MoveRender(devicecontext);

    //if(m_fadeFlag)
    pScreenOut.Render(devicecontext);

}

Scene_Result::~Scene_Result()
{
    pCamera.Destroy();
}
