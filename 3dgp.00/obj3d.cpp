#include "obj3d.h"
//初期化
Obj3D::Obj3D(std::shared_ptr<ModelResource> model_resource)
	:mPosition(0,0,0),mScale(1,1,1),mAngle(0,0,0),mColor(1,1,1,1),mExist(true)
{
	mObj = std::make_unique<Model>(model_resource);
	//mPosition = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
	//mScale = DirectX::XMFLOAT3(1.f, 1.f, 1.f);
	//mAngle = DirectX::XMFLOAT3(0.f, 0.f, 0.f);
	//mColor = DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	//mExist = true;
	turn.left = false;
	turn.right = false;
	turn.front = false;
	turn.back = false;
}
//更新
void Obj3D::Update()
{
	turn.left = false;
	turn.right = false;
	turn.front = false;
	turn.back = false;

	DirectX::XMMATRIX s, r, t;
	//拡大行列作成
	s = DirectX::XMMatrixScaling(mScale.x, mScale.y, mScale.z);//スケール行列

	//回転行列作成
	r = DirectX::XMMatrixRotationRollPitchYaw(mAngle.x, mAngle.y, mAngle.z);//回転行列

    //移動行列作成
	t = DirectX::XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);//移動行列

    //行列合成と変換
	DirectX::XMStoreFloat4x4(&mWorld, s*r*t);

}
int Obj3D::RayPick(const DirectX::XMFLOAT3 & startPosition, const DirectX::XMFLOAT3 & endPosition, DirectX::XMFLOAT3 * outPosition, DirectX::XMFLOAT3 * outNormal, float*outDistance)
{
	DirectX::XMMATRIX W = DirectX::XMLoadFloat4x4(&mWorld);
	DirectX::XMMATRIX invers_W;
	invers_W = DirectX::XMMatrixInverse(NULL, W);
	//オブジェクト空間でのレイに変換
	DirectX::XMVECTOR startposition = DirectX::XMLoadFloat3(&startPosition);
	DirectX::XMVECTOR endposition = DirectX::XMLoadFloat3(&endPosition);
	DirectX::XMVECTOR loacal_start = DirectX::XMVector3TransformCoord(startposition, invers_W);
	DirectX::XMVECTOR loacal_end = DirectX::XMVector3TransformCoord(endposition, invers_W);
	//レイピック
	DirectX::XMFLOAT3 start, end;
	DirectX::XMStoreFloat3(&start, loacal_start);
	DirectX::XMStoreFloat3(&end, loacal_end);
	int ret = mObj->RayPick(start, end, outPosition, outNormal, outDistance);
	if (ret != -1)
	{
		//オブジェクト空間からワールド空間へ変換
		DirectX::XMVECTOR out_p = DirectX::XMLoadFloat3(outPosition);
		DirectX::XMVECTOR out_n = DirectX::XMLoadFloat3(outNormal);

		DirectX::XMVECTOR world_position = DirectX::XMVector3TransformCoord(out_p, W);
		DirectX::XMVECTOR world_normal = DirectX::XMVector3TransformCoord(out_n, W);

		DirectX::XMStoreFloat3(outPosition, world_position);
		DirectX::XMStoreFloat3(outNormal, world_normal);
	}
	return ret;

}
//描画
void Obj3D::Render(std::shared_ptr<ModelRenderer> model_renderer, ID3D11DeviceContext * devicecontext)
{
	if (!mExist)return;
	//XMMATRIXに変換
	DirectX::XMMATRIX world;
	world = DirectX::XMLoadFloat4x4(&mWorld);
	
	mObj->CalculateLocalTransform();
	mObj->CalculateWorldTransform(world);
	
	model_renderer->Draw(devicecontext, *mObj,mColor);
}
