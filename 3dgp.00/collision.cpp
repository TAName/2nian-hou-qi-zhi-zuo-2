#include "collision.h"

bool IsHitCube(const DirectX::XMFLOAT3 & cube1_min, const DirectX::XMFLOAT3 & cube1_max, const DirectX::XMFLOAT3 & cube2_min, const DirectX::XMFLOAT3 & cube2_max)
{
	if (cube1_min.x <= cube2_max.x&&cube1_min.y <= cube2_max.y&&cube1_min.z <= cube2_max.z
		&&cube1_max.x >= cube2_min.x&&cube1_max.y >= cube2_min.y&&cube1_max.z >= cube2_min.z)
	{
		return true;
	}
	return false;
}

//bool IsHitAABBCube(const DirectX::XMFLOAT3&cube1_min, const DirectX::XMFLOAT3&cube1_max, const DirectX::XMFLOAT3 & point1, const DirectX::XMFLOAT3 & point2)
//{
//	//cubeの各面の中心座標を求める
//	AABB cube,line;
//	float x, y, z;
//	x = (cube1_max.x - cube1_min.x)*0.5f;
//	y = (cube1_max.y - cube1_min.y)*0.5f;
//	z = (cube1_max.z - cube1_min.z)*0.5f;
//	cube.left = DirectX::XMFLOAT3(cube1_max.x, cube1_max.y - y, cube1_max.z - z);
//	cube.right = DirectX::XMFLOAT3(cube1_min.x, cube1_min.y + y, cube1_min.z + z);
//	cube.up = DirectX::XMFLOAT3(cube1_max.x - x, cube1_max.y, cube1_max.z - z);
//	cube.down = DirectX::XMFLOAT3(cube1_min.x + x, cube1_min.y, cube1_min.z + z);
//	cube.front = DirectX::XMFLOAT3(cube1_min.x + x, cube1_min.y + y, cube1_min.z);
//	cube.back = DirectX::XMFLOAT3(cube1_max.x - x, cube1_max.y - y, cube1_max.z);
//	line.left = DirectX::XMFLOAT3(1, 0, 0);
//	line.right = DirectX::XMFLOAT3(-1, 0, 0);
//	line.up = DirectX::XMFLOAT3(0, 1, 0);
//	line.down = DirectX::XMFLOAT3(0, -1, 0);
//	line.front = DirectX::XMFLOAT3(0, 0, -1);
//	line.back = DirectX::XMFLOAT3(0, 0, 1);
//
//	//各面の中心座標からpointへのベクトルを求める
//	DirectX::XMFLOAT3 vect[6][2];
//	vect[0][0] = DirectX::XMFLOAT3(point1.x - cube.left.x, point1.y - cube.left.y, point1.z - cube.left.z);
//	vect[0][1] = DirectX::XMFLOAT3(point2.x - cube.left.x, point2.y - cube.left.y, point2.z - cube.left.z);
//	vect[1][0] = DirectX::XMFLOAT3(point1.x - cube.right.x, point1.y - cube.right.y, point1.z - cube.right.z);
//	vect[1][1] = DirectX::XMFLOAT3(point2.x - cube.right.x, point2.y - cube.right.y, point2.z - cube.right.z);
//	vect[2][0] = DirectX::XMFLOAT3(point1.x - cube.up.x, point1.y - cube.up.y, point1.z - cube.up.z);
//	vect[2][1] = DirectX::XMFLOAT3(point2.x - cube.up.x, point2.y - cube.up.y, point2.z - cube.up.z);
//	vect[3][0] = DirectX::XMFLOAT3(point1.x - cube.down.x, point1.y - cube.down.y, point1.z - cube.down.z);
//	vect[3][1] = DirectX::XMFLOAT3(point2.x - cube.down.x, point2.y - cube.down.y, point2.z - cube.down.z);
//	vect[4][0] = DirectX::XMFLOAT3(point1.x - cube.front.x, point1.y - cube.front.y, point1.z - cube.front.z);
//	vect[4][1] = DirectX::XMFLOAT3(point2.x - cube.front.x, point2.y - cube.front.y, point2.z - cube.front.z);
//	vect[5][0] = DirectX::XMFLOAT3(point1.x - cube.back.x, point1.y - cube.back.y, point1.z - cube.back.z);
//	vect[5][1] = DirectX::XMFLOAT3(point2.x - cube.back.x, point2.y - cube.back.y, point2.z - cube.back.z);
//
//	//内積を求める
//	float dot[6][2];
//
//	dot[0][0] = vect[0][0].x*line.left.x + vect[0][0].y*line.left.y + vect[0][0].z*line.left.z;
//	dot[0][1] = vect[0][1].x*line.left.x + vect[0][1].y*line.left.y + vect[0][1].z*line.left.z;
//	dot[1][0] = vect[1][0].x*line.right.x + vect[1][0].y*line.right.y + vect[1][0].z*line.right.z;
//	dot[1][1] = vect[1][1].x*line.right.x + vect[1][1].y*line.right.y + vect[1][1].z*line.right.z;
//	dot[2][0] = vect[2][0].x*line.up.x + vect[2][0].y*line.up.y + vect[2][0].z*line.up.z;
//	dot[2][1] = vect[2][1].x*line.up.x + vect[2][1].y*line.up.y + vect[2][1].z*line.up.z;
//	dot[3][0] = vect[3][0].x*line.down.x + vect[3][0].y*line.down.y + vect[3][0].z*line.down.z;
//	dot[3][1] = vect[3][1].x*line.down.x + vect[3][1].y*line.down.y + vect[3][1].z*line.down.z;
//	dot[4][0] = vect[4][0].x*line.front.x + vect[4][0].y*line.front.y + vect[4][0].z*line.front.z;
//	dot[4][1] = vect[4][1].x*line.front.x + vect[4][1].y*line.front.y + vect[4][1].z*line.front.z;
//	dot[5][0] = vect[5][0].x*line.back.x + vect[5][0].y*line.back.y + vect[5][0].z*line.back.z;
//	dot[5][1] = vect[5][1].x*line.back.x + vect[5][1].y*line.back.y + vect[5][1].z*line.back.z;
//
//
//	if (dot[0][0] * dot[0][1] < 0)
//	{
//
//	}
//
//	return false;
//}
