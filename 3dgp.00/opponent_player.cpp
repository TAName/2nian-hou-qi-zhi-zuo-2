#include "opponent_player.h"
#include "fbx_load.h"
#include"objmanager.h"
#include"stage.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

OpponentPlayer::OpponentPlayer(ID3D11Device * device, int pType)
	:state(OPPONENT_MOVE::WAIT), cubeGetFlag(false), targetSetFlag(false), hit(false), wallHit(false), rayHit(false), animend(false), moveTimer(0), turn(false)
{
	const char* fbx_filename = "Data/FBX/player1_03_c.fbx";
	playType = pType;
	if (playType == 0)
	{
		std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
		FbxLoader fbx_loader;
		fbx_loader.Load(fbx_filename, *model_data);

		std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
		mObj = std::make_shared<PlayerObj>(model_resource);
		pObjManager.Set_Player2(mObj);
		mObj->SetPosition(DirectX::XMFLOAT3(-80, 0, -20));
		mObj->SetScale(DirectX::XMFLOAT3(1, 1, 1));
		mObj->SetAngle(DirectX::XMFLOAT3(0, 0, 0));
		mObj->Play(1, true);
		beforePosition = mObj->GetPosition();
		mObj->SetHit(false);
	}
	else
	{
		mPlayer = std::make_unique<Player>(device, fbx_filename, playType, DirectX::XMFLOAT3(-80, 0, -20));
	}
}

void OpponentPlayer::Update(float elapsed_time)
{
	if (playType == 1)
	{
		mPlayer->Update(elapsed_time);
		return;
	}
	if (!mObj->GetExist())return;
	position = mObj->GetPosition();
	angle = mObj->GetAngle().y;
	if (animend)
	{
		if (!mObj->GetEndAnimation())
		{
			mObj->Play(0, true);
			animend = false;
			mObj->SetAnimTime(1.f / 60.f);
		}
		else return;
	}
	switch (state)
	{
	case OPPONENT_MOVE::WAIT:
		Wait();
		break;
	case OPPONENT_MOVE::TARGET:
		Target();
		break;
	case OPPONENT_MOVE::RUN:
		Run();
		break;
	case OPPONENT_MOVE::GET:
		Get();
		break;
	case OPPONENT_MOVE::THROW:
		Throw();
		break;
	}

#ifdef USE_IMGUI
	ImGui::Begin("player2");
	ImGui::Text("leng.x%f,leng.z%f", leng.x, leng.z);
	ImGui::Text("sido.x%f,sido.z%f", sido.x, sido.z);
	ImGui::End();
#endif
	mObj->SetPosition(position);
	//if (moveTimer % 10 == 1)
	{
		beforePosition = position;
	}
	//mObj->Animation(1.f / 60.f);
	mObj->SetAngle(DirectX::XMFLOAT3(0, angle, 0));
	mObj->SetSpeed(mSpeed);
	mObj->SetHit(false);
	mObj->SetHitDown(false);
	mObj->SetHitUp(false);
	mObj->SetHitLeft(false);
	mObj->SetHitRight(false);
}
//cubeに当たりそうかどうか

void OpponentPlayer::CubeAvoidance()
{
	DirectX::XMFLOAT3 p, n;
	float l;
	float cos = mSpeed.x*12.f;
	float sin = mSpeed.z*12.f;

	switch (type)
	{
	case 0:
		if (pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + sido.z*2.f), DirectX::XMFLOAT3(position.x + cos, position.y, position.z + sido.z*2.f), &p, &n, &l, -1) != -1
			|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - sido.z*2.f), DirectX::XMFLOAT3(position.x + cos, position.y, position.z - sido.z*2.f), &p, &n, &l, -1) != -1)
		{
			mSpeed.z = 0;
			mSpeed.x = 0;
			SetSpeed(leng.z, &mSpeed.z);
			SetAngle(0.f, mSpeed.z);
			type = 1;
		}
		break;
	case 1:
		if (pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x + sido.x*2.f, position.y, position.z), DirectX::XMFLOAT3(position.x + sido.x*2.f, position.y, position.z + sin), &p, &n, &l, -1) != -1
			|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x - sido.x*2.f, position.y, position.z), DirectX::XMFLOAT3(position.x - sido.x*2.f, position.y, position.z + sin), &p, &n, &l, -1) != -1)
		{
			mSpeed.z = 0;
			mSpeed.x = 0;
			SetSpeed(leng.x, &mSpeed.x);
			SetAngle(0.5f, mSpeed.x);
			type = 0;
		}
		break;
	}
}

//取りに行くcubeの座標取得
void OpponentPlayer::CubeLockOn()
{
	int cubeNo = 0;
	float neart = 1000000000.f;
	DirectX::XMFLOAT3 cubePosition;
	targetNo = -1;
	for (auto&cube : pStageManager.GetCube())
	{
		if (cube.GetObjType() == 1 && !cube.GetMoveFlag())
		{
			cubePosition = cube.GetPosition();
			float leng = (cubePosition.x - position.x)*(cubePosition.x - position.x) + (cubePosition.z - position.z)*(cubePosition.z - position.z);
			if (neart > leng)
			{
				targetPosition = cubePosition;
				neart = leng;
				targetNo = cubeNo;
			}
		}
		cubeNo++;
	}

}
//待機
void OpponentPlayer::Wait()
{
	//
	CubeLockOn();
	if (targetNo > -1)//もし取りに行くcubuがあったら
	{
		MeasurementSpeed();
		state = OPPONENT_MOVE::RUN;
		mObj->Play(0, true);
	}

}
//次に取りに行くcubeがあるかどうかを知る
void OpponentPlayer::Target()
{
	CubeLockOn();
	if (targetNo == -1)//なかったら
	{
		state = 0;
		mObj->Play(1, true);
	}
	else//あったら
	{
		mObj->Play(0, true);
		MeasurementSpeed();
		hit = false;
		state++;
	}

}
//スピードのセッター
void OpponentPlayer::SetSpeed(float leng, float*speed)
{
	if (leng > 0)*speed = 1.f;
	else *speed = -1.f;
}
//角度の設定
void OpponentPlayer::SetAngle(float increase, float speed)
{
	if (speed == 1)
	{
		angle = 3.14f*increase;
	}
	else
	{
		angle = 3.14f*(increase + 1.f);
	}

}

//cubeに向かうときの初期速度設定
void OpponentPlayer::MeasurementSpeed()
{
	mSpeed = DirectX::XMFLOAT3(0, 0, 0);

	leng.x = targetPosition.x - position.x;
	leng.z = targetPosition.z - position.z;
	if (leng.x*leng.x >= leng.z*leng.z)
	{
		SetSpeed(leng.x, &mSpeed.x);
		SetAngle(0.5f, mSpeed.x);
		type = 0;
	}
	else
	{
		SetSpeed(leng.z, &mSpeed.z);
		SetAngle(0.f, mSpeed.z);
		type = 1;
	}
}
//cubeに向かう
void OpponentPlayer::Run()
{
	if (targetNo == -1)return;
	leng.x = targetPosition.x - position.x;
	leng.z = targetPosition.z - position.z;
	DirectX::XMFLOAT3 p, n;
	float l;

#if MODO
	if (leng.x*leng.x <= 100.f&&leng.z*leng.z <= 100.f)
	{
		state++;
		return;
	}

	if (moveTimer % 10 == 0)
	{
		switch (type)
		{
		case 0:
			if (!wallHit)
			{
				if (leng.z > 0)
				{
					sido = DirectX::XMFLOAT3(0, 0, 1);

				}
				else
				{
					sido = DirectX::XMFLOAT3(0, 0, -1);

				}

				if (beforePosition.x != position.x)
				{
					mSpeed.x = 0;
					mSpeed.z = 0;
					wallHit = true;
					SetSpeed(leng.z, &mSpeed.z);
					SetAngle(0.f, mSpeed.z);
					sido.z = -mSpeed.z;

				}
				else if (leng.x <= 4 && leng.x >= -4)
				{
					turn = true;
				}
				if (turn)
				{
					if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + mSpeed.x*12.f, position.y, position.z), &p, &n, &l) != -1
						|| pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + mSpeed.x*12.f, position.y, position.z), &p, &n, &l, -1) != -1)
					{
						mSpeed.x *= -1;
					}

					if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
						&& pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1
						)
					{
						turn = false;
						mSpeed.z = 0;
						mSpeed.x = 0;
						SetSpeed(leng.z, &mSpeed.z);
						type = 1;
						SetAngle(0.f, mSpeed.z);
					}

				}
			}
			else
			{
				if (leng.x > 0)
				{
					sido = DirectX::XMFLOAT3(1, 0, 0);

				}
				else
				{
					sido = DirectX::XMFLOAT3(-1, 0, 0);

				}

				//if (beforePosition.x == position.x)
				{
					if (
						pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
						&& pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1
						)

					{
						turn = false;
						mSpeed.z = 0;
						SetSpeed(leng.x, &mSpeed.x);
						wallHit = false;
						SetAngle(0.5f, mSpeed.x);


					}
				}

			}
			break;
		case 1:
			if (!wallHit)
			{
				if (leng.x > 0)
				{
					sido = DirectX::XMFLOAT3(1, 0, 0);

				}
				else
				{
					sido = DirectX::XMFLOAT3(-1, 0, 0);

				}


				if (beforePosition.z != position.z)
				{

					mSpeed.x = 0;
					mSpeed.z = 0;
					wallHit = true;
					SetSpeed(leng.x, &mSpeed.x);
					SetAngle(0.5f, mSpeed.x);
					sido.x = -mSpeed.x;
				}
				else if (leng.z <= 4 && leng.z >= -4)
				{
					turn = true;
				}
				if (turn)
				{
					if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*12.f), &p, &n, &l) != -1
						|| pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*12.f), &p, &n, &l, -1) != -1)
					{
						mSpeed.z *= -1;
					}

					if (
						pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
						&& pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1
						)
					{
						mSpeed.x = 0;
						mSpeed.z = 0;
						turn = false;
						SetSpeed(leng.x, &mSpeed.x);
						type = 0;
						SetAngle(0.5f, mSpeed.x);

					}

				}

			}
			else
			{
				if (leng.z > 0)
				{
					sido = DirectX::XMFLOAT3(0, 0, 1);

				}
				else
				{
					sido = DirectX::XMFLOAT3(0, 0, -1);

				}

				if (beforePosition.z == position.z)
				{
					if (
						pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
						&& pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1
						)
					{
						turn = false;

						wallHit = false;
						mSpeed.x = 0;
						mSpeed.z = 0;
						SetSpeed(leng.z, &mSpeed.z);
						SetAngle(0.f, mSpeed.z);

					}

				}

			}
			break;
		}



		CubeAvoidance();
		position.x += mSpeed.x*10.0f;
		position.z += mSpeed.z*10.0f;

	}
	moveTimer++;
#else
	if (leng.x*leng.x + leng.z*leng.z <= 100.f)
	{
		state++;
		return;
	}
	if (leng.z > 0)
	{
		sido.z = 1;
	}
	else
	{
		sido.z = -1;
	}
	if (leng.x > 0)
	{
		sido.x = 1;
	}
	else
	{
		sido.x = -1;
	}

	switch (type)
	{
	case 0:

		//壁に当たった時
		if (mObj->GetHitLeft() || mObj->GetHitRight() || beforePosition.x != position.x)
		{
			mSpeed = DirectX::XMFLOAT3(0, 0, 0);
			type = 1;
			SetSpeed(leng.z, &mSpeed.z);
			//向いたほうに壁があったら
			if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + sido.x*2.f, position.y, position.z), DirectX::XMFLOAT3(position.x + sido.x*2.f, position.y, position.z + mSpeed.z*9.f), &p, &n, &l) != -1
				|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - sido.x*2.f, position.y, position.z), DirectX::XMFLOAT3(position.x - sido.x*2.f, position.y, position.z - mSpeed.z*9.f), &p, &n, &l) != -1)
			{
				mSpeed.z *= -1;
			}
			SetAngle(0.f, mSpeed.z);
		}
		else if (leng.x <= 4 && leng.x >= -4)
		{
			turn = true;
		}

		if (turn)
		{
			if (
				pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1
				&& pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1
				)
			{
				turn = false;
				mSpeed.z = 0;
				mSpeed.x = 0;
				SetSpeed(leng.z, &mSpeed.z);
				type = 1;
				SetAngle(0.f, mSpeed.z);
			}
		}
		break;
	case 1:
		//壁に当たった時
		if (mObj->GetHitDown() || mObj->GetHitUp() || beforePosition.z != position.z)
		{
			mSpeed = DirectX::XMFLOAT3(0, 0, 0);
			type = 0;
			SetSpeed(leng.x, &mSpeed.x);
			//向いたほうに壁があったら
			if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + sido.z*2.f), DirectX::XMFLOAT3(position.x + mSpeed.x*9.f, position.y, position.z + sido.z*2.f), &p, &n, &l) != -1
				|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*9.f, position.y, position.z + sido.z*2.f), &p, &n, &l) != -1)
			{
				mSpeed.x *= -1;
			}
			SetAngle(0.5f, mSpeed.x);
		}
		else if (leng.z <= 4 && leng.z >= -4)
		{
			turn = true;
		}
		if (turn)
		{
			if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + mSpeed.z*3.f), &p, &n, &l) == -1
				&& pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z - mSpeed.z*3.f), &p, &n, &l) == -1
				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + mSpeed.z*3.f), &p, &n, &l, -1) == -1
				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z - mSpeed.z*3.f), &p, &n, &l, -1) == -1)
			{
				turn = false;
				mSpeed.z = 0;
				mSpeed.x = 0;
				SetSpeed(leng.x, &mSpeed.x);
				type = 0;
				SetAngle(0.5f, mSpeed.x);
			}
		}

		break;
	}
	CubeAvoidance();
	position.x += mSpeed.x*1.0f;
	position.z += mSpeed.z*1.0f;

#endif

}
//cubeを取得
void OpponentPlayer::Get()
{
	//moveTimer = 0;
	if (pStageManager.GetCube().at(targetNo).GetObjType() == 1)
	{
		state++;
		mSpeed = DirectX::XMFLOAT3(0, 0, 0);
		//DirectX::XMFLOAT3 p1Position = pObjManager.GetPlayer1Position();
		//leng.x = p1Position.x - position.x;
		//leng.z = p1Position.z - position.z;
		if (leng.x*leng.x < leng.z*leng.z)
		{
			SetSpeed(leng.z, &mSpeed.z);
			SetAngle(0.f, mSpeed.z);
			type = 1;
		}
		else
		{
			SetSpeed(leng.x, &mSpeed.x);
			SetAngle(0.5f, mSpeed.x);
			type = 0;
		}

		mObj->SetCarryCube(targetNo);
		wallHit = false;
		mObj->Play(2);
		hit = false;
		//animend = true;

		mObj->SetAnimTime(1.f / 90.f);
	}
	else
	{
		state = OPPONENT_MOVE::TARGET;

	}
}
//playerの方に行って投げる
void OpponentPlayer::Throw()
{
	DirectX::XMFLOAT3 p1Position = pObjManager.GetPlayer1Position();
	leng.x = p1Position.x - position.x;
	leng.z = p1Position.z - position.z;
	if (!targetSetFlag)
	{
		if (leng.x*leng.x < leng.z*leng.z)
		{
			mSpeed.x = 0;
			mSpeed.z = 0;

			SetSpeed(leng.z, &mSpeed.z);
			SetAngle(0.f, mSpeed.z);
			type = 1;
		}
		else
		{
			mSpeed.x = 0;
			mSpeed.z = 0;

			SetSpeed(leng.x, &mSpeed.x);
			SetAngle(0.5f, mSpeed.x);
			type = 0;
		}
		targetSetFlag = true;
		moveTimer++;
		return;
	}
	DirectX::XMFLOAT3 p, n;
	float l;
	static int timer = 0;
#if MODO
	if (leng.x*leng.x <= 20.f || leng.z*leng.z <= 20.f)
	{
		if (leng.x*leng.x <= 20.f)
		{
			if (leng.z > 0)
			{
				throwleng = DirectX::XMFLOAT3(0, 0, 1);
			}
			else
			{
				throwleng = DirectX::XMFLOAT3(0, 0, -1);

			}
		}
		else
		{
			if (leng.x > 0)
			{
				throwleng = DirectX::XMFLOAT3(1, 0, 0);
			}
			else
			{
				throwleng = DirectX::XMFLOAT3(-1, 0, 0);
			}
		}
		if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + throwleng.x*10.f, position.y, position.z + throwleng.z*10.f), &p, &n, &l) == -1)
		{
			mSpeed = DirectX::XMFLOAT3(0, 0, 0);
			if (leng.x*leng.x <= 20.f)
			{
				SetSpeed(leng.z, &mSpeed.z);
				SetAngle(0.f, mSpeed.z);
			}
			else
			{
				SetSpeed(leng.x, &mSpeed.x);
				SetAngle(0.5f, mSpeed.x);

			}

			pStageManager.GetCube().at(targetNo).SetPosition(DirectX::XMFLOAT3(position.x + mSpeed.x * 10, position.y, position.z + mSpeed.z * 10));

			pStageManager.GetCube().at(targetNo).SetSpeed(DirectX::XMFLOAT3(mSpeed.x*2.f, 0, mSpeed.z*2.f));
			state = 1;
			cubeGetFlag = false;
			targetSetFlag = false;
			mObj->SetCarryCube(-1);
			mObj->Play(3);
			animend = true;
			mObj->SetAnimTime(1.f / 90.f);
			targetNo = -1;
			return;

		}
		hit = true;
	}

	if (moveTimer % 10 == 0)
	{

		switch (type)
		{
		case 0:
			//if (hit)
			//{
			//	if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + throwleng.x*10.f, position.y, position.z + throwleng.z*10.f), &p, &n, &l) == -1)
			//	{
			//		{
			//			mSpeed.x = 0;
			//			mSpeed.z = 0;
			//			hit = false;
			//			wallHit = false;
			//			type = 1;
			//			SetSpeed(leng.z, &mSpeed.z);
			//			SetAngle(0.f, mSpeed.z);

			//		}
			//	}
			//}

			if (!wallHit)
			{
				if (beforePosition.x != position.x)
				{
					sido = mSpeed;
					mSpeed.x = 0;
					mSpeed.z = 0;
					SetSpeed(leng.z, &mSpeed.z);
					if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + mSpeed.x*12.f, position.y, position.z + mSpeed.z*12.f), &p, &n, &l) != -1
						|| pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + mSpeed.x*12.f, position.y, position.z + mSpeed.z*12.f), &p, &n, &l, -1) != -1)
					{
						mSpeed.z *= -1;
					}
					SetAngle(0.f, mSpeed.z);
					hit = false;
					wallHit = true;

				}
			}
			else
			{
				if (moveTimer % 10 == 0)
				{
					if (leng.x > 0)
					{
						sido = DirectX::XMFLOAT3(1, 0, 0);

					}
					else
					{
						sido = DirectX::XMFLOAT3(-1, 0, 0);

					}

					if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
						&& pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1)
					{
						wallHit = false;
						mSpeed.x = 0;
						mSpeed.z = 0;
						SetSpeed(leng.x, &mSpeed.x);

						SetAngle(0.5f, mSpeed.x);

					}

				}

			}

			break;
		case 1:


			//if (hit)
			//{
			//	if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + throwleng.x*12.f, position.y, position.z + throwleng.z*12.f), &p, &n, &l) == -1)
			//	{
			//		{
			//			hit = false;
			//			mSpeed.x = 0;
			//			mSpeed.z = 0;
			//			type = 0;
			//			SetSpeed(leng.x, &mSpeed.x);
			//			SetAngle(0.5f, mSpeed.x);
			//			wallHit = false;
			//		}

			//	}

			//}
			if (!wallHit)
			{
				if (beforePosition.z != position.z)
				{
					sido = mSpeed;
					mSpeed.x = 0;
					mSpeed.z = 0;
					SetSpeed(leng.x, &mSpeed.x);
					if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + mSpeed.x*12.f, position.y, position.z + mSpeed.z*12.f), &p, &n, &l) != -1
						|| pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + mSpeed.x*12.f, position.y, position.z + mSpeed.z*12.f), &p, &n, &l, -1) != -1)
					{
						mSpeed.x *= -1;
					}

					SetAngle(0.5f, mSpeed.x);
					wallHit = true;
				}
			}
			else
			{
				if (moveTimer % 10 == 0)
				{
					if (leng.z > 0)
					{
						sido = DirectX::XMFLOAT3(0, 0, 1);

					}
					else
					{
						sido = DirectX::XMFLOAT3(0, 0, -1);

					}

					if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
						&& pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1)
					{
						wallHit = false;
						mSpeed.x = 0;
						mSpeed.z = 0;
						SetSpeed(leng.z, &mSpeed.z);

						SetAngle(0.f, mSpeed.z);

					}

				}

			}
			break;
		}
		CubeAvoidance();
		position.x += mSpeed.x*10.0f;
		position.z += mSpeed.z*10.0f;
	}
	moveTimer++;
#else
	if (leng.x*leng.x <= 16.f || leng.z*leng.z <= 16.f)
	{
		if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(p1Position.x + mSpeed.x*3.f, p1Position.y, p1Position.z + mSpeed.z*3.f), &p, &n, &l) == -1
			&& pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(p1Position.x + mSpeed.x*3.f, p1Position.y, p1Position.z + mSpeed.z*3.f), &p, &n, &l) == -1
			)
		{
			mSpeed = DirectX::XMFLOAT3(0, 0, 0);
			if (leng.x*leng.x <= 20.f)
			{
				SetSpeed(leng.z, &mSpeed.z);
				SetAngle(0.f, mSpeed.z);
			}
			else
			{
				SetSpeed(leng.x, &mSpeed.x);
				SetAngle(0.5f, mSpeed.x);

			}

			pStageManager.GetCube().at(targetNo).SetPosition(DirectX::XMFLOAT3(position.x + mSpeed.x * 10, position.y, position.z + mSpeed.z * 10));

			pStageManager.GetCube().at(targetNo).SetSpeed(DirectX::XMFLOAT3(mSpeed.x*2.f, 0, mSpeed.z*2.f));
			state = 1;
			cubeGetFlag = false;
			targetSetFlag = false;
			mObj->SetCarryCube(-1);
			mObj->Play(3);
			animend = true;
			mObj->SetAnimTime(1.f / 90.f);
			targetNo = -1;
			return;

		}
		hit = true;
	}
	if (leng.z > 0)
	{
		sido.z = 1;
	}
	else
	{
		sido.z = -1;
	}
	if (leng.x > 0)
	{
		sido.x = 1;
	}
	else
	{
		sido.x = -1;
	}

	//switch (type)
	//{
	//case 0:
	//	//壁に当たってるとき
	//	if (!wallHit)
	//	{
	//		//右か左が当たってたら
	//		if (mObj->GetHitLeft() || mObj->GetHitRight())
	//		{
	//			mSpeed.x = 0;
	//			mSpeed.z = 0;
	//			type = 1;
	//			SetSpeed(leng.z, &mSpeed.z);
	//			if (

	//				pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + mSpeed.z*3.f), &p, &n, &l) != -1
	//				|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z - mSpeed.z*3.f), &p, &n, &l) != -1
	//				|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z + mSpeed.z*2.5f), &p, &n, &l, -1) != -1
	//				|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z - mSpeed.z*2.5f), &p, &n, &l, -1) != -1
	//				)
	//			{
	//				mSpeed.z *= -1;
	//			}

	//			SetAngle(0.f, mSpeed.z);
	//		}
	//		//
	//		else if (leng.x <= 4 && leng.x >= -4)
	//		{
	//			turn = true;
	//		}
	//		if (mObj->GetHitLeft() || mObj->GetHitRight())
	//		{
	//			if (mObj->GetHitUp() || mObj->GetHitDown())
	//			{
	//				mSpeed.x *= -1;
	//			}
	//		}
	//		//
	//		if (turn)
	//		{

	//			if (
	//				pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
	//				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) == -1
	//				&& pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
	//				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) == -1

	//				)
	//			{
	//				hit = false;
	//				turn = false;
	//				mSpeed.z = 0;
	//				mSpeed.x = 0;
	//				SetSpeed(leng.z, &mSpeed.z);
	//				type = 1;
	//				SetAngle(0.f, mSpeed.z);
	//			}
	//		}
	//		//if (
	//		//	pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + mSpeed.z*3.f), &p, &n, &l) != -1
	//		//	|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z - mSpeed.z*3.f), &p, &n, &l) != -1
	//		//	|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z + mSpeed.z*2.5f), &p, &n, &l, -1) != -1
	//		//	|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z - mSpeed.z*2.5f), &p, &n, &l, -1) != -1
	//		//	)
	//		//{
	//		//	mSpeed.x = 0;
	//		//	mSpeed.z = 0;
	//		//	type = 1;
	//		//	SetSpeed(leng.z, &mSpeed.z);
	//		//	SetAngle(0.f, mSpeed.z);
	//		//}
	//	}
	//	else
	//	{


	//		if (mObj->GetHitUp() || mObj->GetHitDown())
	//		{
	//			mSpeed.x = 0;
	//			mSpeed.z = 0;
	//			wallHit = false;
	//			SetSpeed(leng.x, &mSpeed.x);
	//			if (
	//				pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) != -1
	//				|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) != -1
	//				|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) != -1
	//				|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) != -1

	//				)
	//			{
	//				mSpeed.x *= -1;
	//			}
	//			SetAngle(0.5f, mSpeed.x);
	//		}

	//		if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + mSpeed.z*3.f), &p, &n, &l) == -1
	//			&& pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z - mSpeed.z*3.f), &p, &n, &l) == -1
	//			&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z + mSpeed.z*2.5f), &p, &n, &l, -1) == -1
	//			&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z - mSpeed.z*2.5f), &p, &n, &l, -1) == -1)
	//		{
	//			turn = false;
	//			mSpeed.z = 0;
	//			SetSpeed(leng.x, &mSpeed.x);
	//			wallHit = false;
	//			SetAngle(0.5f, mSpeed.x);
	//		}

	//	}
	//	break;
	//case 1:
	//	if (!wallHit)
	//	{
	//		//if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*12.f), &p, &n, &l) != -1
	//		//	|| pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*15.f), &p, &n, &l, -1) != -1)
	//		//{
	//		//	if (pObjManager.StageObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z), &p, &n, &l) != -1
	//		//		|| pObjManager.CubeObjRayPic(position, DirectX::XMFLOAT3(position.x + sido.x*15.f, position.y, position.z), &p, &n, &l, -1) != -1)
	//		//	{
	//		//		mSpeed.z *= -1;

	//		//	}

	//		//}

	//		if (mObj->GetHitUp() || mObj->GetHitDown())
	//		{
	//			mSpeed.x = 0;
	//			mSpeed.z = 0;
	//			type = 0;
	//			SetSpeed(leng.x, &mSpeed.x);
	//			if (
	//				pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) != -1
	//				|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) != -1
	//				|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) != -1
	//				|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) != -1

	//				)
	//			{
	//				mSpeed.x *= -1;
	//			}

	//			SetAngle(0.5f, mSpeed.x);
	//		}
	//		else if (leng.z <= 4 && leng.z >= -4)
	//		{
	//			turn = true;
	//		}
	//		if (mObj->GetHitLeft() || mObj->GetHitRight())
	//		{
	//			if (mObj->GetHitUp() || mObj->GetHitDown())
	//			{
	//				mSpeed.z *= -1;
	//			}
	//		}

	//		if (turn)
	//		{

	//			if (

	//				pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + mSpeed.z*3.f), &p, &n, &l) == -1
	//				&& pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z - mSpeed.z*3.f), &p, &n, &l) == -1
	//				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z + mSpeed.z*2.5f), &p, &n, &l, -1) == -1
	//				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z - mSpeed.z*2.5f), &p, &n, &l, -1) == -1
	//				)
	//			{
	//				hit = false;
	//				turn = false;
	//				mSpeed.z = 0;
	//				mSpeed.x = 0;
	//				SetSpeed(leng.x, &mSpeed.x);
	//				type = 0;
	//				SetAngle(0.5f, mSpeed.x);
	//			}
	//		}
	//		//if (
	//		//	pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) != -1
	//		//	|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) != -1
	//		//	|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) != -1
	//		//	|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) != -1
	//		//	)
	//		//{
	//		//	mSpeed.x = 0;
	//		//	mSpeed.z = 0;
	//		//	type = 0;
	//		//	SetSpeed(leng.x, &mSpeed.x);
	//		//	SetAngle(0.5f, mSpeed.x);
	//		//}
	//	}
	//	else
	//	{
	//		if (mObj->GetHitLeft() || mObj->GetHitRight())
	//		{
	//			if (mObj->GetHitUp() || mObj->GetHitDown())
	//			{
	//				mSpeed.x *= -1;
	//			}
	//		}


	//		//if (beforePosition.z == position.z)

	//		if (mObj->GetHitLeft() || mObj->GetHitRight())
	//		{
	//			mSpeed.x = 0;
	//			mSpeed.z = 0;
	//			wallHit = false;
	//			SetSpeed(leng.z, &mSpeed.z);
	//			if (

	//				pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + mSpeed.z*3.f), &p, &n, &l) != -1
	//				|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z - mSpeed.z*3.f), &p, &n, &l) != -1
	//				|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z + mSpeed.z*2.5f), &p, &n, &l, -1) != -1
	//				|| pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*2.5f), DirectX::XMFLOAT3(position.x + sido.x*20.f, position.y, position.z - mSpeed.z*2.5f), &p, &n, &l, -1) != -1
	//				)
	//			{
	//				mSpeed.z *= -1;
	//			}
	//			SetAngle(0.f, mSpeed.z);
	//		}

	//		if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
	//			&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) == -1
	//			&& pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*2.5f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
	//			&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*2.5f, position.y, position.z + sido.z*20.f), &p, &n, &l, -1) == -1
	//			)
	//		{
	//			turn = false;
	//			mSpeed.x = 0;
	//			SetSpeed(leng.z, &mSpeed.z);
	//			wallHit = false;
	//			SetAngle(0.f, mSpeed.z);
	//		}

	//	}

	//	break;
	//}
	//CubeAvoidance();
	switch (type)
	{
	case 0:
		//壁に当たった時
		if (mObj->GetHitLeft() || mObj->GetHitRight() || beforePosition.x != position.x)
		{
			mSpeed = DirectX::XMFLOAT3(0, 0, 0);
			type = 1;
			SetSpeed(leng.z, &mSpeed.z);
			//向いたほうに壁があったら
			if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + sido.x*2.f, position.y, position.z), DirectX::XMFLOAT3(position.x + sido.x*2.f, position.y, position.z + mSpeed.z*9.f), &p, &n, &l) != -1
				|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - sido.x*2.f, position.y, position.z), DirectX::XMFLOAT3(position.x - sido.x*2.f, position.y, position.z - mSpeed.z*9.f), &p, &n, &l) != -1)
			{
				mSpeed.z *= -1;
			}
			SetAngle(0.f, mSpeed.z);
			turn = false;
		}
		else if (leng.x <= 4 && leng.x >= -4)
		{
			turn = true;
		}

		if (turn)
		{
			if (
				pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1
				&& pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l) == -1
				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z), DirectX::XMFLOAT3(position.x + mSpeed.x*3.f, position.y, position.z + sido.z*12.f), &p, &n, &l, -1) == -1
				)
			{
				turn = false;
				mSpeed.z = 0;
				mSpeed.x = 0;
				SetSpeed(leng.z, &mSpeed.z);
				type = 1;
				SetAngle(0.f, mSpeed.z);
			}
		}

		break;
	case 1:
		//壁に当たった時
		if (mObj->GetHitDown() || mObj->GetHitUp() || beforePosition.z != position.z)
		{
			mSpeed = DirectX::XMFLOAT3(0, 0, 0);
			type = 0;
			SetSpeed(leng.x, &mSpeed.x);
			//向いたほうに壁があったら
			if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + sido.z*2.f), DirectX::XMFLOAT3(position.x + mSpeed.x*9.f, position.y, position.z + sido.z*2.f), &p, &n, &l) != -1
				|| pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z), DirectX::XMFLOAT3(position.x - mSpeed.x*9.f, position.y, position.z + sido.z*2.f), &p, &n, &l) != -1)
			{
				mSpeed.x *= -1;
			}
			SetAngle(0.5f, mSpeed.x);
			turn = false;
		}
		else if (leng.z <= 4 && leng.z >= -4)
		{
			turn = true;
		}
		if (turn)
		{
			if (pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + mSpeed.z*3.f), &p, &n, &l) == -1
				&& pObjManager.StageObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z - mSpeed.z*3.f), &p, &n, &l) == -1
				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z + mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z + mSpeed.z*3.f), &p, &n, &l, -1) == -1
				&& pObjManager.CubeObjRayPic(DirectX::XMFLOAT3(position.x, position.y, position.z - mSpeed.z*3.f), DirectX::XMFLOAT3(position.x + sido.x*12.f, position.y, position.z - mSpeed.z*3.f), &p, &n, &l, -1) == -1)
			{
				turn = false;
				mSpeed.z = 0;
				mSpeed.x = 0;
				SetSpeed(leng.x, &mSpeed.x);
				type = 0;
				SetAngle(0.5f, mSpeed.x);
			}
		}

		break;
	}
	CubeAvoidance();
	position.x += mSpeed.x*1.0f;
	position.z += mSpeed.z*1.0f;

#endif
	pStageManager.GetCube().at(targetNo).SetPosition(DirectX::XMFLOAT3(position.x + mSpeed.x * 7, position.y, position.z + mSpeed.z * 7));
}
