#include "screenout.h"

ScreenOut::ScreenOut()
{
	state = SCREENTYPE::PLAY;
	colorW = 1.f;
}
int ScreenOut::Update(float elapsed_time)
{
	int screen = 0;
	switch (state)
	{
	case SCREENTYPE::START:
		colorW -= elapsed_time;
		if (colorW <= 0.f)
		{
			state = SCREENTYPE::PLAY;
			colorW = 0.f;
			screen = 1;
		}
		break;
	case SCREENTYPE::PLAY:
		screen = -1;
		break;
	case SCREENTYPE::END:
		colorW += elapsed_time*0.5f;
		if (colorW >=1.f)
		{
			state = SCREENTYPE::PLAY;
			colorW = 1.f;
			screen = 2;
		}
		break;
	}
	return screen;
}

void ScreenOut::Render(ID3D11DeviceContext * devicecontext)
{
	if (text != nullptr)text->render(devicecontext, 0, 0, 1920, 1080, 0, 0, 1024, 1024, 0, 1, 1, 1, colorW);
}

void ScreenOut::Load(ID3D11Device * device, const wchar_t * filename)
{
	text = std::make_unique<Sprite>(device, filename);
}
