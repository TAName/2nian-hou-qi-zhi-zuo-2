#pragma once

#include <DirectXMath.h>
#include <memory>
#include "geometric_primitive.h"
#include "Board.h"
#include <list>
#include <algorithm>
#include<vector>
#include"easing.h"
#include "BlendState.h"
class Particle_obj //geometric_primitive ver
{
public:

	std::shared_ptr<geometric_primitive> obj;

	DirectX::XMFLOAT3	pos;
	DirectX::XMFLOAT3	speed;
	DirectX::XMFLOAT3	accel;
	DirectX::XMFLOAT3	angle;
	float	scale;
	DirectX::XMFLOAT4	color;
	float w;

	bool exist;
	int  dispTimer;
	int  dispMaxTimer;
	virtual void Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<geometric_primitive> obj) = 0;
	virtual void Update() = 0;
	void Render(ID3D11DeviceContext*immediate_context, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& projection, const DirectX::XMFLOAT4& light_dir);

};
class Particle_board //BillBoard ver
{
public:

	std::shared_ptr<Board> board;


	DirectX::XMFLOAT3	pos;
	DirectX::XMFLOAT3	speed;
	DirectX::XMFLOAT3	accel;
	DirectX::XMFLOAT3	angle;
	float	scale;
	DirectX::XMFLOAT4	color;
	float w;

	bool exist;
	int  dispTimer;
	int  dispMaxTimer;
	virtual void Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<Board> board) = 0;
	virtual void Update() = 0;
	void Render(ID3D11DeviceContext*immediate_context, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& projection, const DirectX::XMFLOAT4& light_dir);
};



class ParticleManager
{
private:

	std::list<std::shared_ptr<Particle_obj>>particleObjList;
	std::list<std::shared_ptr<Particle_board>>particleBoardList;

	std::vector<std::shared_ptr<geometric_primitive>> obj;
	std::vector<std::shared_ptr<Board>> board;



	static ParticleManager *instance;

	struct ParticleData
	{
		DirectX::XMFLOAT3	speed;
		DirectX::XMFLOAT3	accel;
		DirectX::XMFLOAT3	angle;
		float	scale;
		DirectX::XMFLOAT4	color;
		float w;
		DirectX::XMFLOAT3 v;
		int  dispTimer;
		int  dispMaxTimer;
		int amount;

	};

public:
	enum  tex //sprite 追加
	{
		star,
		shere1,
		shere2,
		smoke,
	};
	enum  geo//geometric_primitive　追加
	{
		cube,
		geoSphere

	};

	ParticleData Data;

	void Create(ID3D11Device *device)
	{
		if (instance != nullptr)return;
		instance = new ParticleManager;
		instance->Init(device);
	}


	void Init(ID3D11Device *device);

	void Update();

	void Render(ID3D11DeviceContext*immediate_context, const DirectX::XMMATRIX& view, const DirectX::XMMATRIX& projection, const DirectX::XMFLOAT4& light_dir);


	bool Add_Obj(std::shared_ptr<Particle_obj>, geo i, const DirectX::XMFLOAT3& pos = DirectX::XMFLOAT3(0.f, 0.f, 0.f)); //引数(Particle種類,素材種類,場所)
	bool Add_Board(std::shared_ptr<Particle_board>, tex i, const DirectX::XMFLOAT3& pos = DirectX::XMFLOAT3(0.f, 0.f, 0.f)); //引数(Particle種類,素材種類,場所)

	void Load();

	static ParticleManager* getInstance()
	{

		return instance;
	}

	void Destroy()
	{
		delete instance;
		instance = nullptr;
	}
};
#define	pParticleManager	ParticleManager::getInstance()

class ParticletstObj : public Particle_obj
{
public:

	void Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<geometric_primitive> obj);
	void Update();

};

class Particlebreak : public Particle_board
{
private:
	float scale_max;
public:
	void Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<Board> board);
	void Update();

};

class ParticlePut_Blue : public Particle_board
{

public:

	void Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<Board> board);
	void Update();

};

class ParticlePut_Red :public Particle_board
{
public:

	void Init(const DirectX::XMFLOAT3 & pos, std::shared_ptr<Board> board);
	void Update();
};