#include "loading.h"
#include"gamepad.h"
#include"screenout.h"
#include"soundmanager.h"

Loading::Loading(ID3D11Device * device, const wchar_t * textname, const wchar_t * loadingname,const wchar_t*buttonname)
	:loadEnd(true),timer(0),anim(0),textNo(0)
{
	text = std::make_unique<Sprite>(device, textname);
	load = std::make_unique<Sprite>(device, loadingname);

	button = std::make_unique<Sprite>(device, buttonname);
}

bool Loading::NowLoadingUpdate(bool isNowLoadingFlag)
{
	if (!isNowLoadingFlag&&loadEnd)
	{
		if (input::ButtonRisingState(0, input::PadLabel::A))
		{
			pSoundManager.Play(SoundManager::SOUND::decision);
			pSoundManager.SetVolume(SoundManager::SOUND::decision, 1.0f);
			pSoundManager.Play(SoundManager::SOUND::BGM_Select,true);
			pSoundManager.SetVolume(SoundManager::SOUND::BGM_Select,1.0f);
			loadEnd = false;
		}
	}
	if (input::ButtonRisingState(0, input::PadLabel::LEFT))
	{
		if (textNo == 1)textNo--;
	}
	else if (input::ButtonRisingState(0, input::PadLabel::RIGHT))
	{
		if (textNo == 0)textNo++;
	}

	return loadEnd;
}

bool Loading::NowLoadingRender(bool isNowLoadingFlag, ID3D11DeviceContext* devicecontext)
{
	if (loadEnd)
	{
		pScreenOut.Render(devicecontext);
		if (isNowLoadingFlag)
		{
			if (load != nullptr)
			{
				if (timer++ % 10 == 0)anim++;
				if (anim > 3)anim = 0;
				load->render(devicecontext, 1450, 950, static_cast<float>(320+anim*32), 32, 0, 0, static_cast<float>(640 + anim * 64), 64, 0, 0, 1, 1, 1);
			}
		}
		else
		{
			if (button != nullptr)
			{
				if (timer++ % 30 > 17)button->render(devicecontext, 1600, 900, 100, 100, 0, 0, 64, 64, 0, 1, 1, 1, 1);
			}
		}
		

		if(text != nullptr)text->render(devicecontext, 200, 100, 1360, 830, static_cast<float>(1360*textNo), 0, 1360, 830, 0, 1, 1, 1, 1);
	}
	return loadEnd;
}
