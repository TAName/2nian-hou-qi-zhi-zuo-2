#pragma once
#include"cubeObj.h"

class ElectrodeeCube
{
public:
	ElectrodeeCube(std::shared_ptr<ModelResource>modelResouce,const DirectX::XMFLOAT3&position, const DirectX::XMFLOAT4&color);
	void Update();
	//ゲッター
	const DirectX::XMFLOAT3&GetPosition() const { return mObj->GetPosition(); }
	const DirectX::XMFLOAT4&GetColor()const { return mObj->GetColor(); }
	const int GetObjType()const { return type; }
	const bool GetMoveFlag()const { return modeFlag; }
	void SetExist(bool exist) { mExist = exist; }
	//セッター
	void SetPosition(const DirectX::XMFLOAT3&posittion)
	{
		mObj->SetPosition(posittion);
	}
	void SetSpeed(const DirectX::XMFLOAT3&speed)
	{
		mSpeed = speed;
		modeFlag = true;
		setflag = true;
	};
	void SetColor(const DirectX::XMFLOAT4&color)
	{
		mObj->SetColor(color);
	}
	const bool GetObjExist() { return mObj->GetExist(); }
	operator const bool() const { return mExist; };
private:
	bool modeFlag;
	bool setflag;
	int type;
	bool mExist;
	DirectX::XMFLOAT3 beforePosition;
	DirectX::XMFLOAT3 mSpeed;
	std::shared_ptr<CubeObj>mObj;
};