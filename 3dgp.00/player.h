#pragma once

#include"playerobj.h"
#include <memory>
#include "x_input.h"

class Player
{
private:
    std::shared_ptr<PlayerObj> m_obj;
    DirectX::XMFLOAT3 m_forward;
    DirectX::XMFLOAT3 m_electrodeeCubePos;
    DirectX::XMFLOAT3 m_electrodeeCubeSpeed;
    int m_dir;
    bool m_moveFlg;
    int m_electroType;
    // 電気
    int m_electroNum;
    int m_target;
    // アニメーション
    int m_currentAnimation;
    int m_nextAnimation;
    bool m_loop;
    bool m_move;
	int mPlayerType;
    float d;
	int moveTimer;
    enum Direction
    {
        UP,
        DOWN,
        RIGHT,
        LEFT
    };

    /*
    アニメーションメモ

    0    移動
    1    待機
    2    もつ
    3    投げる
    4
    */
    enum AnimationNum
    {
        Wait = 1,
        Move = 0,
        Catch = 2,
        Throw = 3
    };

    // 向きの取得
    void m_PlayerDirection();
    // 移動関数
    void m_Move(float elapsedTime, 
        DirectX::XMFLOAT3& translate, DirectX::XMFLOAT3& rotation);
    // 投げる関数
    void m_Throw(DirectX::XMFLOAT3& translate, DirectX::XMFLOAT3& rotation);
    // アニメーション変更
    void m_ChangeAinmation(const int nextAnimation, bool loop = false)
    {
        if (m_currentAnimation != nextAnimation)
        {
            m_currentAnimation = nextAnimation;
            m_obj->Play(m_currentAnimation, loop);
        }
    }

public:
    bool m_isHave;
    bool m_isThrow;

    Player(ID3D11Device* device, const char* fbx_filename,int type,const DirectX::XMFLOAT3&position);
    void Update(float elapsed_time);

    // ゲッター
    const DirectX::XMFLOAT3 GetPosition() { return m_obj->GetPosition(); }
    const DirectX::XMFLOAT3 GetScale() { return m_obj->GetScale(); }
    const DirectX::XMFLOAT3 GetAngle() { return m_obj->GetAngle(); }
    const DirectX::XMFLOAT3& GetElectrodeCube() const { return m_electrodeeCubePos; }
    const DirectX::XMFLOAT3& GetElectrodeSpeed() const { return m_electrodeeCubeSpeed; }
    // セッター
    void SetPositin(const DirectX::XMFLOAT3& position) { m_obj->SetPosition(position); }
    void SetScale(const DirectX::XMFLOAT3& scale) { m_obj->SetScale(scale); }
    void SetAngle(const DirectX::XMFLOAT3& angle) { m_obj->SetAngle(angle); }
    void SetElectrodeeCude(DirectX::XMFLOAT3 ePos) { m_electrodeeCubePos = ePos; }
    void SetElectrodeType(const int type) { m_electroType = type; }
};
