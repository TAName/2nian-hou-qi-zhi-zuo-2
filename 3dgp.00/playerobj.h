#pragma once
#include"obj3d.h"

class PlayerObj :public Obj3D
{
public:
	PlayerObj(std::shared_ptr<ModelResource>model_resource);
	void CreateHitArea();
	const DirectX::XMFLOAT3&GetHitMin() const { return hitMin; }
	const DirectX::XMFLOAT3&GetHitMax() const { return hitMax; }
	void SetCarryCube(int carry) { carryCube = carry; }
	void SetAnimTime(float animtime) { animTime = animtime; }
	const int&GetCarryCube() { return carryCube; }
	const float GetAnimTime() { return animTime; }
	void SetSpeed(const DirectX::XMFLOAT3 speed) { mSpeed = speed; }
	const DirectX::XMFLOAT3&GetSpeed() { return mSpeed; }
	void SetHit(bool hit) { mHit = hit; }
	const bool GetHit() { return mHit; }
	void SetHitLeft(bool hit) { mHitLeft = hit; }
	const bool GetHitLeft() { return mHitLeft; }
	void SetHitRight(bool hit) { mHitRight = hit; }
	const bool GetHitRight() { return mHitRight; }
	void SetHitUp(bool hit) { mHitUp = hit; }
	const bool GetHitUp() { return mHitUp; }
	void SetHitDown(bool hit) { mHitDown = hit; }
	const bool GetHitDown() { return mHitDown; }

private:
	DirectX::XMFLOAT3 hitMin;
	DirectX::XMFLOAT3 hitMax;
	DirectX::XMFLOAT3 mSpeed;
	int carryCube;
	float animTime;
	bool mHit;
	bool mHitLeft;
	bool mHitRight;
	bool mHitUp;
	bool mHitDown;
};
#define MODO 0
