#pragma once
#include"sprite.h"
#include<memory>
class ScreenOut
{
public:
	void Initialize()
	{
		state = SCREENTYPE::PLAY;
		colorW = .0f;
	}
	int Update(float elapsed_time);
	void Render(ID3D11DeviceContext*devicecontext);
	static ScreenOut&getinctance()
	{
		static ScreenOut screen;
		return screen;
	}
	void ScreenInStart()
	{
		state = SCREENTYPE::START;
		colorW = 1.0f;
	}
	void ScreenOutStart()
	{
		if (state == SCREENTYPE::END)return;
		state = SCREENTYPE::END;
		colorW = .0f;
	}

	void Load(ID3D11Device*device, const wchar_t*filename);
private:
	enum class  SCREENTYPE
	{
		START,
		PLAY,
		END
	};
	SCREENTYPE state;
	float colorW;
	std::unique_ptr<Sprite>text;
	ScreenOut();
};
#define pScreenOut (ScreenOut::getinctance())