#pragma once

#include "obj3d.h"
#include<vector>
#include "vectordelete.h"
#include "collision.h"
#include "electrode_cube.h"
class StageObj
{
public:
	operator const bool() const { return mExist; };
	bool mExist;
	int filenumber;
	std::shared_ptr<Obj3D> obj;
};

class StageManager
{
private:

	int select;

	int plusX;
	int plusZ;
	struct SaveData
	{
		DirectX::XMFLOAT3 savePos;
		int saveFile;
	};
	struct SaveCube
	{
		DirectX::XMFLOAT3 savePos;
		DirectX::XMFLOAT4 color;
	};

	int filenumber;
	DirectX::XMFLOAT3 tstPos;
	std::vector<SaveData> save;
	std::vector<SaveCube> saveCube;

	std::vector<StageObj> stage;
	std::vector<ElectrodeeCube> cube;

	std::shared_ptr<Obj3D> tstObj;

	std::vector<std::shared_ptr<ModelResource>>modelResource;
	static StageManager *mStage;

	StageManager(ID3D11Device *device);

public:
	void Create(ID3D11Device *device)
	{
		if (mStage != nullptr)return;
		mStage = new StageManager(device);
	}
	void Destroy()
	{
		modelResource.clear();
		stage.clear();	
		save.clear();
		cube.clear();
	
		delete mStage;
		mStage = nullptr;
	}
	std::vector<ElectrodeeCube>&GetCube()
	{
		return cube;
	};
	static StageManager& getInstance()
	{
		return *mStage;
	}
	

	void EditorLoad();
	void Save();
	void Editer();
	void Load(const int&stageNo);
	void Update();
	
};
#define	pStageManager	(StageManager::getInstance())
