#pragma once
#include<DirectXMath.h>

bool IsHitCube(const DirectX::XMFLOAT3&cube1_min, const DirectX::XMFLOAT3&cube1_max, const DirectX::XMFLOAT3&cube2_min, const DirectX::XMFLOAT3&cube2_max);

//struct AABB
//{
//	DirectX::XMFLOAT3 left;
//	DirectX::XMFLOAT3 right;
//	DirectX::XMFLOAT3 up;
//	DirectX::XMFLOAT3 down;
//	DirectX::XMFLOAT3 front;
//	DirectX::XMFLOAT3 back;
//};
//bool IsHitAABBCube(const DirectX::XMFLOAT3&cube1_min, const DirectX::XMFLOAT3&cube1_max,const DirectX::XMFLOAT3&point1,const DirectX::XMFLOAT3&point2);
