#pragma once
#include"sound.h"
struct SoundData
{
	int soundNo;
	const char*soundfile;
};
class SoundManager
{
public:
	void Play(int soundNo, bool loop=false)
	{
		mSound.at(soundNo)->Play(loop);
	}
	void Stop(int soundNo)
	{
		mSound.at(soundNo)->Pause();
	}
	void Update()
	{
		for (auto&sound : mSound)
		{
			sound->Update();
		}
	}
	void SetVolume(int soundNo, const float volume)
	{
		mSound.at(soundNo)->SetVolume(volume);
	}
	~SoundManager()
	{
		mSound.clear();
	}
	enum SOUND
	{
		decision,//���艹
		BGM_Title,
		BGM_Select,
		BGM_Game,
		BGM_Result,
		Count,
		Des,
		max
	};
	static SoundManager&getinctance()
	{
		static SoundManager s;
		return s;
	}
private:
	std::vector<std::unique_ptr<Sound>>mSound;
	SoundManager();

};
#define pSoundManager (SoundManager::getinctance())