#pragma once
#include"model_renderer.h"
#include"playerobj.h"
#include"cubeObj.h"
class ObjManager
{
public:
	//生成
	void Create(ID3D11Device*device)
	{
		if (m_objmanager != nullptr)return;
		m_objmanager = new ObjManager(device);
	}
    static int m_winner;
	//セッター
	void Set_Player1(std::shared_ptr<PlayerObj>obj) { player1Obj = obj; }
	void Set_Player2(std::shared_ptr<PlayerObj>obj) { player2Obj = obj; }
	void SetEditor(std::shared_ptr<Obj3D>obj) { editorObj = obj; }
	void Set_StageObj(std::shared_ptr<Obj3D>obj) { stageObj.push_back(obj); }
	void Set_CubeObj(std::shared_ptr<CubeObj>obj) { cubeObj.push_back(obj); }
	void Set_FloorObj(std::shared_ptr<Obj3D>obj) { floorObj.push_back(obj); }
	//ゲッター
	const DirectX::XMFLOAT3&GetPlayer1Position() { return player1Obj->GetPosition(); }
	//初期化
	void StageObjClear() { stageObj.clear(); }
	void CubeObjClear() { cubeObj.clear(); }
	void floorObjClear() { floorObj.clear(); }
	//更新
	void Update();
	//レイピック
	int StageObjRayPic(const DirectX::XMFLOAT3&ryaStart, const DirectX::XMFLOAT3&ryaEnd, DirectX::XMFLOAT3*outPosition, DirectX::XMFLOAT3*outNormal, float*outDirection);
	int CubeObjRayPic(const DirectX::XMFLOAT3&ryaStart, const DirectX::XMFLOAT3&ryaEnd, DirectX::XMFLOAT3*outPosition, DirectX::XMFLOAT3*outNormal,float*outDirection, int t);
	//描画
	void Render(ID3D11DeviceContext*devicecontext, const DirectX::XMFLOAT4X4&view, const DirectX::XMFLOAT4X4&projection);
	//消去
	void Destroy()
	{
		stageObj.clear();
		cubeObj.clear();
		floorObj.clear();
		delete m_objmanager;
		m_objmanager = nullptr;
	}
	static ObjManager&getinctance()
	{
		return *m_objmanager;
	}
private:
	struct HIT
	{
		bool left = false;
		bool right = false;
		bool up = false;
		bool down = false;
		void clear()
		{
			left = false;
			right = false;
			up = false;
			down = false;
		}
	};
	void SetHitCubPosition(const DirectX::XMFLOAT3&cubeMin, const DirectX::XMFLOAT3&cubeMax, const DirectX::XMFLOAT3&playerScale, DirectX::XMFLOAT3*playerPosition, const DirectX::XMFLOAT3&size);
	int SetHitCubPosition(const DirectX::XMFLOAT3&cubeMin, const DirectX::XMFLOAT3&cubeMax, const DirectX::XMFLOAT3&playerScale, DirectX::XMFLOAT3*playerPosition,const DirectX::XMFLOAT3&sizeMin, const DirectX::XMFLOAT3&sizeMax,HIT*hit);
	void StageHit(const DirectX::XMFLOAT3&playerMin,const DirectX::XMFLOAT3&playerMax,const DirectX::XMFLOAT3&player2Min,const DirectX::XMFLOAT3&player2Max);
	void CubeHit(const DirectX::XMFLOAT3&playerMin, const DirectX::XMFLOAT3&playerMax, const DirectX::XMFLOAT3&player2Min, const DirectX::XMFLOAT3&player2Max);
	void EditorHit(const DirectX::XMFLOAT3&playerMin, const DirectX::XMFLOAT3&playerMax, const DirectX::XMFLOAT3&player2Min, const DirectX::XMFLOAT3&player2Max);
	static ObjManager*m_objmanager;
	bool cubeChange;
	std::shared_ptr<PlayerObj>player1Obj;
	std::shared_ptr<PlayerObj>player2Obj;
	std::shared_ptr<Obj3D>editorObj;
	std::vector<std::shared_ptr<Obj3D>>stageObj;
	std::vector<std::shared_ptr<Obj3D>>floorObj;
	std::vector<std::shared_ptr<CubeObj>>cubeObj;
	std::shared_ptr<ModelRenderer>modelRenderer;
	ObjManager(ID3D11Device*device);
};
#define pObjManager (ObjManager::getinctance())