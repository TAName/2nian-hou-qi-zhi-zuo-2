#pragma once
#include"obj3d.h"

class CubeObj :public Obj3D
{
public:
	CubeObj(std::shared_ptr<ModelResource> model_resource);
	//セッター
	void SetMove(bool move) { moveFlag = move; }
	void SetChange(bool chang) { changeFlag = chang; }
	void SetType(int t) { type = t; }
	//ゲッター
	const bool GetMove() { return moveFlag; }
	const bool GetChange() { return changeFlag; }
	const int GetType() { return type; }
	

	void ChangeClear() { changeFlag = false; }
private:
	bool moveFlag;
	bool changeFlag;
	int type;
};