#pragma once
#include"sprite.h"
#include<memory>

class StageStart
{
public:
	StageStart(ID3D11Device*device, const wchar_t*countfile, const wchar_t*selectfile, const wchar_t*buttontfile, const wchar_t*startfile);
	bool Update(float elapsed_time,int fadoNo);
	void Render(ID3D11DeviceContext*devicecontext);
	const bool RenderSelect() {return state == STARTSTATE::select;}
	const int GetStageNo() { return selectNo; }
private:
	void Select();
	void Count(float elapsed_time);
	void LoadCamera();
	void SaveCamera();
	void CameraEditor();
	enum class STARTSTATE
	{
		wait,
		select,
		in,
		count,
		play
	};
	STARTSTATE state;
	std::unique_ptr<Sprite>countText;
	std::unique_ptr<Sprite>selectText;
	std::unique_ptr<Sprite>buttonText;
	std::unique_ptr<Sprite>startText;
	float fTimer;
	int timer;
	int selectNo;
	int selecttimer;
	int buttontimer;
	DirectX::XMFLOAT2 position;
	DirectX::XMFLOAT3 eye;
};