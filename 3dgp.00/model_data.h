#pragma once
#include <string>
#include <vector>
#include <directxmath.h>

struct ModelData
{
	struct Node
	{
		std::string			name;
		int					parent_index;
		DirectX::XMFLOAT3	scale;
		DirectX::XMFLOAT4	rotate;
		DirectX::XMFLOAT3	translate;
	};

	struct Vertex
	{
		DirectX::XMFLOAT3	position;
		DirectX::XMFLOAT3	normal;
		DirectX::XMFLOAT2	texcoord;
		DirectX::XMFLOAT4	bone_weight;
		DirectX::XMUINT4	bone_index;
	};

	struct Subset
	{
		int					material_index;
		int					start_index;
		int					index_count;
	};

	struct Mesh
	{
		std::vector<Vertex>	vertices;
		std::vector<int>	indices;
		std::vector<Subset>	subsets;

		int					node_index;

		std::vector<int>					node_indices;
		std::vector<DirectX::XMFLOAT4X4>	inverse_transforms;
	};

	struct Material
	{
		DirectX::XMFLOAT4	color;
		std::string			texture_filename;
	};

	struct NodeKeyData
	{
		DirectX::XMFLOAT3	scale;
		DirectX::XMFLOAT4	rotate;
		DirectX::XMFLOAT3	translate;
	};

	struct Keyframe
	{
		float						seconds;
		std::vector<NodeKeyData>	node_keys;
	};
	struct Animation
	{
		float						seconds_length;
		std::vector<Keyframe>		keyframes;
	};
	struct Face
	{
		DirectX::XMFLOAT3 position[3];
		int materialIndex;
	};

	std::vector<Node>		nodes;

	std::vector<Mesh>		meshes;
	std::vector<Material>	materials;

	std::vector<Animation>	animations;

	std::vector<Face>faces;

};