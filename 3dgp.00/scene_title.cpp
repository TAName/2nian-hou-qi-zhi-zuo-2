#include "scene_title.h"
#include "camera.h"
#include"soundmanager.h"
#include "screenout.h"

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

int gamechoice = 0;
Scene_Title::Scene_Title(ID3D11Device * device)
{
	loading_thread = std::make_unique<std::thread>([&](ID3D11Device *device)
	{
		std::lock_guard<std::mutex> lock(loading_mutex);

		modelRenderer = std::make_shared<ModelRenderer>(device);
		std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
		FbxLoader fbx_loader;
		fbx_loader.Load("Data/FBX/player2_02_c.fbx", *model_data);
		std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));

		player = std::make_unique<PlayerObj>(model_resource);
		player->SetPosition(DirectX::XMFLOAT3(-10.0f, -10.0f, 0.0f));
		player->SetScale(DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f));
		player->SetAngle(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
		//player->Play(1, true);



		std::unique_ptr<ModelData> model_data1 = std::make_unique<ModelData>();
		FbxLoader fbx_loader1;
		fbx_loader1.Load("Data/FBX/player1_03_c.fbx", *model_data1);
		std::shared_ptr<ModelResource> model_resource1 = std::make_shared<ModelResource>(device, std::move(model_data1));


		player2 = std::make_unique<PlayerObj>(model_resource1);
		player2->SetPosition(DirectX::XMFLOAT3(10.0f, -10.0f, 0.0f));
		player2->SetScale(DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f));
		player2->SetAngle(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
		//	player2->Play(1, true);


		bg = std::make_unique<Sprite>(device, L"Data/image/bg.png");
		title = std::make_unique<Sprite>(device, L"Data/Image/titl.png");
		push = std::make_unique<Sprite>(device, L"Data/Image/start.png");
		solo = std::make_unique<Sprite>(device, L"Data/Image/2.png");
		duo = std::make_unique<Sprite>(device, L"Data/Image/1.png");


		tstpos = DirectX::XMFLOAT3(0, 0, 0);
		tstscale = 1;
		title_size = DirectX::XMFLOAT2(1517, 219);
		mode = Mode::start;
		selectflag = false;
		selectscale = DirectX::XMFLOAT2(500.0f, 200.0f);
		feadflag = false;
	}, device);
	particle = std::make_unique<Sprite>(device, L"Data/image/NOW_LOADING.png");
	pCamera.Create(DirectX::XMFLOAT3(0, 0, -50), DirectX::XMFLOAT3(0.0f, 00.0f, 0.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f));
	pScreenOut.Initialize();
	playsound = false;
	animTime = 0;
	anim = 0;
}


int Scene_Title::Update(float elapsed_time)
{
	static int time = 0;
	if (IsNowLoading())
	{

		return 0;
	}
	EndLoading();
	if (!playsound)
	{
		pSoundManager.Play(SoundManager::SOUND::BGM_Title, true);
		pSoundManager.SetVolume(SoundManager::SOUND::BGM_Title, 1.0f);
		playsound = true;
	}
	player->Animation(1.f / 30.f);

	player->Update();

	player2->Animation(1.f / 30.f);
	player2->Update();


	Move();

	if (feadflag)
	{
		int end = pScreenOut.Update(elapsed_time);

		if (end == 2)
		{
			pSoundManager.Stop(SoundManager::SOUND::BGM_Title);
			return 2;
		}
	}
#ifdef USE_IMGUI
	ImGui::Begin("pos ");

	ImGui::SliderFloat("pos.x", &tstpos.x, 0, 1920);
	ImGui::SliderFloat("pos.y", &tstpos.y, 0, 1080);
	ImGui::SliderFloat("pos.z", &tstpos.z, -10, 10);

	ImGui::SliderFloat("scale", &tstscale, 0, 200);

	ImGui::Text("p: (%f)", player->GetPosition().z);
	ImGui::End();
#endif
	
	return 0;

}

void Scene_Title::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{
	if (IsNowLoading())
	{
		if (animTime++ % 10 == 0)anim++;
		if (anim > 3)anim = 0;
		particle->render(devicecontext, 1450, 950, static_cast<float>(320 + anim * 32), 32, 0, 0, static_cast<float>(640 + anim * 64), 64, 0, 1, 1, 1, 1);

		return;
	}


	const float PI = 3.14f;

	//ビュー行列
	DirectX::XMMATRIX V;
	{
		//カメラの設定
		DirectX::XMVECTOR eye, focus, up;
		eye = DirectX::XMVectorSet(pCamera.GetEye().x, pCamera.GetEye().y, pCamera.GetEye().z, 1.0f);//視点
		focus = DirectX::XMVectorSet(pCamera.GetFocus().x, pCamera.GetFocus().y, pCamera.GetFocus().z, 1.0f);//注視点
		up = DirectX::XMVectorSet(pCamera.GetUp().x, pCamera.GetUp().y, pCamera.GetUp().z, 1.0f);//上ベクトル

		V = DirectX::XMMatrixLookAtLH(eye, focus, up);
	}
	DirectX::XMMATRIX P;
	{
		//画面サイズ取得のためビューポートを取得
		D3D11_VIEWPORT viewport;
		UINT num_viewports = 1;
		devicecontext->RSGetViewports(&num_viewports, &viewport);

		//角度をラジアン(θ)に変換
		float fov_y = 30 * (PI / 180.f);//角度
		float aspect = viewport.Width / viewport.Height;//画面比率
		float near_z = 0.1f;//表示最近面までの距離
		float far_z = 1000.0f;//表紙最遠面までの距離

		P = DirectX::XMMatrixPerspectiveFovLH(fov_y, aspect, near_z, far_z);
	}
	//

	static DirectX::XMFLOAT4 c(1.f, 1.0f, 1.0f, 1.f);
	DirectX::XMFLOAT4X4 view, projection;
	DirectX::XMStoreFloat4x4(&view, V);
	DirectX::XMStoreFloat4x4(&projection, P);

	bg->render(devicecontext, 0,0,1920,1080,0,0,1920,1080,0,1,1,1,1);
	title->render(devicecontext, 332, 45, 1300, 230, 0, 0, 1517, 219, 0, 1, 1, 1, 1);
	static int pushTime = 0;


	modelRenderer->Begin(devicecontext,
		view,
		projection,
		DirectX::XMFLOAT4(0, -1, -1, 0)	// ライトの向き
		);



	player->Render(modelRenderer, devicecontext);
	player2->Render(modelRenderer, devicecontext);

	modelRenderer->End(devicecontext);

	switch (mode)
	{
	case Mode::start:

		if (pushTime++ % 80 >= 40 && pushTime % 80 >= 0)
		{
			push->render(devicecontext, 720, 800, 463, 132, 0, 0, 463, 132, 0, 1, 1, 1, 1);
		}

		break;
	case Mode::select:

		if (selectflag)
		{
			solo->render(devicecontext, 1070, 300, selectscale.x, selectscale.y, 0, 0, 500, 200, 0, 1, 1, 1, 1);
			duo->render(devicecontext, 249, 300, 250, 100, 0, 0, 500, 200, 0, 1, 1, 1, 1);
		}

		else
		{
			solo->render(devicecontext, 1070, 300, 250, 100, 0, 0, 500, 200, 0, 1, 1, 1, 1);
			duo->render(devicecontext, 249, 300, selectscale.x, selectscale.y, 0, 0, 500, 200, 0, 1, 1, 1, 1);
		}
		break;
	}
	pScreenOut.Render(devicecontext);
}
void Scene_Title::Move()
{
	DirectX::XMFLOAT3 trans = player->GetPosition();
	DirectX::XMFLOAT3 trans2 = player2->GetPosition();

	switch (mode)
	{
	case Mode::start:
		if (input::ButtonRisingState(0, input::PadLabel::A))
		{
			pSoundManager.Play(SoundManager::SOUND::decision);
			pSoundManager.SetVolume(SoundManager::SOUND::decision, 2.0f);

			mode = Mode::select;
			player->Play(1, true);
		}
		break;
	case Mode::select:
		if (input::ButtonRisingState(0, input::PadLabel::RIGHT))
		{
			selectflag = true;
			player2->Play(1, true);
			player->Play(1, true);
		}
		if (input::ButtonRisingState(0, input::PadLabel::LEFT))
		{
			selectflag = false;
			player2->Play(2, false);
		}
		if (input::ButtonRisingState(0, input::PadLabel::A))
		{
			pSoundManager.Play(SoundManager::SOUND::decision);
			pSoundManager.SetVolume(SoundManager::SOUND::decision, 1.0f);
			if (selectflag)
			{
				mode = Mode::duo;
				player->Play(0, true);
				player2->Play(0, true);
				feadflag = true;
				gamechoice = 1;
			}
			else
			{
				mode = Mode::solo;
				player->Play(0, true);
				feadflag = true;
				gamechoice = 0;
			}
		}
		break;
	case Mode::solo:
		trans.z += 0.5;
		player->SetPosition(trans);
		pScreenOut.ScreenOutStart();
		break;
	case Mode::duo:
		trans.z += 0.5;
		trans2.z += 0.5;
		player->SetPosition(trans);
		player2->SetPosition(trans2);
		pScreenOut.ScreenOutStart();
		break;
	}


}
Scene_Title::~Scene_Title()
{
	pCamera.Destroy();
}