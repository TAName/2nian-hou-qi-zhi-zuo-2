#include "scene_manager.h"
#include"scene_title.h"
#include"scene_game.h"
#include"scene_result.h"
#include"screenout.h"
#include<Windows.h>
#include"soundmanager.h"
#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif
SceneManager*SceneManager::m_scenemanager = nullptr;
SceneManager::SceneManager(ID3D11Device * device)
{
	pScreenOut.Load(device, L"Data/image/siro.png");
	m_scene = new Scene_Title(device);

}
void SceneManager::Updata(float elapsed_time, ID3D11Device * device)
{
	//POINT pt;
	//GetCursorPos(&pt);
	int scene_chang = m_scene->Update(elapsed_time);
	if (scene_chang > 0)
	{
		ChangScene(scene_chang, device);
	}
	pSoundManager.Update();
//#ifdef USE_IMGUI
//    static int currentScene = 2;
//    static bool change1 = false; static bool change2 = false; static bool change3 = false;
//    ImGui::Begin("SceneManager");
//    ImGui::Text("currentSceneNum: %d   ", currentScene);
//    ImGui::SameLine();
//    switch (currentScene)
//    {
//    case SceneName::Title: ImGui::Text("Title"); break;
//    case SceneName::Game: ImGui::Text("Game"); break;
//    case SceneName::Result: ImGui::Text("Result"); break;
//    }
//    	
//        change1 = ImGui::RadioButton("SceneTitle", &currentScene, SceneName::Title);
//        ImGui::SameLine();
//        change2 = ImGui::RadioButton("SceneGame", &currentScene, SceneName::Game);
//        ImGui::SameLine();
//        change3 = ImGui::RadioButton("SceneResult", &currentScene, SceneName::Result);
//        if (change1 || change2 || change3)
//        {
//            ChangScene(currentScene, device);
//            change1 = false; change2 = false; change3 = false;
//        }
//    	ImGui::End();
//#endif

}

void SceneManager::ChangScene(int next_scene, ID3D11Device * device)
{
	delete m_scene;
	switch (next_scene)
	{
	case SceneName::Title:
		m_scene = new Scene_Title(device);

		break;
	case SceneName::Game:
		m_scene = new Scene_Game(device);

		break;
	case SceneName::Result:
		m_scene = new Scene_Result(device);

		break;
	}

}

void SceneManager::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{

	m_scene->Render(elapsed_time, devicecontext);

}
